# Aufgabe 

## Infos

### Beispielproject 
- [Beispiel Projekt](https://sopra.cs.tu-dortmund.de/wiki/projekte/beispiele/gettingthingsdone/start)


## Für dieses Issue wird folgendes erwartet


## Wohin damit ?
1. [Eigenes Wiki](https://sopra-gitlab.cs.tu-dortmund.de/sopra20A/gruppe01/projekt1/-/wikis/Analysemodell)
2. [Präsentations Wiki](https://sopra-gitlab.cs.tu-dortmund.de/sopra20A/presentations/-/wikis/Gruppe01) 

## Hilfe

 (╯°□°)╯︵ ┻━┻
<
Einfach bei Telegram Schreiben ;)
