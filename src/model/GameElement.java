package model;

import java.io.Serializable;
import java.util.UUID;

public abstract class GameElement implements Serializable {
    String identifier;

    public GameElement() {

        this.identifier = UUID.randomUUID().toString();
    }

    public String getId() {
        return identifier;
    }
}
