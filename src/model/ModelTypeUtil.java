package model;

public class ModelTypeUtil {
    public enum CardType {
        GULDEN,
        DIRHAM,
        DENAR,
        DUKATEN,
        WERTUNG;
    }

    public enum ActionType {
        // TODO define ENUM for actions
    }

    public enum PlayerColor {
        YELLOW,
        BLUE,
        RED,
        WHITE,
        ORANGE,
        GREEN
    }

    public enum BuildingType {
        BRUNNEN,
        PAVILLON,
        SERAIL,
        ARKADEN,
        GEMAECHER,
        GARTEN,
        TURM
    }

    public enum  Direction {
        UP,
        RIGHT,
        DOWN,
        LEFT
    }

}
