package model;

import java.awt.*;

public class BoardPosition extends GameElement {
    int xPos, yPos;

    public BoardPosition(int xPos, int yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public BoardPosition(Point point) {
        this.xPos = point.x;
        this.yPos = point.y;
    }

    public int getX() {
        return xPos;
    }

    public void setX(int xPos) {
        this.xPos = xPos;
    }

    public int getY() {
        return yPos;
    }

    public void setY(int yPos) {
        this.yPos = yPos;
    }

    public String toString(){
        return "x:" + xPos + " y:" + yPos;
    }
}
