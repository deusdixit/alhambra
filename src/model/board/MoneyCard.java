package model.board;

import model.GameElement;
import model.ModelTypeUtil.CardType;

public class MoneyCard extends GameElement implements Comparable{

    private int value;

    private CardType type;

    public MoneyCard(int value, CardType type) {
        this.value = value;
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MoneyCard) {
            MoneyCard moneyCard = (MoneyCard) obj;
            if (moneyCard.getValue() == getValue() && moneyCard.getType() == getType()) {
                return true;
            }
        }
        return false;

    }

    @Override
    public String toString() {
        return this.type.toString() + " " + this.value;
    }

    @Override
    public int compareTo(Object object) {
        if (object instanceof MoneyCard){
            return ((MoneyCard) object).getValue()-this.getValue();
        }
        return -1;
    }
}
