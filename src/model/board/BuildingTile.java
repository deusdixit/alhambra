package model.board;

import model.GameElement;
import model.ModelTypeUtil.BuildingType;

import java.io.Serializable;

public class BuildingTile extends GameElement implements Comparable {

    private int price;
    private Wall wall;
    private BuildingType type;

    public BuildingTile(int price, BuildingType type, Wall wall) {
        this.price = price;
        this.type = type;
        this.wall = wall;
    }

    public BuildingTile() {
        this.price = 0;
        this.type = BuildingType.BRUNNEN;
        wall = new Wall(false, false, false, false);
    }

    public int getPrice() {
        return price;
    }

    public BuildingType getType() {
        return type;
    }

    public boolean isWallTop() {
        return wall.wallTop;
    }

    public boolean isWallLeft() {
        return wall.wallLeft;
    }

    public boolean isWallBottom() {
        return wall.wallBottom;
    }

    public boolean isWallRight() {
        return wall.wallRight;
    }

    public int getWallCount() {
        return wall.getWallCount();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BuildingTile) {
            BuildingTile tile = (BuildingTile) obj;
            if (tile.price == price && tile.type == type && tile.isWallBottom() == tile.isWallBottom()
                    && tile.isWallTop() == isWallTop() && tile.isWallLeft() == isWallLeft()
                    && tile.isWallRight() == isWallRight()) {
                return true;
            }
        }
        return false;

    }

    @Override
    public String toString() {
        return type.toString() + " " + price;
    }

    @Override
    public int compareTo(Object object) {
        if (object instanceof BuildingTile){
            return  ((BuildingTile)object).price-this.price;
        }
        return -1;
    }

    public static class Wall implements Serializable {

        private boolean wallTop;

        private boolean wallLeft;

        private boolean wallBottom;

        private boolean wallRight;

        public Wall(boolean wallTop, boolean wallLeft, boolean wallBottom, boolean wallRight) {
            this.wallTop = wallTop;
            this.wallLeft = wallLeft;
            this.wallBottom = wallBottom;
            this.wallRight = wallRight;
        }

        public boolean isWallTop() {
            return wallTop;
        }

        public boolean isWallLeft() {
            return wallLeft;
        }

        public boolean isWallBottom() {
            return wallBottom;
        }

        public boolean isWallRight() {
            return wallRight;
        }

        public int getWallCount() {
            return (isWallTop()?1:0) + (isWallLeft()?1:0) + (isWallBottom()?1:0) +(isWallRight()?1:0);
        }
    }
}
