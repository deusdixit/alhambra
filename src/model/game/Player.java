package model.game;

import model.GameElement;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.MoneyCard;

import java.util.LinkedList;
import java.util.List;

public class Player extends GameElement {

    private String name;

    private List<MoneyCard> moneyCards;

    private List<BuildingTile> reserveBuildings;

    private PlayerColor color;

    private int score;

    private boolean cheater;

    private GameBoard gameBoard;

    public Player(String name, PlayerColor color) {

        this.name = name;
        this.color = color;
        score = 0;
        cheater = false;
        moneyCards = new LinkedList<>();
        reserveBuildings = new LinkedList<>();
        gameBoard = new GameBoard();
    }

    public String getName() {
        return name;
    }

    public List<MoneyCard> getMoneyCards() {
        return moneyCards;
    }

    public List<BuildingTile> getReserveBuildings() {
        return reserveBuildings;
    }
    
    public void setReserveBuildings(List<BuildingTile> reserveBuildings){ this.reserveBuildings = reserveBuildings;}

    public PlayerColor getColor() {
        return color;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int newScore) {
	score = newScore;
    }

    public boolean getCheater() { return cheater; }

    public void setCheater(boolean cheater) { this.cheater = cheater; }

    public GameBoard getGameBoard() {
        return gameBoard;
    }

    public void setMoneyCards(List<MoneyCard> moneyCards) {
        this.moneyCards = moneyCards;
    }

    @Override
    public String toString() {
        return this.getName() + " " + this.getColor().name() + "\n";

    }
}
