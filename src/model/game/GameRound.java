package model.game;

import model.GameElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GameRound extends LinkedList<GameTurn> implements Serializable {

    public GameRound() {
        super();
    }

    public List<GameTurn> getTurns() {
        return this;
    }

    public void removeTurn(GameTurn gameTurn){
            this.remove(gameTurn);
    }
}
