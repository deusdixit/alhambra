package model.game;

import model.GameElement;

public abstract class GameTurn extends GameElement {

    Player currentPlayer;

    public GameTurn(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    /**
     * Executes a turn
     */
    public abstract void doTurn();

    /**
     * Undos turn
     */
    public abstract void unDoTurn();

    /**
     * checks if the rules are conform
     * @return returns true if the rules are conform
     */
    public abstract boolean validate();

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public abstract String printTurn();
}
