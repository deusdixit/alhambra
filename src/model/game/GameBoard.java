package model.game;

import model.BoardPosition;
import model.GameElement;
import model.board.BuildingTile;

import java.util.ArrayList;
import java.util.List;


public class GameBoard extends GameElement {
    public final static int SIZE = 21;
    private BuildingTile[][] board;

    public GameBoard() {
        board = new BuildingTile[SIZE][SIZE];
        board[10][10] = new BuildingTile();

    }

    public BuildingTile[][] getBoard() {
        return board;
    }

    public BuildingTile getPosition(BoardPosition pos){
        return board[pos.getX()][pos.getY()];
    }

    public void setBoard(BuildingTile[][] board) {
        this.board = board;
    }

    public List<BuildingTile> getBuildingList() {
        ArrayList<BuildingTile> list = new ArrayList<>();

        for (int pos1 = 0; pos1 < SIZE; pos1++) {
            for (int pos2 = 0; pos2 < SIZE; pos2++) {
                if (board[pos1][pos2] != null) {
                    list.add(board[pos1][pos2]);
                }
            }
        }


        return list;
    }

    public int[] getBuildings(){
        List<BuildingTile> buildings = getBuildingList();
        int[] arr = new int[6];
        for(BuildingTile bt : buildings){
            switch (bt.getType()){
                case PAVILLON: arr[0]++; break;
                case SERAIL: arr[1]++; break;
                case ARKADEN: arr[2]++; break;
                case GEMAECHER: arr[3]++; break;
                case GARTEN: arr[4]++; break;
                case TURM: arr[5]++; break;
                case BRUNNEN: break;
                default: throw new IllegalArgumentException("Unexpected Tile!");
            }

        }

        return arr;
    }

}
