package model.game;

import controller.GameLogicController;
import controller.ai.AIController;
import controller.ai.BuyAIController;
import controller.ai.NaiveAIController;
import controller.ai.NaiveOptimalAIController;
import model.ModelTypeUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Bot extends Player {

    transient AIController aiController ;
    boolean ready;
    int type;
    List<GameTurn> gameTurns = new LinkedList<>();

    public Bot(String name, ModelTypeUtil.PlayerColor color, GameLogicController gameLogicController) {
        super(name, color);
        ready=true;
        aiController=new NaiveAIController(this,gameLogicController);
    }
    public Bot(String name, ModelTypeUtil.PlayerColor color, int type) {
        super(name, color);
        ready=false;
        this.type=type;
    }
    public void startBot(GameLogicController gameLogicController){
        switch (type){
            case 1:
            default:
            aiController=new BuyAIController(this,gameLogicController);
            break;
            case  2:
            aiController=new NaiveAIController(this,gameLogicController);
            break;
            case 3:
            aiController= new NaiveOptimalAIController(this,gameLogicController);

        }

        ready=true;
    }

    public boolean isReady() {
        return ready;
    }

    /**
     *  getNextTurn
     * @return next Turn
     */
    public GameTurn getNextTurn() {
        if (gameTurns.size() == 0) { gameTurns = aiController.think(); }
        if (gameTurns.size() == 0) {
            return null;
        }
        GameTurn exe = gameTurns.remove(0);
        String printString = "EXECUTING >>> " + getColor() + " " + exe.printTurn();
        System.out.println(printString);
        return exe;
    }

}
