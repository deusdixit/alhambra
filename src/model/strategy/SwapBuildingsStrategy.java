package model.strategy;

import model.BoardPosition;
import model.GameInstance;
import model.board.BuildingTile;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Strategy to swap a {@link BuildingTile} from the {@link model.game.GameBoard}
 * with a reserved {@link BuildingTile}.
 */
public class SwapBuildingsStrategy extends GameTurn {

    BuildingTile buildingTile;
    int xValue;
    int yValue;
    GameInstance gameInstance;
    BuildingTile[][] oldBoard;
    List<BuildingTile> oldReserve;
    Player oldPlayer;

	/**
	 * Constructor.
	 *
	 * @param currentPlayer The current {@link Player}.
	 * @param buildingTile  The reserverd {@link BuildingTile}.
	 * @param gameInstance  The {@link GameInstance} of the game.
	 * @param position      The {@link BoardPosition} where the other
	 *                      {@link BuildingTile} is placed.
	 */
	public SwapBuildingsStrategy(Player currentPlayer, BuildingTile buildingTile, GameInstance gameInstance,
			BoardPosition position) {
		super(currentPlayer);
		this.buildingTile = buildingTile;
		this.gameInstance = gameInstance;
		this.xValue = position.getX();
		this.yValue = position.getY();
	}

    @Override
    /**
     * 1. Remove Tile from reserve 2. put board tile into reserve 3. remove board
     * tile from reserve 4. place tile to reserve
     */
    public void doTurn() {
        BuildingTile[][] board = getCurrentPlayer().getGameBoard().getBoard();
        oldBoard = board.clone();
        for (int i = 0; i < board.length; i++) {
            oldBoard[i] = oldBoard[i].clone();
        }
        oldPlayer = getCurrentPlayer();
        oldReserve = new ArrayList<>(getCurrentPlayer().getReserveBuildings());

        if (validate()) {
            getCurrentPlayer().getReserveBuildings().remove(buildingTile);
            getCurrentPlayer().getReserveBuildings().add(getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue]);
            getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue] = null;
            getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue] = buildingTile;
            gameInstance.setBusy(true);
        }
    }

    @Override
    /**
     * 1. Reset gameboard 2. reset reserve 3. set busy to false
     */
    public void unDoTurn() {
        getCurrentPlayer().getGameBoard().setBoard(oldBoard);
        getCurrentPlayer().setReserveBuildings(oldReserve);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setBusy(false);
    }

	@Override
	/**
	 * Valid if: 1. Player has the tile in reserve 2. Specified location has a
	 * building placed 3. Valid location for building tile
	 */
	public boolean validate() {
		if (!getCurrentPlayer().getReserveBuildings().contains(buildingTile)) {
			return false;
		}
		if (getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue] == null) {
			return false;
		}
		if (!GameLogicUtil.validField(getCurrentPlayer().getGameBoard().getBoard())) {
			return false;
		}
		return true;

	}

	@Override
	public String printTurn() {
		String print = "";
		if (buildingTile != null && validate()) {
			BuildingTile toSwap = getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue];
			print = "Tausche das Gebäudeplättchen " + buildingTile.toString()
					+ " aus der Reserve mit dem Gebäudeplättchen " + toSwap.toString() + " an der Position " + xValue
					+ " " + yValue;
		}
		return print;
	}
}
