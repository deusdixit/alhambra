package model.strategy;

import controller.GameLogicController;
import model.BoardPosition;
import model.GameInstance;
import model.board.BuildingTile;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

import java.util.ArrayList;

public class DestroyBuildingStrategy extends GameTurn {

    GameInstance gameInstance;
    BoardPosition boardPos;

    BuildingTile[][] oldBoard;
    ArrayList<BuildingTile> oldReserve;
    Player oldPlayer;

    public DestroyBuildingStrategy(Player currentPlayer, GameInstance gameInstance, BoardPosition boardPosition) {
        super(currentPlayer);
        this.gameInstance = gameInstance;
        this.boardPos = boardPosition;
    }

    @Override
    /**
     * Removes a building from the building yard
     */
    public void doTurn() {
        BuildingTile[][] curr = getCurrentPlayer().getGameBoard().getBoard();
        oldBoard = curr.clone();
        for(int i=0; i< oldBoard.length; i++)
            oldBoard[i] = oldBoard[i].clone();
        oldPlayer = getCurrentPlayer();
        oldReserve = new ArrayList<>(getCurrentPlayer().getReserveBuildings());
        if (validate()) {
            getCurrentPlayer().getReserveBuildings().add(getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()]);
            getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()] = null;

            gameInstance.setBusy(true);
        }
    }

    @Override
    public void unDoTurn() {
        getCurrentPlayer().setReserveBuildings(oldReserve);
        getCurrentPlayer().getGameBoard().setBoard(oldBoard);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setBusy(false);
    }

    /**
     * 1. Check there is a tile on [x][y]
     * 2. 
     * 3.
     */
    @Override
    public boolean validate() {
        if (getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()] == null)
            return false;
        if (!GameLogicUtil.validField(getCurrentPlayer().getGameBoard().getBoard()))
            return false;
        return true;
    }

    @Override
    public String printTurn() {
        String print = validate()? "" : "(invalid) ";
        BuildingTile toRemove = getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()];
        print += "Nehme das Gebäudeplättchen " + toRemove.toString() + " von der Position " + boardPos.toString() + " und lege es in die Reserve";
        return print;
    }
}
