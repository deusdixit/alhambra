package model.strategy;

import component.mainwindow.Money;
import model.GameInstance;
import model.board.MoneyCard;
import model.game.GameTurn;
import model.game.Player;
import util.exception.InvalidGameTurnExecption;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The strategy to take money from the bank.
 */
public class RobBankStrategy extends GameTurn {

    GameInstance gameInstance;
    MoneyCard[] cardsToRob;
    MoneyCard[] oldBank;
    List<MoneyCard> oldPlayerCards;
    Player oldPlayer;

    /**
     * Contructor.
     *
     * @param gameInstance  The {@link GameInstance} of the game.
     * @param currentPlayer The current {@link Player}.
     * @param cards         The {@link MoneyCard}s to be taken from the bank.
     */
    public RobBankStrategy(GameInstance gameInstance, Player currentPlayer, MoneyCard... cards) {
        super(currentPlayer);
        this.gameInstance = gameInstance;
        this.cardsToRob = cards;
    }

    @Override
    /**
     * Removes cards from bank and add them to the player
     */
    public void doTurn() {
        oldBank = gameInstance.getBank().clone();
        oldPlayerCards = new ArrayList<>(getCurrentPlayer().getMoneyCards());
        oldPlayer = getCurrentPlayer();

        if (validate()) {
            for (MoneyCard card : cardsToRob) {
                removeCardFromBank(card);
                getCurrentPlayer().getMoneyCards().add(card);
            }
        }
        gameInstance.setBusy(true);
    }

    /**
     * Function to remove one {@link MoneyCard} from the bank.
     */
    private void removeCardFromBank(MoneyCard card) {
        for (int i = 0; i < gameInstance.getBank().length; i++) {
            if (gameInstance.getBank()[i] != null) {
                if (gameInstance.getBank()[i].equals(card)) {
                    gameInstance.getBank()[i] = null;
                    break;
                }
            }
        }
    }

    /**
     * Undo this turn.
     */
    @Override
    public void unDoTurn() {
        gameInstance.setBank(oldBank);
        getCurrentPlayer().setMoneyCards(oldPlayerCards);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setBusy(false);
    }

    /**
     * Checks if the {@link Player} can take the money.
     */
    @Override
    public boolean validate() {
        int max = 1;
        if (cardsToRob.length == max) {
            return true;
        }

        int sum = 0;
        for (MoneyCard card : cardsToRob) {
            sum += card.getValue();
        }
        return sum <= 5;
    }

    @Override
    public String printTurn() {
        String print = "";
        if (cardsToRob.length > 0) {
            print += validate()? "" : "(invalid) ";
            String cards = Arrays.stream(cardsToRob).map(MoneyCard::toString).collect(Collectors.joining(", "));
            print += "Nehme die Geldkarten " + cards;
        }
        return print;
    }
}
