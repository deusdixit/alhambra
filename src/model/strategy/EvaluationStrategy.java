package model.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.board.BuildingTile;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

/**
 * Strategy to evaluate the current scoring of the players.
 */
public class EvaluationStrategy extends GameTurn {

	int eval;
	GameInstance gameInstance;
	private final int[][] ROUND_ONE = new int[][] { { 1, 0, 0 }, { 2, 0, 0 }, { 3, 0, 0 }, { 4, 0, 0 }, { 5, 0, 0 },
			{ 6, 0, 0 } };
	private final int[][] ROUND_TWO = new int[][] { { 8, 1, 0 }, { 9, 2, 0 }, { 10, 3, 0 }, { 11, 4, 0 }, { 12, 5, 0 },
			{ 13, 6, 0 } };
	private final int[][] ROUND_THREE = new int[][] { { 16, 8, 1 }, { 17, 9, 2 }, { 18, 10, 3 }, { 19, 11, 4 },
			{ 20, 12, 5 }, { 21, 13, 6 } };
	private int[][] currentRound;
    
        private int[][] allScores;
	private Map<String, Integer> reservedScores;

	/**
	 * Constructor.
	 * 
	 * @param currentPlayer The current {@link Player}.
	 * @param gameInstance  {@link GameInstance} of the game.
	 * @param eval          The number of the round to be scored.
	 */
	public EvaluationStrategy(Player currentPlayer, GameInstance gameInstance, int eval) {
		super(currentPlayer);
		this.gameInstance = gameInstance;
		this.eval = eval;
		reservedScores = new HashMap<>();
		allScores = null;
		switch (this.eval) {
			case 1:
				currentRound = ROUND_ONE;
				break;
			case 2:
				currentRound = ROUND_TWO;
				break;
			case 3:
				currentRound = ROUND_THREE;
				break;
			default:
				break;
		}
	}

	@Override
	/**
	 * Executes the evaluation of the game triggered by an evaluation card
	 */
	public void doTurn() {

		for (Player player : gameInstance.getPlayerList()) {
			reservedScores.put(player.getId(), player.getScore());
		}
		this.allScores = new int[gameInstance.getPlayerList().size()][6];
		for (BuildingType valueType : BuildingType.values()) {
			switch (valueType) {
				case BRUNNEN:
					break;
				case PAVILLON:
					fillScores(BuildingType.PAVILLON, 0);
					break;
				case SERAIL:
					fillScores(BuildingType.SERAIL, 1);
					break;
				case ARKADEN:
					fillScores(BuildingType.ARKADEN, 2);
					break;
				case GEMAECHER:
					fillScores(BuildingType.GEMAECHER, 3);
					break;
				case GARTEN:
					fillScores(BuildingType.GARTEN, 4);
					break;
				case TURM:
					fillScores(BuildingType.TURM, 5);
					break;
				default:
					break;
			}
		}
		for (Player player : gameInstance.getPlayerList()) {
			int oldScore = player.getScore();
			player.setScore(oldScore + GameLogicUtil.getLargestOuterWallLength(player.getGameBoard().getBoard()));
		}      
		if (gameInstance.isEval()==3){
			gameInstance.setFinish(true);
		}
		gameInstance.setEval(0);
	}

	/**
	 * Add the current score of each {@link Player} for a specific
	 * {@link model.ModelTypeUtil.BuildingType}.
	 * 
	 * @param type  The {@link model.ModelTypeUtil.BuildingType}.
	 * @param index The round number.
	 */
	private void fillScores(BuildingType type, int index) {
		List<Player> playerList = gameInstance.getPlayerList();
		int[] ranking = new int[gameInstance.getPlayerList().size()];
		for (int i = 0; i < playerList.size(); i++) {
			ranking[i] = countBuildings(playerList.get(i), type);
		}
		int[] sortedIndices = IntStream.range(0, ranking.length).boxed().sorted((i, j) -> -(ranking[i] - ranking[j]))
				.mapToInt(ele -> ele).toArray();
		int rankingCount = playerList.size() < 3 ? 2 : 3;
		int begin = 0;
		for (int i = 0; i < rankingCount; i++) {
			if (ranking[sortedIndices[begin]] == 0) {
				continue;
			}
			int end = firstDifference(ranking, sortedIndices, begin);
			int diff = end - begin + 1;
			int score = 0;
			for (int j = begin; j < Math.min(end + 1, 3); j++) {
				score += currentRound[index][j];
			}
			score = score / diff;

			for (int j = begin; j <= end; j++) {
				int oldScore = playerList.get(sortedIndices[j]).getScore();
				playerList.get(sortedIndices[j]).setScore(score + oldScore);
				allScores[sortedIndices[j]][index] = score;
			}
			begin = end + 1;
			i += diff - 1;
		}
	}

	/**
	 * Returns all the scores. Null if the turn isn't executed yet.
	 */	
	public int[][] getAllScores() {
		return allScores;
	}
	
	/**
	 * Returns the index of the first occuring difference in the array ranking. Used
	 * to determine how many players have the same number of {@link BuildingTile}s
	 * of the specifiy {@link model.ModelTypeUtil.BuildingType}.
	 * 
	 * @param ranking   An array of the quantity of the {@link BuildingTile} with
	 *                  the specific {@link model.ModelTypeUtil.BuildingType} for
	 *                  each {@link Player}.
	 * @param indices[] The array of the sorting indices.
	 * @param start     The starting point.
	 */
	private int firstDifference(int[] ranking, int[] indices, int start) {
		int startValue = ranking[indices[start]];
		for (int i = start + 1; i < indices.length; i++) {
			if (startValue != ranking[indices[i]]) {
				return i - 1;
			}
		}
		return indices.length - 1;
	}

	/**
	 * Return the number of {@link BuildingTile} with an specific
	 * {@link BuildingType} of a {@link Player}.
	 * 
	 * @param player The {@link Player}.
	 * @param type   The {@link model.ModelTypeUtil.BuildingType}.
	 */
	private int countBuildings(Player player, BuildingType type) {
		int result = 0;
		BuildingTile[][] board = player.getGameBoard().getBoard();
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j] != null && type == board[i][j].getType()) {
					result++;
				}
			}
		}
		return result;
	}

	/**
	 * Undo this turn.
	 **/
	@Override
	public void unDoTurn() {
		for (Player player : gameInstance.getPlayerList()) {
			if (reservedScores.containsKey(player.getId())) {
				player.setScore(reservedScores.get(player.getId()));
			}
		}
		gameInstance.setEval(this.eval);
	}

	/**
	 * Not used.
	 **/
	@Override
	public boolean validate() {
		return false;	
	} 
	/* *
	 *Prints the strategy.
	 */
	@Override
	public String printTurn() {
		return "Führe Auswertung aus.";
	}

	public int getEval() {
		return eval;
	}
}
