package model.strategy;

import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameTurn;
import model.game.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Strategy to buy and reserve {@link BuildingTile}
 */
public class BuyReserveBuildingStrategy extends GameTurn {

    GameInstance gameInstance;
    private MoneyCard[] moneycards;
    BuildingTile buildingTile;
    ModelTypeUtil.CardType moneyType;
    BuildingTile[] oldBuildingYard;
    List<MoneyCard> oldMoneyCards;
    Stack<MoneyCard> oldGYard;
    Player oldPlayer;

    /**
     * Constructor.
     *
     * @param currentPlayer The current {@link Player}.
     * @param buildingTile  {@link BuildingTile} to be bought and reserved.
     * @param gameInstance  {@link GameInstance} of the game.
     * @param moneyCards    {@link MoneyCard}s used for the transaction.
     */
    public BuyReserveBuildingStrategy(Player currentPlayer, BuildingTile buildingTile, GameInstance gameInstance,
                                      MoneyCard... moneyCards) {
        super(currentPlayer);
        this.gameInstance = gameInstance;
        this.moneycards = moneyCards;
        this.buildingTile = buildingTile;
        oldGYard=new Stack<>();

    }

    @Override
    /**
     * Buys and places a bulding into the reserve
     */
    public void doTurn() {
        oldBuildingYard = gameInstance.getBuildingYard().clone();
        oldMoneyCards = new ArrayList<>(getCurrentPlayer().getMoneyCards());
        oldPlayer = getCurrentPlayer();
        if(!gameInstance.getGraveyard().isEmpty())
            oldGYard.addAll(gameInstance.getGraveyard());

        if (validate()) {
            removeBuildingFromYard();
            removeMoneyFromPlayer();
            getCurrentPlayer().getReserveBuildings().add(buildingTile);
            gameInstance.setBusy(!canBuyPerfec());
        }

    }

    /**
     * undo this turn.
     */
    @Override
    public void unDoTurn() {
        gameInstance.setBuildingYard(oldBuildingYard);
        oldPlayer.setMoneyCards(oldMoneyCards);
        oldPlayer.getReserveBuildings().remove(buildingTile);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setGraveyard(oldGYard);
        gameInstance.setBusy(false);
    }

    /**
     * Check if the {@link BuildingTile} is present in the building yard and if the
     * {@link Player} has enough money.
     */
    @Override
    public boolean validate() {
        if (isInBuildingYard())
            return hasEnoughMoney();
        return false;
    }

    /**
     * Check if the {@link Player} has enough money.
     */
    private boolean hasEnoughMoney() {
        return calculateBuyingPower() >= buildingTile.getPrice();
    }

    /**
     * Check if the {@link Player} can buy without loss.
     */
    private boolean canBuyPerfec() {
        return calculateBuyingPower() == buildingTile.getPrice();
    }

    /**
     * Calculates buying power of the player.
     */
    private int calculateBuyingPower() {
        return Arrays.stream(moneycards).mapToInt(value -> value.getValue()).sum();
    }

    /**
     * Check if {@link BuildingTile} is present in the building yard.
     */
    private boolean isInBuildingYard() {
        for (int i = 0; i < gameInstance.getBuildingYard().length; i++) {
            if (gameInstance.getBuildingYard()[i] != null) {
                if (gameInstance.getBuildingYard()[i].equals(buildingTile)) {
                    switch (i) {
                        case 0:
                            moneyType = ModelTypeUtil.CardType.GULDEN;
                            break;
                        case 2:
                            moneyType = ModelTypeUtil.CardType.DIRHAM;
                            break;
                        case 3:
                            moneyType = ModelTypeUtil.CardType.DENAR;
                            break;
                        case 4:
                            moneyType = ModelTypeUtil.CardType.DUKATEN;
                            break;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Remove {@link BuildingTile} from the building yard.
     */
    private void removeBuildingFromYard() {
        for (int i = 0; i < gameInstance.getBuildingYard().length; i++) {
            if (gameInstance.getBuildingYard()[i] != null && gameInstance.getBuildingYard()[i].equals(buildingTile))
                gameInstance.getBuildingYard()[i] = null;
        }
    }

    /**
     * Remove {@link MoneyCard}s from the {@link Player}.
     */
    private void removeMoneyFromPlayer() {
        for (MoneyCard card : moneycards)
           {
               getCurrentPlayer().getMoneyCards().remove(card);
               gameInstance.getGraveyard().add(card);
           }
    }

    @Override
    public String printTurn() {
        String print = "";
        if (buildingTile != null) {
            print += validate()? "" : "(invalid) ";
            String cards = Arrays.stream(moneycards).map(MoneyCard::toString).collect(Collectors.joining(", "));
            print += "Kaufe das Gebäudeplättchen " + buildingTile.toString() + " mit den Karten " + cards + " und lege es in die Reserve";
        }
        return print;
    }
}
