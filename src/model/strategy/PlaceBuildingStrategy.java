package model.strategy;

import model.BoardPosition;
import model.GameInstance;
import model.board.BuildingTile;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

/**
 * Strategy to place a {@link BuildingTile}.
 */
public class PlaceBuildingStrategy extends GameTurn {

    BuildingTile buildingTile;
    BuildingTile[][] oldBoard;
    BoardPosition boardPos;
    GameInstance gameInstance;
    Player oldPlayer;

    /**
     * Constructor.
     *
     * @param currentPlayer The current {@link Player}.
     * @param buildingTile  The {@link BuildingTile} to be placed.
     * @param boardPos      The {@link BoardPosition} for the {@link BuildingTile}.
     */
    public PlaceBuildingStrategy(Player currentPlayer, BuildingTile buildingTile, BoardPosition boardPos, GameInstance gameInstance) {
        super(currentPlayer);
        this.buildingTile = buildingTile;
        this.boardPos = boardPos;
        this.gameInstance = gameInstance;
    }

    @Override
    /**
     * Places a building on the gameboard
     */
    public void doTurn() {

        oldPlayer = getCurrentPlayer();
        BuildingTile[][] board = getCurrentPlayer().getGameBoard().getBoard();
        oldBoard = board.clone();
        for(int i = 0; i < oldBoard.length; i++)
            oldBoard[i] = oldBoard[i].clone();

        if (validate()) {
            getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()] = buildingTile;
            gameInstance.setBusy(true);
        }
    }

    @Override
    public void unDoTurn()
    {
        getCurrentPlayer().getGameBoard().setBoard(oldBoard);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setBusy(false);
    }

    @Override
    /**
     * 1. Check if place is empty 2. Check if place is valid (with util)
     */
    public boolean validate() {
        if (getCurrentPlayer().getGameBoard().getBoard()[boardPos.getX()][boardPos.getY()] != null)
            return false;
        if (!GameLogicUtil.validField(getCurrentPlayer().getGameBoard(), buildingTile, boardPos))
            return false;
        return true;
    }

    @Override
    public String printTurn() {
        String print = "";
        if (buildingTile != null) {
            print += validate()? "" : "(invalid) ";
            print += "Baue das Gebäudeplättchen " + buildingTile.toString() + " an die Position " + boardPos.toString();
        }
        return print;
    }
}
