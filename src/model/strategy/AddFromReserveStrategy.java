package model.strategy;

import model.BoardPosition;
import model.GameInstance;
import model.board.BuildingTile;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

import java.util.ArrayList;

/**
 * <h1>Strategy to add a reserved {@link BuildingTile} to the gameboard</h1>
 */
public class AddFromReserveStrategy extends GameTurn {
    BuildingTile buildingTile;
    int xValue;
    int yValue;

    ArrayList<BuildingTile> oldReserve;
    BuildingTile[][] oldGameBoard;
    GameInstance gameInstance;
    Player oldPlayer;

    /**
     * Constructor.
     *
     * @param currentPlayer Player who wants to place the reserved {@link BuildingTile}
     * @param buildingTile  The reserved {@link BuildingTile}
     * @param gameInstance  The {@link GameInstance} of the game.
     * @param boardPos      {@link BoardPosition} where the reserved {@link BuildingTile} will be placed.
     */
    public AddFromReserveStrategy(Player currentPlayer, BuildingTile buildingTile, GameInstance gameInstance, BoardPosition boardPos) {
        super(currentPlayer);
        this.buildingTile = buildingTile;
        xValue = boardPos.getX();
        yValue = boardPos.getY();
        this.gameInstance = gameInstance;
    }

    @Override
    /**
     * Adds a building tile from the reserve to the gameboard
     */
    public void doTurn() {
        oldReserve = new ArrayList<>(getCurrentPlayer().getReserveBuildings());
        BuildingTile[][] current = getCurrentPlayer().getGameBoard().getBoard();
        oldGameBoard = current.clone();
        for(int i = 0; i < oldGameBoard.length; i++)
            oldGameBoard[i] = oldGameBoard[i].clone();
        oldPlayer = getCurrentPlayer();

        if (validate()) {
            getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue] = buildingTile;
            getCurrentPlayer().getReserveBuildings().remove(buildingTile);
            gameInstance.setBusy(true);
        }
    }

    /**
     * Undo this turn
     */
    @Override
    public void unDoTurn() {
        oldPlayer.setReserveBuildings(oldReserve);
        oldPlayer.getGameBoard().setBoard(oldGameBoard);
        gameInstance.setCurrentPlayer(oldPlayer);
        gameInstance.setBusy(false);
    }

    @Override
    /**
     * 1. Check if Tile is in reserve
     * 2. Check if GameBoard is valid after placing
     */
    public boolean validate() {
        if (!getCurrentPlayer().getReserveBuildings().contains(buildingTile))
            return false;
        if (!GameLogicUtil.validField(getCurrentPlayer().getGameBoard(), buildingTile, new BoardPosition(xValue, yValue))) {
            System.out.println("Fail");
            return false;
        }
        return true;
    }

    @Override
    public String printTurn() {
        String print = "";
        if (buildingTile != null) {
            print += validate()? "" : "(invalid) ";
            print += "Nehme das Gebäudeplättchen " + buildingTile.toString() + " vom Reservefeld und lege es an die Position " + xValue + " " + yValue;
        }
        return print;
    }
}
