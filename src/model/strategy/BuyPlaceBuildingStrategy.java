package model.strategy;

import component.mainwindow.HandCard;
import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameTurn;
import model.game.Player;
import util.GameLogicUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * <h1>Strategy to buy and place a {@link BuildingTile}</h1>
 */
public class BuyPlaceBuildingStrategy extends GameTurn {

    GameInstance gameInstance;
    private MoneyCard[] moneycards;
    BuildingTile buildingTile;
    ModelTypeUtil.CardType moneyType;
    BuildingTile[] oldBuildingYard;
    List<MoneyCard> oldMoneyCards;
    private int xValue, yValue;
    Stack<MoneyCard> oldGYard;

    Player oldPlayer;
    /**
     * Constructor.
     *
     * @param buildingTile {@link BuildingTile} to be bought and placed.
     * @param gameInstance {@link GameInstance} of the game.
     * @param boardPos     {@link BoardPosition} for the bought
     *                     {@link BuildingTile}.
     * @param moneyCards   The {@link MoneyCard}s used for the transaction.
     */
    public BuyPlaceBuildingStrategy(BuildingTile buildingTile, GameInstance gameInstance, BoardPosition boardPos,
                                    MoneyCard... moneyCards) {
        super(gameInstance.getCurrentPlayer());
        this.gameInstance = gameInstance;
        this.moneycards = moneyCards;
        this.buildingTile = buildingTile;
        this.xValue = boardPos.getX();
        this.yValue = boardPos.getY();
        this.oldGYard = new Stack<>();
    }

    /**
     * Constructor.
     *
     * @param tile         {@link BuildingTile} to be bought and placed.
     * @param gameInstance {@link GameInstance} of the game.
     * @param position     {@link BoardPosition} for the bought
     *                     {@link BuildingTile}.
     * @param selected     {@link HandCard}s used for the transaction.
     */
    public BuyPlaceBuildingStrategy(BuildingTile tile, GameInstance gameInstance, BoardPosition position,
                                    List<HandCard> selected) {
        super(gameInstance.getCurrentPlayer());
        this.gameInstance = gameInstance;
        this.moneycards = new MoneyCard[selected.size()];
        for (int i = 0; i < this.moneycards.length; i++) {
            this.moneycards[i] = selected.get(i).getCard();
        }
        this.buildingTile = tile;
        this.xValue = position.getX();
        this.yValue = position.getY();
        this.oldGYard = new Stack<>();
    }

    @Override
    /**
     * Buys and places a building
     */
    public void doTurn() {
        oldBuildingYard = gameInstance.getBuildingYard().clone();
        oldMoneyCards = new ArrayList<>(getCurrentPlayer().getMoneyCards());
        if(!gameInstance.getGraveyard().isEmpty())
            oldGYard.addAll(gameInstance.getGraveyard());
        oldPlayer = getCurrentPlayer();

        if (validate()) {
            removeBuildingFromYard();
            removeMoneyFromPlayer();
            getCurrentPlayer().getGameBoard().getBoard()[xValue][yValue] = buildingTile;
            gameInstance.setBusy(!canBuyPerfec());
        }

    }

    /**
     * Undo the turn
     */
    @Override
    public void unDoTurn() {
        gameInstance.setBuildingYard(oldBuildingYard);
        oldPlayer.setMoneyCards(oldMoneyCards);
        oldPlayer.getGameBoard().getBoard()[xValue][yValue] = null;
        gameInstance.setGraveyard(oldGYard);
        gameInstance.setBusy(false);

        gameInstance.setCurrentPlayer(oldPlayer);
    }

    /**
     * Validate if the {@link BuildingTile} is present in the building yard and if
     * the {@link model.game.Player} has enough money.
     */
    @Override
    public boolean validate() {
        if (isInBuildingYard())
            if (GameLogicUtil.validField(getCurrentPlayer().getGameBoard(), buildingTile, new BoardPosition(xValue, yValue)))
                return hasEnoughMoney();
        return false;
    }


    /**
     * Determines if player has enough {@link MoneyCard} to buy the
     * {@link BuildingTile}
     */
    private boolean hasEnoughMoney() {
        return calculateBuyingPower() >= buildingTile.getPrice();
    }

    /**
     * Determines if {@link model.game.Player} can buy without a loss.
     */
    private boolean canBuyPerfec() {
        return calculateBuyingPower() == buildingTile.getPrice();
    }

    /**
     * Calculates buying power of the player.
     */
    private int calculateBuyingPower() {
        return Arrays.stream(moneycards).mapToInt(value -> value.getValue()).sum();
    }

    /**
     * Checks if the {@link BuildingTile} is present in the building yard.
     */
    private boolean isInBuildingYard() {
        for (int i = 0; i < gameInstance.getBuildingYard().length; i++) {
            if (gameInstance.getBuildingYard()[i] == null) {
                continue;
            }
            if (gameInstance.getBuildingYard()[i].equals(buildingTile)) {
                switch (i) {
                    case 0:
                        moneyType = ModelTypeUtil.CardType.GULDEN;
                        break;
                    case 1:
                        moneyType = ModelTypeUtil.CardType.DIRHAM;
                        break;
                    case 2:
                        moneyType = ModelTypeUtil.CardType.DENAR;
                        break;
                    case 3:
                        moneyType = ModelTypeUtil.CardType.DUKATEN;
                        break;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Removes the {@link BuildingTile} from the building yard.
     */
    private void removeBuildingFromYard() {
        for (int i = 0; i < gameInstance.getBuildingYard().length; i++) {
            if (gameInstance.getBuildingYard()[i] != null && gameInstance.getBuildingYard()[i].equals(buildingTile))
                gameInstance.getBuildingYard()[i] = null;
        }
    }

    /**
     * Remove the {@link MoneyCard} from the {@link model.game.Player}
     */
    private void removeMoneyFromPlayer() {
        for (MoneyCard card : moneycards){
            getCurrentPlayer().getMoneyCards().remove(card);
            gameInstance.getGraveyard().add(card);
        }
    }

    @Override
    public String printTurn() {
        String print = "";
        if (buildingTile != null) {
            print += validate()? "" : "(invalid) ";
            String cards = Arrays.stream(moneycards).map(MoneyCard::toString).collect(Collectors.joining(", "));
            print += "Kaufe das Gebäudeplättchen " + buildingTile.toString() + " mit den Karten " + cards + " und lege es an die Position " + xValue + " " + yValue;
        }
        return print;
    }
}
