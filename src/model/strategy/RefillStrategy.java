package model.strategy;

import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameTurn;
import model.game.Player;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Strategy to refill the bank and the building yard.
 */
public class RefillStrategy extends GameTurn {

	GameInstance gameInstance;
	MoneyCard[] oldBank;
	BuildingTile[] oldYard;
	Stack<MoneyCard> oldCards;
	Stack<MoneyCard> oldGYard;
	Stack<MoneyCard> moneyStackChange;
	Stack<BuildingTile> buildingStackChange;
	Player oldPlayer;
	boolean isBotTournament;
	/**
	 * Constructor.
	 *  @param currentPlayer The current {@link Player}.
	 * @param gameInstance  The {@link GameInstance} of the game.
	 */
	public RefillStrategy(Player currentPlayer, GameInstance gameInstance) {
		super(currentPlayer);
		this.gameInstance = gameInstance;



		oldCards=new Stack<>();
		oldCards.addAll(gameInstance.getCardStack());
		oldGYard=new Stack<>();
		oldGYard.addAll(gameInstance.getGraveyard());



		moneyStackChange = new Stack<>();
		buildingStackChange = new Stack<>();
		this.isBotTournament=gameInstance.isTournament();
	}

	/**
	 * Refills the Bank and the building yard inbetween players
	 */
	@Override
	public void doTurn() {
		oldBank = gameInstance.getBank();
		oldYard = gameInstance.getBuildingYard();
		oldPlayer = getCurrentPlayer();
		MoneyCard[] bank = oldBank.clone();
		BuildingTile[] yard = oldYard.clone();

		Stack<MoneyCard> moneyStack = gameInstance.getCardStack();
		Stack<BuildingTile> buildingStack = gameInstance.getBuildingStack();

		for (int i = 0; i < bank.length; i++) {
			if (moneyStack.empty()) {
				Stack<MoneyCard> refillList=gameInstance.getGraveyard();
				if (!isBotTournament){
					Collections.shuffle(refillList);
				}
				gameInstance.setCardStack(refillList);
				gameInstance.setGraveyard(new Stack<>());
				moneyStack=refillList;
			}
			if (bank[i] == null) {
				bank[i] = moneyStack.pop();
				this.moneyStackChange.push(bank[i]);
				if (bank[i].getType() == ModelTypeUtil.CardType.WERTUNG) {
					int evalNum=bank[i].getValue();
					bank[i] = moneyStack.pop();
					this.moneyStackChange.push(bank[i]);
					gameInstance.setEval(evalNum);
				}
			}
		}
		for (int i = 0; i < yard.length; i++) {
			if (buildingStack.empty()  && yard[i] == null ) {
				gameInstance.setLastRound(true);
				for (Player player:gameInstance.getPlayerList()){
					player.setReserveBuildings(new LinkedList<>());
				}

				for (int j=0;j<yard.length;j++){
					if (yard[j]!=null){
						ModelTypeUtil.CardType type= ModelTypeUtil.CardType.values()[j];
						int playerNum=-1;
						int curMax=0;
						int curentPlayerPosition=0;
						for (Player player:gameInstance.getPlayerList()){
							int thisMay=0;
							for (MoneyCard card:player.getMoneyCards()){
								if (card.getType()==type){
									thisMay+=card.getValue();
								}
							}
							if (thisMay>curMax){
								playerNum=curentPlayerPosition;
								curMax=thisMay;
							}
							curentPlayerPosition++;
						}
						if (playerNum>=0){
							gameInstance.getPlayerList().get(playerNum).getReserveBuildings().add(yard[j]);
						}
						yard[j]=null;
					}
				}

				for (Player player:gameInstance.getPlayerList()){
					player.setMoneyCards(new LinkedList<>());
				}
				gameInstance.setBank(bank);
				gameInstance.setBuildingYard(yard);
				gameInstance.setEval(3);
				return;
			} else if (yard[i] == null) {
				yard[i] = buildingStack.pop();
				this.buildingStackChange.push(yard[i]);
			}
		}
		gameInstance.setBuildingYard(yard);
		gameInstance.setBank(bank);
	}

	/**
	 * Undo this turn.
	 */
	@Override
	public void unDoTurn() {
		gameInstance.setBank(oldBank);
		gameInstance.setBuildingYard(oldYard);

		Stack<MoneyCard> moneyStack = gameInstance.getCardStack();
		Stack<BuildingTile> buildingStack = gameInstance.getBuildingStack();

		while (!moneyStackChange.empty()) {
			if (moneyStackChange.peek().getType() == ModelTypeUtil.CardType.WERTUNG) {
				gameInstance.setEval(0);
			}
			moneyStack.push(moneyStackChange.pop());
		}
		while (!buildingStackChange.empty()) {
			buildingStack.push(buildingStackChange.pop());
		}
		gameInstance.setGraveyard(oldGYard);
		gameInstance.setCardStack(oldCards);
		gameInstance.setCurrentPlayer(oldPlayer);
	}

	/**
	 * Check if {@link GameInstance} is null.
	 */
	@Override
	public boolean validate() {
		return gameInstance != null;

	}
	/**
	 *Prints the strategy.
	 */
	@Override
	public String printTurn() {
		return (validate()? "": "(invalid) ") + "Fülle Bank und/oder Bauhof auf.";
	}
}
