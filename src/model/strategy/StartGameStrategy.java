package model.strategy;

import model.game.GameTurn;

/**
 * Just a strategy for the beginning of the game. Used only intern.
 */
public class StartGameStrategy extends GameTurn {

	public StartGameStrategy() {
		super(null);
	}

	@Override
	public void doTurn() {

	}

	@Override
	public void unDoTurn() {

	}

	@Override
	public boolean validate() {
		return true;
	}
	/**
	 *Prints the strategy.
	 */
	@Override
	public String printTurn() {
		return "Starte das Spiel";
	}
}
