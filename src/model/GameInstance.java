package model;

import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameRound;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.EvaluationStrategy;
import org.joda.time.DateTime;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public class GameInstance extends GameElement {

    PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    private MoneyCard[] bank;

    private BuildingTile[] buildingYard;

    private Stack<BuildingTile> buildingStack;

    private Stack<GameTurn> redoStack;


    private Stack<MoneyCard> cardStack;

    private Stack<MoneyCard> graveyard;

    private LinkedList<GameRound> rounds;

    private List<Player> playerList;

    private Player currentPlayer;

    private DateTime gameCreated;
    private EvaluationStrategy evaluationStrategy;

    private boolean busy, lastRound, finish,tournament;
    private int eval;
    private GameTurn lastTurn;

    public GameInstance() {
        gameCreated = DateTime.now();
        lastTurn = null;
        evaluationStrategy=null;
        tournament=false;
        bank = new MoneyCard[4];
        buildingYard = new BuildingTile[4];
        buildingStack = new Stack<>();
        cardStack = new Stack<>();
        graveyard = new Stack<>();
        rounds = new LinkedList<>();
        playerList = new ArrayList<>();
        currentPlayer = null;
        busy = false;
        eval = 0;
        redoStack = new Stack<>();
        finish = false;
        lastRound = false;

    }

    public boolean isTournament() {
        return tournament;
    }

    public void setTournament(boolean tournament) {
        this.tournament = tournament;
    }

    public EvaluationStrategy getEvaluationStrategy() {
        return evaluationStrategy;
    }

    public void setEvaluationStrategy(EvaluationStrategy evaluationStrategy) {
        this.evaluationStrategy = evaluationStrategy;
    }

    public MoneyCard[] getBank() {
        return this.bank;
    }

    public BuildingTile[] getBuildingYard() {
        return buildingYard;
    }

    public Stack<BuildingTile> getBuildingStack() {
        return buildingStack;
    }

    public Stack<MoneyCard> getCardStack() {
        return cardStack;
    }

    public Stack<MoneyCard> getGraveyard() {
        return graveyard;
    }

    public LinkedList<GameRound> getRounds() {
        return rounds;
    }

    public void setRedoStack(Stack<GameTurn> redoStack) {
        this.redoStack = redoStack;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    public boolean isLastRound() {
        return lastRound;
    }

    public void setLastRound(boolean lastRound) {
        this.lastRound = lastRound;
    }

    public boolean isFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public DateTime getGameCreated() {
        return gameCreated;
    }

    public void setBuildingStack(Stack<BuildingTile> buildingStack) {
        this.buildingStack = buildingStack;

    }

    public void setCardStack(Stack<MoneyCard> cardStack) {
        this.cardStack = cardStack;

    }

    public void setGraveyard(Stack<MoneyCard> graveyard) {
        this.graveyard = graveyard;
    }

    public void addUser(Player player) {
        playerList.add(player);
    }

    public void addObserver(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener("theProperty", listener);
    }

    public void trigger() {

        pcs.firePropertyChange("theProperty", new Object(), this);

    }

    public GameTurn getLastTurn() {
        return lastTurn;
    }

    public void setLastTurn(GameTurn lastTurn) {
        this.lastTurn = lastTurn;
    }


    public void setCurrentPlayer(Player player) {
        this.currentPlayer = player;

    }

    public int isEval() {
        return eval;
    }

    public void setEval(int eval) {
        this.eval = eval;
    }

    public void setBank(MoneyCard[] bank) {
        this.bank = bank;
    }

    public void setBuildingYard(BuildingTile[] buildingYard) {
        this.buildingYard = buildingYard;
    }

    public Stack<GameTurn> getRedoStack() {
        return redoStack;
    }

    public GameInstance deepCopy() {
        GameInstance[] arr = multiDeepCopy(1);
        return arr == null ? null : arr[0];
    }

    public GameInstance[] multiDeepCopy(int len) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(this);

            GameInstance[] instances = new GameInstance[len];
            AtomicBoolean err = new AtomicBoolean(false);
            IntStream.range(0, len).forEach(index -> {
                try {
                    ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                    ObjectInputStream ois = new ObjectInputStream(bais);
                    instances[index] = (GameInstance) ois.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    err.set(true);
                }
            });
            return err.get()? null : instances;
        } catch (IOException e) {
            return null;
        }
    }

    public int indexOfCurrentPlayer() {
        if (currentPlayer == null) {
            return -1;
        } else {
            return playerList.indexOf(currentPlayer);
        }
    }


}
