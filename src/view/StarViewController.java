package view;

import component.HiScore;
import component.LoadGame;
import component.MenuButton;
import component.Settings;
import component.playerselect.PlayerSelection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import util.FXUtil;
import util.ConfigUtil;

import java.io.File;
import java.io.IOException;

import static util.FXUtil.projectScale;
import static util.FXUtil.setMusicPlayer;

public class StarViewController extends BorderPane {

    private final Stage maistage;
    @FXML
    Button closeButton, tutorial, newGameButton,settings,highScore,loadButton;

    public StarViewController(Stage maistage, ConfigUtil.Configuration config) {
        this.maistage = maistage;
        init(true);
    }

    public void init(boolean first) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/layout/StartWindow.fxml"));

        if (first) {
            BackgroundSize bSize = new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false);
            this.setBackground(new Background(new BackgroundImage(new Image("/res/img/background/WallpaperMenue.png"), BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.CENTER, bSize)));
            InnerShadow innerShadow = new InnerShadow();
            innerShadow.setColor(Color.BLACK);
            innerShadow.setWidth(75);
            this.setEffect(innerShadow);
        }

        loader.setController(this);


        try {
            BorderPane pane = loader.load();
            projectScale(pane);
            setButtonFunction();
            pane.setPadding(new Insets(50, 0, 0, 0));
            VBox logo = new VBox(new ImageView(new Image("res/img/LogoAlhabraCut.png", 240 * 2, 250, false, false)), pane);
            logo.setAlignment(Pos.CENTER);
            this.setCenter(projectScale(logo));
            setMusicPlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setButtonFunction() {
        closeButton.setOnAction(action -> {
            System.exit(0);
        });
        loadButton.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            this.setCenter(new LoadGame(this));
        });
        tutorial.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            WebView view = new WebView();
            String url = getClass().getResource("/docs/site/index.html").toExternalForm();
            view.getEngine().load(url);

            view.setMaxWidth(1200);
            view.setMaxWidth(800);
            view.setMinWidth(1200);
            view.setMinWidth(800);
            MenuButton backButton = new MenuButton("Zurück");
            backButton.setOnAction(backAction -> {
                init(false);
            });
            VBox vBox = new VBox(view, backButton);
            vBox.setAlignment(Pos.CENTER);
            this.setCenter(vBox);
        });
        newGameButton.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            this.setCenter(new PlayerSelection(this));
        });
        settings.setOnAction(action->{
            FXUtil.setMenuButtonSound();
            this.setCenter(new Settings(this));
        });
        highScore.setOnAction(action->{
            FXUtil.setMenuButtonSound();
            this.setCenter(new HiScore(this));
        });
    }

    public Stage getMaistage() {
        return maistage;
    }
}
