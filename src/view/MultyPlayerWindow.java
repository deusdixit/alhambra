package view;

import component.ProjectBackButton;
import component.ProjectButton;
import component.WindowBanner;
import controller.NetworkController;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import util.FXUtil;

import static util.FXUtil.projectScale;

@SuppressWarnings("PMD")

public class MultyPlayerWindow extends BorderPane {
    WorkerThread thread;
    StarViewController starViewController;
    StackPane stackPane;
    boolean conecting, conected, initial;

    MultyPlayerWindow(StarViewController starViewController) {
        conecting = false;
        conected = false;
        initial = true;

        this.starViewController = starViewController;
        this.setCenter(getCenterPane());

        this.getCenter().setScaleY(projectScale());
        this.getCenter().setScaleX(projectScale());


        Text text = new Text("Netzwerk Spiel Menue");
        text.setFont(new Font(64));
        VBox vBox = new VBox(text);
        vBox.setAlignment(Pos.TOP_CENTER);
        ProjectBackButton backButton = new ProjectBackButton();

        backButton.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            this.starViewController.init(false);
        });


        HBox hBox = new HBox(backButton);
        hBox.setAlignment(Pos.TOP_LEFT);
        StackPane stackPane = new StackPane();
        this.setTop(stackPane);
        stackPane.getChildren().addAll(new WindowBanner("Netzwerk Spiel"), hBox);
    }

    private StackPane getCenterPane() {
        stackPane = new StackPane();
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(50);
        stackPane.setEffect(innerShadow);

        ListView<String> listView = new ListView<>();
        listView.getItems().add("Klicke auf suche um nach spielen zu suchen");
        ProjectButton hostButton, directButton, refreshButton;

        hostButton = new ProjectButton("Spiel erstellen ");
        directButton = new ProjectButton("Manuell");
        directButton.setOnAction(action -> {
            if (!conecting) {
                conecting = true;

                ProjectButton cancel = new ProjectButton("Abbrechen");
                ProjectButton tryButton = new ProjectButton("Verbinden");
                cancel.setOnAction(action2 -> {
                    FXUtil.setMenuButtonSound();
                    stackPane.getChildren().remove(stackPane.getChildren().size() - 1);
                    conecting = false;
                });

                Text text = new Text("Ziel IP-Adresse eingeben:");
                TextField textField = new TextField();
                text.setFont(new Font(36));
                HBox hBox = new HBox(cancel, tryButton);
                hBox.setAlignment(Pos.CENTER);
                hBox.setSpacing(10);
                VBox content = new VBox(text, textField, hBox);
                content.setAlignment(Pos.CENTER);
                content.setSpacing(5);
                content.setStyle("-fx-background-color: #e8d2a1;-fx-pref-height: 300;-fx-pref-width: 500;-fx-max-height: 300;-fx-max-width: 500");

                stackPane.getChildren().add(content);
            }
        });
        refreshButton = new ProjectButton("Suchen");

        refreshButton.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            initial = false;
            listView.getItems().clear();
            listView.getItems().add("Suche nach spielen");
            thread = new WorkerThread(listView);
            thread.start();
        });
        HBox buttonBar = new HBox(hostButton, directButton, refreshButton);
        buttonBar.setAlignment(Pos.CENTER);
        buttonBar.setSpacing(10);

        listView.setOnMouseClicked(event -> {
            FXUtil.setMenuButtonSound();
            if (!conecting && !initial) {
                conecting = true;

                ProjectButton projectButton = new ProjectButton("Abbrechen");
                projectButton.setOnAction(action -> {
                    stackPane.getChildren().remove(stackPane.getChildren().size() - 1);
                    conecting = false;
                });
                projectButton.setPrefHeight(50);
                projectButton.setPrefWidth(200);
                Text text = new Text("Verbinde zu server\n" + listView.getSelectionModel().getSelectedItem());
                text.setFont(new Font(36));
                VBox content = new VBox(text, projectButton);
                content.setAlignment(Pos.CENTER);
                content.setSpacing(5);
                content.setStyle("-fx-background-color: #e8d2a1;-fx-pref-height: 300;-fx-pref-width: 500;-fx-max-height: 300;-fx-max-width: 500");

                stackPane.getChildren().add(content);
            }

        });

        listView.setStyle(" -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;");
        listView.setMaxWidth(800);
        listView.setMaxHeight(450);
        VBox center = new VBox(buttonBar, listView);
        center.setSpacing(5);
        center.setAlignment(Pos.CENTER);
        stackPane.getChildren().add(center);
        return stackPane;

    }

    public class WorkerThread extends Thread {

        private ListView<String> list = null;

        public WorkerThread(ListView<String> list) {
            setDaemon(true);
            this.list = list;
        }

        @Override
        public void run() {


            // UI updaten
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    NetworkController.fillList(list.getItems());
                }
            });
        }

    }


}
