package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import util.ConfigUtil;
import util.ConfigUtil.Configuration;
import view.StarViewController;

public class Main extends Application {
  @Override
  public void start(Stage primaryStage) {
    try {
      Configuration config= ConfigUtil.load();
      StarViewController starViewController = new StarViewController(primaryStage,config);

      Scene scene = new Scene(starViewController, 400, 200);
      //scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setMaximized(true);
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.setResizable(false);

        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);
        primaryStage.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    launch(args);
  }
}
