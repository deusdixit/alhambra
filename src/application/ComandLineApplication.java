/*Hauptklasse und GUI zur kontrolle und interaktion mit dem eigentlichen Client
 * */

package application;

import controller.BoardLogicControllerUtil;
import controller.MainLogicController;
import model.GameInstance;
import model.ModelTypeUtil;
import model.game.Bot;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.StartGameStrategy;
import util.GameInstanceUtil;
import util.exception.InvalidGameTurnExecption;
import util.exception.NoMoneyException;

import java.util.Scanner;

import static util.ApplicationUtil.*;

@SuppressWarnings("PMD")
public class ComandLineApplication {
    static final float version = 0.1f;
    static boolean running = true; // Exit Variable
    static MainLogicController game;
    static Scanner scanner = new Scanner(System.in); // Befahleingabe

    public static void main(String[] args)
    {
        head();
        System.out.println("Was soll  erledigt werden ?");
        while (running) {
            run();
        }
    }


    static void run()
    {
        String in = "";

        in = scanner.nextLine();

        String[] inArr = in.split(" ");
        switch (inArr[0]) {
            case "info":
                if (game!=null){
                    GameInstanceUtil.printStatistic(game.getGameInstance());
                }else {
                    System.out.println("Erstelle zuerst ein neues Spiel");
                }
                break;
            case "current":
            case "cur":
                if (game!=null){
                    GameInstanceUtil.printPlayerDetail(game.getGameInstance().getCurrentPlayer());
                }else {
                    System.out.println("Erstelle zuerst ein neues Spiel");
                }
                break;
            case "next":
                if (game!=null){
                            if (game.getGameInstance().getCurrentPlayer() instanceof Bot){
                                GameTurn turn= ((Bot)game.getGameInstance().getCurrentPlayer()).getNextTurn();
                                System.out.println("Bot "+turn.getCurrentPlayer().getName() +" führt "+turn.toString()+" aus.");
                                try {
                                    game.execute(turn);
                                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                                    invalidGameTurnExecption.printStackTrace();
                                }
                                System.out.println("Bot hat zug ausgeführt");
                            }else {
                                System.out.println("Menschlicher Sieler an der Reihe");
                            }
                }else {
                    System.out.println("Erstelle zuerst ein neues Spiel");
                }
                break;
            case "start":
            case "new":
                newBotDialog();
            break;
            case "exit":
            case "quit":
                running = false;
                System.out.println("Bis zum naechsten mal");
                scanner.close();
                System.exit(0);
                break;
            case "":
                System.out.println();
                break;
            case "help":
            case "Help":
                help();
                break;
            default:
                System.err.println("Falsche eingabe!!!\nhelp fuer hilfe");
                break; // error
        }
    }

    private static void newBotDialog() {
            String in="";
            Integer integer=null;
            while (integer==null){
                System.out.println("Wie viele bots sollen dem Spiel hinzugefügt werden ? [2-6]");
                in=scanner.nextLine();
                try {
                    integer=Integer.parseInt(in);
                }catch (Exception e){ }
            }
            GameInstance gameInstance=new GameInstance();
            game = new MainLogicController(gameInstance);

            Player [] players=new Player[integer];
            for (int i=0;i<integer;i++){
                players[i]=new Bot("Bot_"+i, ModelTypeUtil.PlayerColor.values()[i],game.getGameLogicController());
            }
        try {
            BoardLogicControllerUtil.initBoard(gameInstance,players);
            game.execute(new StartGameStrategy());
        } catch (NoMoneyException | InvalidGameTurnExecption e) {
            e.printStackTrace();
        }
        System.out.println("Alles fertig");
    }


}
