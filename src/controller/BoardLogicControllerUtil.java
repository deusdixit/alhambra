package controller;

import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameRound;
import model.game.Player;
import util.GameInstanceUtil;
import util.exception.NoMoneyException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class BoardLogicControllerUtil {
	public static void initBoard(GameInstance gameInstance, Player... players) throws NoMoneyException {

		initiateGameInstance(gameInstance, players);

		Stack<BuildingTile> buildings = GameInstanceUtil.generateBuildingTile();
		Stack<MoneyCard> money = GameInstanceUtil.generateMoneyCards();

		Collections.shuffle(buildings);
		Collections.shuffle(money);

		initPlayerCards(gameInstance, money);

		initMoneyStack(money);

		gameInstance.setCardStack(money);
		gameInstance.setBuildingStack(buildings);
		gameInstance.setCurrentPlayer(getStartingPlayer(gameInstance.getPlayerList()));
		GameRound firstRound = new GameRound();
		gameInstance.getRounds().add(firstRound);

		// GameInstanceUtil.printStatistic(gameInstance);
	}

    public static void initBoard(GameInstance gameInstance,Stack<BuildingTile> buildings,Stack<MoneyCard> money, Player... players) throws NoMoneyException {
	initiateGameInstance(gameInstance, players);
	
		
	initPlayerCards(gameInstance, money);

	
	gameInstance.setCardStack(money);
	gameInstance.setBuildingStack(buildings);
	gameInstance.setCurrentPlayer(getStartingPlayer(gameInstance.getPlayerList()));
	GameRound firstRound = new GameRound();
	gameInstance.getRounds().add(firstRound);

	}

	private static Player getStartingPlayer(List<Player> playerList) {
		LinkedList<Player> copy = new LinkedList<>(playerList);
		copy.sort((Player player1, Player player2) -> comparePlayers(player1, player2));
		return copy.get(0);
	}

	private static int comparePlayers(Player player1, Player player2) {
		int size1 = player1.getMoneyCards().size();
		int size2 = player2.getMoneyCards().size();
		if (size1 == size2) {
			int balance1 = getBalance(player1);
			int balance2 = getBalance(player2);
			if (balance1 == balance2) {
				return 0;
			} else {
				return Integer.compare(balance1, balance2);
			}
		} else {
			return Integer.compare(size1, size2);
		}
	}

	private static int getBalance(Player player) {
		int result = 0;
		for (MoneyCard mCard : player.getMoneyCards()) {
			result += mCard.getValue();
		}
		return result;
	}

	private static void initMoneyStack(Stack<MoneyCard> money) throws NoMoneyException {
		if (!money.empty()) {
			ArrayList<MoneyCard> list = new ArrayList<>();

			while (!money.empty()) {
				list.add(money.pop());
			}

			int length = list.size();

			int pos01 = ((int) Math.ceil(length / 5)) * 2;
			int pos02 = (((int) Math.ceil(length / 5)) * 4);

			list.add(pos01 - 1, new MoneyCard(1, ModelTypeUtil.CardType.WERTUNG));
			list.add(pos02, new MoneyCard(2, ModelTypeUtil.CardType.WERTUNG));

			Collections.reverse(list);

			for (MoneyCard card : list) {
				money.push(card);
			}
		} else {
			throw new NoMoneyException();
		}

	}

	private static void initPlayerCards(GameInstance gameInstance, Stack<MoneyCard> money) {
		MoneyCard current;
		for (Player player : gameInstance.getPlayerList()) {
			ArrayList<MoneyCard> cards = new ArrayList<>();
			int value = 0;
			while (value < 20) {
				current = money.pop();
				cards.add(current);
				value += current.getValue();
			}
			player.setMoneyCards(cards);
		}
	}

	private static void initiateGameInstance(GameInstance gameInstance, Player... players) throws NoMoneyException {
		if (players == null || players.length == 0) {
			gameInstance.addUser(new Player("Doris", ModelTypeUtil.PlayerColor.BLUE));
			gameInstance.addUser(new Player("Niels", ModelTypeUtil.PlayerColor.RED));

		} else {
			for (Player player : players) {
				gameInstance.addUser(player);
			}
		}

	}
}
