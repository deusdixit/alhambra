package controller.ai;

import controller.GameLogicController;
import model.BoardPosition;
import model.GameInstance;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.GameTurn;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.BuyReserveBuildingStrategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This AI Buys a building whenever he can
 */
public class BuyAIController extends AIController {
    /**
     * Constructs a new object.
     */
    public BuyAIController(Bot bot, GameLogicController gameLogicController) {
        super(bot,gameLogicController);

    }

    @Override
    public List<GameTurn> think() {
        GameInstance gameInstance = gameLogicController.getMainLogicController().getGameInstance();

        if (gameInstance.isLastRound()) {
            return putLast();
        }

        List<GameTurn> gameTurns = new ArrayList<>(4);
        AIBuyUtil.CanBuyPerfectReturn canBuyPerfectReturn = AIBuyUtil.canBuyPerfect(
                getBot(), gameLogicController);

        if (canBuyPerfectReturn.bestFitReturn != null && canBuyPerfectReturn.tile != null) {

            Point point = AIUtil.placeBuildingMaxWallLen(canBuyPerfectReturn.tile, gameInstance);
            if(point != null)
            {
                System.out.println("Placing and buying tile");
                BuyPlaceBuildingStrategy buyStrategy = new BuyPlaceBuildingStrategy(
                        canBuyPerfectReturn.tile,
                        gameLogicController.getMainLogicController().getGameInstance(),
                        new BoardPosition(point.x, point.y),
                        canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0])
                );
                gameTurns.add(buyStrategy);
            }
            else{
                System.out.println("Placing and reserve tile");
                gameTurns.add(new BuyReserveBuildingStrategy(getBot(),
                        canBuyPerfectReturn.tile,
                        gameLogicController.getMainLogicController().getGameInstance(),
                        (canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0]))));
            }
        } else {
            System.out.println("Robbing bank");
            gameTurns.add(AIUtil.robBank(gameInstance, getBot(), gameInstance.getBank()));
        }
        System.out.println(getBot().getColor().toString());
        System.out.println(gameTurns.get(0).printTurn());
        return gameTurns;
    }
}
