package controller.ai;

import controller.GameLogicController;
import model.BoardPosition;
import model.GameInstance;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.BuyReserveBuildingStrategy;

import java.awt.*;
import java.util.*;
import java.util.List;


/**
 * This is a greedy approach.
 * 1. We buy every building we can perfectly buy
 * 2. We take as much money Cards as possible with the highest value as possible
 * 3. We try to build the biggest Wall
 */


public class NaiveAIController extends AIController {
    public NaiveAIController(Bot bot, GameLogicController gameLogicController){

        super(bot,gameLogicController);
    }

    @Override
    public List<GameTurn> think() {
        GameInstance gameInstance = gameLogicController.getMainLogicController().getGameInstance();

        if (gameInstance.isLastRound()) {
            return putLast();
        }

        List<GameTurn> gameTurns = new ArrayList<>(4);
        AIBuyUtil.CanBuyPerfectReturn canBuyPerfectReturn = AIBuyUtil.canBuyPerfect(getBot(), gameLogicController);
        
        if (canBuyPerfectReturn.canBuyPerfect) {
            
            Point point = AIUtil.placeBuildingMaxWallLen(canBuyPerfectReturn.tile, gameInstance);
            if(point != null)
            {
                BuyPlaceBuildingStrategy buyStrategy = new BuyPlaceBuildingStrategy(
                        canBuyPerfectReturn.tile,
                        gameLogicController.getMainLogicController().getGameInstance(),
                        new BoardPosition(point.x, point.y),
                        canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0])
                );
                gameTurns.add(buyStrategy);
            }
            else{
                gameTurns.add(new BuyReserveBuildingStrategy(getBot(),
                        canBuyPerfectReturn.tile,
                        gameLogicController.getMainLogicController().getGameInstance(),
                        (canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0]))));
            }
        } else {
            gameTurns.add(AIUtil.robBank(gameInstance, getBot(), gameInstance.getBank()));
        }

        return gameTurns;
    }

    /**
     * Returns a game strategy as a tip for a player
     * @param player player to gather the tip for
     * @return game strategy as a tip
     */
    public static GameTurn getTip(Player player, GameInstance gameInstance) {

        AIBuyUtil.CanBuyPerfectReturn canBuyPerfectReturn = AIBuyUtil.canBuyPerfect(player, gameInstance);

        if (canBuyPerfectReturn.canBuyPerfect) {

            Point point = AIUtil.placeBuildingMaxWallLen(canBuyPerfectReturn.tile, gameInstance);
            if(point != null)
            {
                return new BuyPlaceBuildingStrategy(
                        canBuyPerfectReturn.tile,
                        gameInstance,
                        new BoardPosition(point.x, point.y),
                        canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0])
                );
            }
            else{
                return (new BuyReserveBuildingStrategy(player,
                        canBuyPerfectReturn.tile,
                        gameInstance,
                        (canBuyPerfectReturn.bestFitReturn.moneyCards.toArray(new MoneyCard[0]))));
            }
        } else {
            return (AIUtil.robBank(gameInstance, player, gameInstance.getBank()));
        }

    }

}
