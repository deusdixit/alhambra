package controller.ai;

import controller.GameLogicController;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.game.Player;

public class AIBuyUtil {

    /**
     * Checks if you can buy a card perfectly
     *
     * @return quad with needed data (Variable names explain enough)
     */
    public static CanBuyPerfectReturn canBuyPerfect(Player player, GameLogicController gameLogicController) {
        return canBuyPerfect(player, gameLogicController.getMainLogicController().getGameInstance());
    }

    /**
     * Checks if you can buy a card perfectly
     *
     * @return quad with needed data (Variable names explain enough)
     */
    public static CanBuyPerfectReturn canBuyPerfect(Player player, GameInstance gameInstance) {

        AIUtil.BestFitReturn bestFitReturn = null;
        BuildingTile tile = null;
        boolean canBuyPerfect = false;
        ModelTypeUtil.CardType cardType = null;

        for (int i = 0; i < 4; i++) {
            tile = gameInstance.getBuildingYard()[i];
            if(tile == null)
                continue;
            switch (i) {
                case 0:
                    bestFitReturn = AIUtil.bestFit(player, tile.getPrice(), ModelTypeUtil.CardType.GULDEN);
                    cardType = ModelTypeUtil.CardType.GULDEN;
                    break;
                case 1:
                    bestFitReturn = AIUtil.bestFit(player, tile.getPrice(), ModelTypeUtil.CardType.DIRHAM);
                    cardType = ModelTypeUtil.CardType.DIRHAM;
                    break;
                case 2:
                    bestFitReturn = AIUtil.bestFit(player, tile.getPrice(), ModelTypeUtil.CardType.DENAR);
                    cardType = ModelTypeUtil.CardType.DENAR;
                    break;
                case 3:
                    bestFitReturn = AIUtil.bestFit(player, tile.getPrice(), ModelTypeUtil.CardType.DUKATEN);
                    cardType = ModelTypeUtil.CardType.DUKATEN;
                    break;
                default:
                    bestFitReturn = null;
                    break;
            }
            if (bestFitReturn != null) {
                if (tile.getPrice() == bestFitReturn.value) {
                    canBuyPerfect = true;
                    break;
                }
            }
        }

        return new CanBuyPerfectReturn(bestFitReturn, tile, cardType, canBuyPerfect);
    }

    public static class CanBuyPerfectReturn {
        AIUtil.BestFitReturn bestFitReturn;
        BuildingTile tile;
        ModelTypeUtil.CardType cardType;
        boolean canBuyPerfect;

        public CanBuyPerfectReturn(AIUtil.BestFitReturn bestFitReturn, BuildingTile tile, ModelTypeUtil.CardType cardType, boolean canBuyPerfect) {
            this.bestFitReturn = bestFitReturn;
            this.tile = tile;
            this.canBuyPerfect = canBuyPerfect;
            this.cardType = cardType;
        }
    }
}
