package controller.ai;

import controller.GameLogicController;
import javafx.util.Pair;
import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.PlaceBuildingStrategy;
import util.GameLogicUtil;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AIController {
    protected GameLogicController gameLogicController;
    private Bot bot;

    AIController(Bot bot, GameLogicController gameLogicController) {
        this.gameLogicController = gameLogicController;
        this.bot = bot;
    }

    public abstract List<GameTurn> think();

    protected Bot getBot() {
        return bot;
    }

    protected List<GameTurn> putLast() {
        GameInstance gameInstance = gameLogicController.getMainLogicController().getGameInstance();
        List<GameTurn> placeTurns = new ArrayList<>();

        for (BuildingTile tile : getBot().getReserveBuildings()) {
            Point point = AIUtil.placeBuildingMaxWallLen(tile, gameInstance);
            if (point != null) {
                placeTurns.add(new PlaceBuildingStrategy(getBot(),
                        tile,
                        new BoardPosition(point),
                        gameInstance));
            }
        }

        return placeTurns;
    }

}
