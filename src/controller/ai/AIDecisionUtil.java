package controller.ai;

import javafx.util.Pair;
import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.RobBankStrategy;
import util.GameLogicUtil;

import java.awt.*;
import java.util.*;
import java.util.List;

public class AIDecisionUtil {

    /**
     * Buys all the buildings that can be bought fitting
     *
     * @param actualGameInstance The initial GameInstance
     * @param clonedGameInstance A clone where to symbolic execute the GameTurns on
     * @return All the new GameTurns tat can be done
     */
    public static List<GameTurn> buyPlaceIfMoneyFits(GameInstance actualGameInstance,
                                                     GameInstance clonedGameInstance) {
        ModelTypeUtil.CardType[] units = new ModelTypeUtil.CardType[]{
                ModelTypeUtil.CardType.DENAR,
                ModelTypeUtil.CardType.DIRHAM,
                ModelTypeUtil.CardType.DUKATEN,
                ModelTypeUtil.CardType.GULDEN
        };

        List<GameTurn> turns = new ArrayList<>();
        List<Pair<AIUtil.BestFitReturn, BuildingTile>> bestFits = new ArrayList<>();

        for (ModelTypeUtil.CardType card : units) {
            BuildingTile tile = AIUtil.getTileByCardFromBuildingYard(actualGameInstance, card);
            if (tile == null) {
                continue;
            }

            AIUtil.BestFitReturn bfr = AIUtil.bestFit(actualGameInstance.getCurrentPlayer(), tile.getPrice(), card);
            if (bfr != null && bfr.value == tile.getPrice()) {
                bestFits.add(new Pair<>(bfr, tile));
            }
        }

        List<Point> pointsChecked = new ArrayList<>();
        // Can buy perfect
        if (!bestFits.isEmpty()) {
            for (Pair<AIUtil.BestFitReturn, BuildingTile> bfr : bestFits) {
                Point point = AIUtil.placeBuildingMaxWallLen(
                        bfr.getValue(),
                        clonedGameInstance); // Best point for maximizing the outer wall length
                MoneyCard[] paymentCards = bfr.getKey().moneyCards.toArray(new MoneyCard[0]);

                if (point == null) { continue; }

                // Execution on clone of the GameInstance
                BuyPlaceBuildingStrategy symbolicTurn = new BuyPlaceBuildingStrategy(
                        bfr.getValue(), clonedGameInstance, new BoardPosition(point), paymentCards);
                if (symbolicTurn.validate() && !pointsChecked.contains(point)) {
                    BuyPlaceBuildingStrategy turnToAdd = new BuyPlaceBuildingStrategy(
                            bfr.getValue(),
                            actualGameInstance,
                            new BoardPosition(point),
                            paymentCards);
                    pointsChecked.add(point);
                    System.out.println("QUEUEING  >>> " + actualGameInstance.getCurrentPlayer().getColor() + " " +
                            turnToAdd.printTurn() + " (buyPlaceIfMoneyFits)");
                    symbolicTurn.doTurn();
                    turns.add(turnToAdd);
                } else {
                    System.out.println("ERROR     >>> " + actualGameInstance.getCurrentPlayer().getColor() +
                            " Invalid: " + symbolicTurn.printTurn() + " (buyPlaceIfMoneyFits)");
                }

                // no return statement, because we bought perfectly and can do another turn
            }
        }

        return turns;
    }

    /**
     * Looks for GameTurns that might maximise the players points for the next WERTUNG
     *
     * @param actualGameInstance The GameInstance the strategy will be executed on
     * @param clonedGameInstance The GameInstance the symbolic execution should be executed on
     * @return A BuyPlaceBuildingStrategy fitting the requirements, otherwise null
     */
    public static BuyPlaceBuildingStrategy buyPlaceRegardingWertung(GameInstance actualGameInstance,
                                                    GameInstance clonedGameInstance) {
        List<ModelTypeUtil.BuildingType> goodToBuy = AIUtil.typesStrategicalGoodToBuy(clonedGameInstance,
                clonedGameInstance.getCurrentPlayer());

        if (!goodToBuy.isEmpty()) {
            BuildingTile minTile = null;
            int minDelta = Integer.MAX_VALUE;
            MoneyCard[] buyCombination= null;

            for (BuildingTile tile: clonedGameInstance.getBuildingYard()) {
                if (tile == null) { continue; }
                if (goodToBuy.contains(tile.getType())) {
                    AIUtil.BestFitReturn bestFitReturn = AIUtil.bestFit(clonedGameInstance.getCurrentPlayer(),
                            tile.getPrice(),
                            AIUtil.getCardByBuildingFromBuildingYard(actualGameInstance, tile));
                    if (bestFitReturn != null && bestFitReturn.value < minDelta) {
                        minDelta = bestFitReturn.value;
                        minTile = tile;
                        buyCombination = bestFitReturn.moneyCards.toArray(new MoneyCard[0]);
                    }
                }
            }

            if (minTile != null) {
                Point point = AIUtil.placeBuildingMaxWallLen(minTile, clonedGameInstance, false);
                if (point == null) { return null; }
                BuyPlaceBuildingStrategy turnToAdd = new BuyPlaceBuildingStrategy(minTile,
                        actualGameInstance,
                        new BoardPosition(point),
                        buyCombination);
                System.out.println("QUEUEING  >>> " + actualGameInstance.getCurrentPlayer().getColor() + " " +
                        turnToAdd.printTurn() + " (buyPlaceRegardingWertung)");
                return turnToAdd;
            }
        }

        return null;
    }

    /**
     * Looks for GameTurns that might maximise the players points for the next WERTUNG regarding the wall length
     *
     * @param actualGameInstance The GameInstance the strategy will be executed on
     * @param clonedGameInstance The GameInstance the symbolic execution should be executed on
     * @return A BuyPlaceBuildingStrategy, where the walls are not blocking too many directions, otherwise null
     */
    public static BuyPlaceBuildingStrategy buyPlaceRegardingWallLength(GameInstance actualGameInstance,
                                                       GameInstance clonedGameInstance) {

        int largestWall = GameLogicUtil.getLargestOuterWallLength(
                clonedGameInstance.getCurrentPlayer().getGameBoard().getBoard());
        Map<ModelTypeUtil.Direction, Integer> oldWallCountMap = GameLogicUtil.countOpenDirections(
                clonedGameInstance.getCurrentPlayer().getGameBoard().getBoard());
        Map<BuyPlaceBuildingStrategy, Integer> strategyWallLengthMap = new HashMap<>();

        // Test all combinations of tiles and places where they can be placed
        for (BuildingTile tile: clonedGameInstance.getBuildingYard()) {
            if (tile == null) { continue; }
            List<BoardPosition> availablePlaces = GameLogicUtil.getAvailablePlaces(
                    clonedGameInstance.getCurrentPlayer().getGameBoard(), tile);
            GameInstance[] localGameInstances = clonedGameInstance.multiDeepCopy(availablePlaces.size());

            // go through all availablePlaces
            for (int index = 0; index < availablePlaces.size(); index++) {
                BoardPosition pos = availablePlaces.get(index);
                GameInstance localGameInstance = localGameInstances[index];

                AIUtil.BestFitReturn bestFitReturn = AIUtil.bestFit(clonedGameInstance.getCurrentPlayer(),
                        tile.getPrice(),
                        AIUtil.getCardByBuildingFromBuildingYard(clonedGameInstance, tile));

                // if bot still has enough money
                if (bestFitReturn != null) {
                    MoneyCard[] buyCombination = bestFitReturn.moneyCards.toArray(new MoneyCard[0]);

                    Player thisBot = localGameInstance.getCurrentPlayer();
                    if (!(thisBot instanceof Bot)) {
                        System.err.println("Cloned GameInstance: Current player is no bot!");
                        return null;
                    }

                    // execute turn on local map
                    BuyPlaceBuildingStrategy testStrategy =
                            new BuyPlaceBuildingStrategy(tile, localGameInstance, pos, buyCombination);
                    if (!testStrategy.validate()) {
                        continue;
                    }
                    testStrategy.doTurn();
                    Map<ModelTypeUtil.Direction, Integer> wallCountMap = GameLogicUtil.countOpenDirections(
                            thisBot.getGameBoard().getBoard());
                    int length = GameLogicUtil.getLargestOuterWallLength(thisBot.getGameBoard().getBoard());

                    // check if it is worth to block specific directions
                    boolean add = true;
                    for (ModelTypeUtil.Direction direction: wallCountMap.keySet()) {
                        if (wallCountMap.get(direction) < oldWallCountMap.get(direction) &&
                                wallCountMap.get(direction) < 1) {
                            if (length < largestWall * 1.5f) {
                                // if the wall length didn't increase significantly, break the testing
                                add = false;
                                break;
                            }
                        }
                    }

                    add = true;
                    if (add) {
                        strategyWallLengthMap.put(
                                new BuyPlaceBuildingStrategy(tile, actualGameInstance, pos, buyCombination), length);
                    }
                }
            }
        }

        // choose longest wall
        if (strategyWallLengthMap.size() > 0) {
            int maxLength = 0;
            BuyPlaceBuildingStrategy maxStrategy = null;

            for (BuyPlaceBuildingStrategy strategy: strategyWallLengthMap.keySet()) {
                if (strategyWallLengthMap.get(strategy) > maxLength) {
                    maxStrategy = strategy;
                }
            }
            if (maxStrategy != null) {
                System.out.println("QUEUEING  >>> " + actualGameInstance.getCurrentPlayer().getColor() + " " +
                        maxStrategy.printTurn() + " (buyPlaceRegardingWallLength)");
                return maxStrategy;
            }
        }

        return null;
    }


    /**
     * Looks for GameTurns where BuildingTiles are bought that others might want buy
     *
     * @param actualGameInstance The GameInstance the strategy will be executed on
     * @param clonedGameInstance The GameInstance the symbolic execution should be executed on
     * @return A BuyPlaceBuildingStrategy fitting the requirements, otherwise null
     */
    public static BuyPlaceBuildingStrategy buyPlaceStealFromOthers(GameInstance actualGameInstance,
                                                                   GameInstance clonedGameInstance) {

        return null;
    }

    /**
     * If possible, pulls money regarding the left over MoneyCards and BuildingTiles, that in the next round can be
     * used to buy tiles perfect
     *
     * @param actualGameInstance The GameInstance the strategy will be executed on
     * @param clonedGameInstance The GameInstance the symbolic execution should be executed on
     * @return A RobBankStrategy fitting the requirements, otherwise null
     */
    public static RobBankStrategy pullMoneyIfCanBuyNextRound(GameInstance actualGameInstance,
                                                             GameInstance clonedGameInstance) {
        List<MoneyCard> remainingMoneyCards = new ArrayList<>();
        Arrays.stream(clonedGameInstance.getBank()).forEach(card ->
        {
            if (card != null) remainingMoneyCards.add(card);
        });
        final int overallSize = remainingMoneyCards.size() * clonedGameInstance.getPlayerList().size();
        GameInstance[] localGameInstances = clonedGameInstance.multiDeepCopy(overallSize);

        for (int moneyIndex = 0; moneyIndex < remainingMoneyCards.size(); moneyIndex++) {
            boolean selected = false;
            for (int playerIndex = 0; playerIndex < clonedGameInstance.getPlayerList().size(); playerIndex++) {
                GameInstance localGameInstance = localGameInstances[
                        moneyIndex * clonedGameInstance.getPlayerList().size() + playerIndex];
                Player thisPlayer = localGameInstance.getCurrentPlayer();
                Player otherPlayer = localGameInstance.getPlayerList().get(playerIndex);

                if (thisPlayer == otherPlayer) { continue; }

                new RobBankStrategy(localGameInstance,
                        localGameInstance.getCurrentPlayer(),
                        remainingMoneyCards.get(moneyIndex))
                        .doTurn();
                localGameInstance.setCurrentPlayer(otherPlayer);

                if (buyPlaceIfMoneyFits(localGameInstance, localGameInstance).isEmpty()) {
                    selected = true;
                    break;
                }
            }
            if (!selected) {
                RobBankStrategy gameTurn = new RobBankStrategy(actualGameInstance,
                        actualGameInstance.getCurrentPlayer(),
                        remainingMoneyCards.get(moneyIndex));
                System.out.println("QUEUEING  >>> " + actualGameInstance.getCurrentPlayer().getColor() + " " +
                        gameTurn.printTurn() + " (pullMoneyIfCanBuyNextRound)");
                return gameTurn;
            }
        }

        return null;
    }

    /**
     * Pulls money
     *
     * @param actualGameInstance The GameInstance the strategy will be executed on
     * @param clonedGameInstance The GameInstance the symbolic execution should be executed on
     * @return A RobBankStrategy fitting the requirements, otherwise null
     */
    public static RobBankStrategy pullMoney(GameInstance actualGameInstance, GameInstance clonedGameInstance) {
        RobBankStrategy gameTurn = AIUtil.robBank(
                actualGameInstance, (Bot) actualGameInstance.getCurrentPlayer(), clonedGameInstance.getBank());
        System.out.println("QUEUEING  >>> " + actualGameInstance.getCurrentPlayer().getColor() + " " +
                gameTurn.printTurn() + " (pullMoney)");
        return gameTurn;
    }
}
