package controller.ai;

import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.Player;
import model.strategy.RobBankStrategy;
import util.GameLogicUtil;

import java.awt.*;
import java.util.*;
import java.util.List;

public class AIUtil {

    /**
     * Takes as much MoneyCards as possible with highest value possible
     */
    public static RobBankStrategy robBank(GameInstance gameInstance, Player player, MoneyCard[] bank) {

        // Clone Array and delete empty (null) slots
        List<MoneyCard> tmp = new ArrayList<>();
        for (MoneyCard card: bank) {
            if (card != null) { tmp.add(card); }
        }
        MoneyCard[] bankClone = tmp.toArray(new MoneyCard[0]);

        Arrays.sort(bankClone, Collections.reverseOrder());
        List<MoneyCard> cardsToRob = new ArrayList<>();

        // Try to get ad many small cards as possible
        int iteration = 0;
        for (int sum = 0; sum <= 5 && iteration < bankClone.length; iteration++) {
            if (bankClone[iteration] == null) {
                continue;
            }

            final int variableIJustAddedBecozPMD = 5;
            if (sum + bankClone[iteration].getValue() > variableIJustAddedBecozPMD) {
                continue;
            }
            sum += bankClone[iteration].getValue();
            cardsToRob.add(bankClone[iteration]);
        }

        // If you can only get one card, get the card with the biggest value
        final int anotherVar = 1;
        if (cardsToRob.size() <= anotherVar) {
            cardsToRob.clear();
            cardsToRob.add(bankClone[bankClone.length - 1]);
        }

        return new RobBankStrategy(gameInstance, player, cardsToRob.toArray(new MoneyCard[0]));
    }

    /**
     * If a tile of the given CardType is buyable, the returned Array contains the possible combinations of MoneyCards
     * to use. Otherwise, it returns null
     *
     * @param price    price of the building tile
     * @param cardType type of money used
     * @return null if you can't buy the tile, else a tuple (price difference, money used)
     */
    protected static BestFitReturn bestFit(Player player,
                                           int price,
                                           ModelTypeUtil.CardType cardType) {
        BestFitReturn[] allPossibilities =
                bestFitHelp(player, cardType, 0, 0,
                        new BestFitReturn[]{
                                new BestFitReturn(0, new ArrayList<>())});

        int bestFitIndex = 0;
        for (int i = 0; i < allPossibilities.length; i++) {
            if (allPossibilities[i].value < price)
                continue;
            if (allPossibilities[bestFitIndex].value < price)
                bestFitIndex = i;
            if (allPossibilities[i].value < allPossibilities[bestFitIndex].value)
                bestFitIndex = i;
        }
        if (price - allPossibilities[bestFitIndex].value > 0)
            return null;
        return allPossibilities[bestFitIndex];
    }

    /**
     * Only God knows how this works. Do not touch
     *
     * @param cardType  CardType to pay with
     * @param start     Enter 0 for initial call
     * @param skipped   Number of cards you have skipped. Enter 0 for initial call.
     * @param oldValues Recursive magic. Enter null for initial call
     * @return Every possible outcome to pay with a money type
     */
    private static BestFitReturn[] bestFitHelp(Player player,
                                              ModelTypeUtil.CardType cardType,
                                              int start,
                                              int skipped,
                                              BestFitReturn[] oldValues) {
        // Only use money with the right type
        if (start >= player.getMoneyCards().size()) {
            return oldValues;
        }

        if (player.getMoneyCards().get(start).getType() != cardType) {
            return bestFitHelp(player, cardType, start + 1, skipped + 1, oldValues);
        }

        // Black magic, only god and i knew what i did there,
        // now only god knows
        BestFitReturn[] values = new BestFitReturn[oldValues.length * 2];
        for (int i = 0; i < oldValues.length; i++) {
            values[i] = new BestFitReturn(player.getMoneyCards().get(start).getValue() + oldValues[i].value,
                    new ArrayList<>(oldValues[i].moneyCards));
            values[i].addCard(player.getMoneyCards().get(start));
            values[i + oldValues.length] = oldValues[i];
        }
        if (start == player.getMoneyCards().size())
            return values;

        return bestFitHelp(player, cardType, start + 1, skipped, values);
    }

    /**
     * Return-type that bundles information in order to determine which building is buyable with the money of a player
     */
    public static class BestFitReturn {
        public int value;
        public List<MoneyCard> moneyCards;

        BestFitReturn(int value, List<MoneyCard> moneyCards) {
            this.value = value;
            this.moneyCards = moneyCards;
        }

        public BestFitReturn addCard(MoneyCard card) {
            moneyCards.add(card);
            return this;
        }

        public BestFitReturn increaseValue(int amount) {
            value += amount;
            return this;
        }
    }

    /**
     *  Returns the BuildingTile buyable with this CardType (money type)
     *
     * @param gameInstance The GamaInstance to look at
     * @param card         The card to search for
     * @return The corresponding Building from the GameInstances BuildYard
     */
    public static BuildingTile getTileByCardFromBuildingYard(GameInstance gameInstance, ModelTypeUtil.CardType card) {
        if (ModelTypeUtil.CardType.WERTUNG.equals(card)) {
            return null;
        }

        ModelTypeUtil.CardType[] units = new ModelTypeUtil.CardType[] {
                ModelTypeUtil.CardType.DENAR,
                ModelTypeUtil.CardType.DIRHAM,
                ModelTypeUtil.CardType.DUKATEN,
                ModelTypeUtil.CardType.GULDEN
        };

        // determine the number of each Element in the enum
        for (int index = 0; index < 4; index++) {
            ModelTypeUtil.CardType selected = ModelTypeUtil.CardType.values()[index];
            if (selected.equals(card)) {
                return gameInstance.getBuildingYard()[index];
            }
        }

        return null;
    }

    /**
     * Returns the CardType, the given BuildingTile is for sell
     *
     * @param gameInstance The GameInstance to regard
     * @param tile         The tile to look for
     * @return The CardType, or null, if the given BuildingTile is not for sell
     */
    public static ModelTypeUtil.CardType getCardByBuildingFromBuildingYard(GameInstance gameInstance, BuildingTile tile) {
        for (int index = 0; index < gameInstance.getBuildingYard().length; index++) {
            ModelTypeUtil.CardType selected = ModelTypeUtil.CardType.values()[index];
            if (gameInstance.getBuildingYard()[index] == null) {
                continue;
            }
            if (gameInstance.getBuildingYard()[index].equals(tile)) {
                return selected;
            }
        }

        return null;
    }

    /**
     * Things that the Player should by regarding the WERTUNGSRUNDE
     *
     * @param gameInstance Actual game instance
     * @param itsMe        The actual player, the method should decide for
     * @return The BuildingTiles the player should buy
     */
    public static List<ModelTypeUtil.BuildingType> typesStrategicalGoodToBuy(GameInstance gameInstance, Player itsMe) {
        List<ModelTypeUtil.BuildingType> typesToBuy = new ArrayList<>();
        List<Player> others = new ArrayList<>(gameInstance.getPlayerList());
        others.remove(itsMe);

        List<BuildingTile> myOwnedTiles = tilesInGameBoard(itsMe.getGameBoard().getBoard());
        myOwnedTiles.addAll(itsMe.getReserveBuildings());
        Map<ModelTypeUtil.BuildingType, Integer> myTileCount = countTypes(myOwnedTiles);

        // Calculate the maximum tiles of the others
        Map<ModelTypeUtil.BuildingType, Integer> maxTilesOthers = new HashMap<>();
        for (Player player: others) {
            List<BuildingTile> playerOwnedTiles = tilesInGameBoard(player.getGameBoard().getBoard());
            playerOwnedTiles.addAll(player.getReserveBuildings());

            Map<ModelTypeUtil.BuildingType, Integer> playerTileCount = countTypes(playerOwnedTiles);
            for (ModelTypeUtil.BuildingType type: playerTileCount.keySet()) {
                if (maxTilesOthers.get(type) != null) {
                    // sets the counter to the maximum
                    maxTilesOthers.put(type, Math.max(maxTilesOthers.get(type), playerTileCount.get(type)));
                } else {
                    maxTilesOthers.put(type, playerTileCount.get(type));
                }
            }
        }

        for (ModelTypeUtil.BuildingType type: ModelTypeUtil.BuildingType.values()) {
            myTileCount.putIfAbsent(type, 0);
            maxTilesOthers.putIfAbsent(type, 0);

            if (!typesToBuy.contains(type) && myTileCount.get(type) + 1 >= maxTilesOthers.get(type)) {
                // it is worth to buy, if the maximum others have only is one more
                typesToBuy.add(type);
            }
        }

        return typesToBuy;
    }

    /**
     * Enumerates all BuildingTiles on a GameBoard
     *
     * @param gameBoard The GameBoard
     * @return A list of all BuildingTiles
     */
    public static List<BuildingTile> tilesInGameBoard(BuildingTile[][] gameBoard) {
        List<BuildingTile> tiles = new ArrayList<>();
        for (BuildingTile[] yArr: gameBoard) {
            for (BuildingTile tile: yArr) {
                if (tile != null) {
                    tiles.add(tile);
                }
            }
        }
        return tiles;
    }

    /**
     * Counts all BuildingTypes in a lint of BuildingTypes
     *
     * @param tiles The list
     * @return A map that associates the BuildingTypes to the number it occurred
     */
    public static Map<ModelTypeUtil.BuildingType, Integer> countTypes(List<BuildingTile> tiles) {
        Map<ModelTypeUtil.BuildingType, Integer> map = new HashMap<>();
        for (BuildingTile tile: tiles) {
            if (tile != null) {
                ModelTypeUtil.BuildingType type = tile.getType();
                if (!map.containsKey(type)) {
                    map.put(type, 1);
                } else {
                    map.put(type, map.get(type) + 1);
                }
            }
        }
        return map;
    }

    /**
     * Determines the point, where to place a given building
     *
     * @param buildingTile BuildingTile to place
     * @return The optimal point
     */
    public static Point placeBuildingMaxWallLen(BuildingTile buildingTile, GameInstance gameInstance) {
        return placeBuildingMaxWallLen(buildingTile, gameInstance, false);
    }

    /**
     * Determines the point, where to place a given building
     *
     * @param buildingTile BuildingTile to place
     * @return The optimal point
     */
    public static Point placeBuildingMaxWallLen(BuildingTile buildingTile, GameInstance gameInstance, boolean avoidBlocking) {
        Player player = gameInstance.getCurrentPlayer();
        List<BoardPosition> places = GameLogicUtil.getAvailablePlaces(player.getGameBoard(), buildingTile);
        int largestWall = GameLogicUtil.getLargestOuterWallLength(
                gameInstance.getCurrentPlayer().getGameBoard().getBoard());
        Map<ModelTypeUtil.Direction, Integer> oldWallCountMap = GameLogicUtil.countOpenDirections(
                gameInstance.getCurrentPlayer().getGameBoard().getBoard());

        if (places.size() == 0) return null;

        int max = 0;
        BoardPosition maxPoint = places.get(0);
        GameInstance[] localGameInstances = gameInstance.multiDeepCopy(places.size());
        for (int index = 0; index < places.size(); index++) {
            BoardPosition point = places.get(index);
            GameInstance localGameInstance = localGameInstances[index];

            BuildingTile[][] clonedBoard = player.getGameBoard().getBoard().clone();
            for (int i = 0; i < clonedBoard.length; i++) {
                clonedBoard[i] = clonedBoard[i].clone();
            }

            int size;
            boolean add = true;
            if (!avoidBlocking) {
                clonedBoard[point.getX()][point.getY()] = buildingTile;
                size = GameLogicUtil.getLargestOuterWallLength(clonedBoard);
            } else {
                Player thisBot = localGameInstance.getCurrentPlayer();
                Map<ModelTypeUtil.Direction, Integer> wallCountMap = GameLogicUtil.countOpenDirections(
                        thisBot.getGameBoard().getBoard());
                size = GameLogicUtil.getLargestOuterWallLength(thisBot.getGameBoard().getBoard());

                for (ModelTypeUtil.Direction direction: wallCountMap.keySet()) {
                    if (wallCountMap.get(direction) < oldWallCountMap.get(direction) &&
                            wallCountMap.get(direction) < 1) {
                        if (size < largestWall * 1.5f) {
                            // if the wall length didn't increase significantly, break the testing
                            add = false;
                            break;
                        }
                    }
                }
            }

            if (!add) { continue; }

            if (size > max) {
                max = size;
                maxPoint = point;
            }
        }

        return new Point(maxPoint.getX(), maxPoint.getY());
    }

    public static <T> int countNotNull(T[] arr) {
        int count = 0;
        for (T item : arr) {
            if (item != null) {
                count++;
            }
        }
        return count;
    }

}
