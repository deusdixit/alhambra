package controller.ai;

import controller.GameLogicController;
import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.game.Bot;
import model.game.GameTurn;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.PlaceBuildingStrategy;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This is a greedy approach.
 * 1. We buy every building we can perfectly buy
 * 2. We take as much money Cards as possible with the highest value as possible
 * 3. We try to build the biggest Wall
 * 4. Tries to optimize walls
 */


public class NaiveOptimalAIController extends AIController {
    HashMap<ModelTypeUtil.BuildingType, Integer> buildingTypeCounter;

    public NaiveOptimalAIController(Bot bot, GameLogicController gameLogicController) {

        super(bot, gameLogicController);
        buildingTypeCounter = new HashMap<>();
    }

    @Override
    public List<GameTurn> think() {
        GameInstance gameInstance = gameLogicController.getMainLogicController().getGameInstance();

        if (gameInstance.isLastRound()) {
            return putLast();
        }

        return buyPlaceOptimal();
    }

    /**
     * Execute some turns
     * 1. Buy fitting tiles and place them
     * 2. Buy and place tiles that are worth to buy regarding the WERTUNG
     * 3. Buy and place tiles in order to increase the max wall length
     * 4. Buy and place tiles that others might like to buy
     * 5. Pull money, so that in the next turn we can buy perfect and the perfect tile fits on the GameBoard
     * 6. Pull money
     *
     * @return A complete list of GameTurn
     */
    protected List<GameTurn> buyPlaceOptimal() {
        GameInstance gameInstance = gameLogicController.getMainLogicController().getGameInstance();
        List<GameTurn> turns = new ArrayList<>();
        GameInstance clonedGameInstance = gameInstance.deepCopy(); // for symbolic execution

        turns.addAll(AIDecisionUtil.buyPlaceIfMoneyFits(gameInstance, clonedGameInstance));

        GameTurn gameTurn = AIDecisionUtil.buyPlaceRegardingWertung(
                gameInstance, clonedGameInstance);
        if (gameTurn != null) {
            turns.add(gameTurn);
            return turns;
        }

        gameTurn = AIDecisionUtil.buyPlaceRegardingWallLength(gameInstance, clonedGameInstance);
        if (gameTurn != null) {
            turns.add(gameTurn);
            return turns;
        }

        gameTurn = AIDecisionUtil.buyPlaceStealFromOthers(gameInstance, clonedGameInstance);
        if (gameTurn != null) {
            turns.add(gameTurn);
            return turns;
        }

        gameTurn = AIDecisionUtil.pullMoneyIfCanBuyNextRound(gameInstance, clonedGameInstance);
        if (gameTurn != null) {
            turns.add(gameTurn);
            return turns;
        }

        gameTurn = AIDecisionUtil.pullMoney(gameInstance, clonedGameInstance);
        turns.add(gameTurn);
        return turns;
    }

    private void countBuildingType(ModelTypeUtil.BuildingType buildingType) {
        if (!buildingTypeCounter.containsKey(buildingType))
            buildingTypeCounter.put(buildingType, 1);
        else
            buildingTypeCounter.put(buildingType, buildingTypeCounter.get(buildingType) + 1);
    }

    private ModelTypeUtil.BuildingType[] getOrderedBuildingPriority() {
        ModelTypeUtil.BuildingType[] sorted = new ModelTypeUtil.BuildingType[6];

        buildingTypeCounter.entrySet().stream()
                .sorted((key1, key2) -> -key1.getValue().compareTo(key2.getValue()))
                .forEach(key -> {
                    for (int j = 0; j < sorted.length; j++)
                        if (sorted[j] == null) {
                            sorted[j] = key.getKey();
                            break;
                        }
                });

        return sorted;
    }
}
