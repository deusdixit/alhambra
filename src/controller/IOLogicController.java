package controller;

import java.io.*;
import java.util.*;

import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.CardType;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.board.BuildingTile.Wall;
import util.ConfigUtil;

/**
 * Controller to read the Buildings and MoneyCards from a csv file.
 */
public class IOLogicController {

	MainLogicController mainLogicController;

	/**
	 * Constructor.
	 * 
	 * @param mainLogicController The {@link MainLogicController}.
	 */
	protected IOLogicController(MainLogicController mainLogicController) {
		this.mainLogicController = mainLogicController;
	}



	/**
	 * Function to read {@link BuildingTile}s from a csv {@link File}.
	 * 
	 * @return Return the {@link BuildingTile}s inside of a {@link Stack}.
	 */
	public static Stack<BuildingTile> readBuildingCSV(File file) {
		try {
			Scanner scanner = new Scanner(file);
			Stack<BuildingTile> result = new Stack<>();
			String firstLine = scanner.nextLine().replace(" ", "");
			String[] header = firstLine.split(",");
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().replace(" ", "");
				String[] col = line.split(",");
				if (col.length < header.length) {
					continue;
				}
				entryGen(result, header, col);
			}
			scanner.close();
			Collections.reverse(result);
			return result;
			
		} catch (Exception ex) {
			System.out.println(ex.getLocalizedMessage());
			return new Stack<>();
		}
	}

	private static void entryGen(Stack<BuildingTile> result, String[] header, String[] col) {
		int value = -1;
		BuildingType typ = null;
		boolean top = false;
		boolean right = false;
		boolean bottom = false;
		boolean left = false;

		for (int i = 0; i < col.length; i++) {
			if (header[i].equals("Gebaeudename")) {
				typ = BuildingType.valueOf(col[i].toUpperCase());
			} else if (header[i].equals("Preis")) {
				value = Integer.parseInt(col[i]);
			} else if (header[i].equals("MauerOben")) {
				top = Boolean.parseBoolean(col[i]);
			} else if (header[i].equals("MauerRechts")) {
				right = Boolean.parseBoolean(col[i]);
			} else if (header[i].equals("MauerUnten")) {
				bottom = Boolean.parseBoolean(col[i]);
			} else if (header[i].equals("MauerLinks")) {
				left = Boolean.parseBoolean(col[i]);
			}
		}
		Wall wall = new Wall(top, left, bottom, right);
		result.add(new BuildingTile(value, typ, wall));
	}
    
    	/**
	 * Function to read {@link MoneyCard}s from a csv {@link File}.
	 * 
	 * @return Return the {@link MoneyCard}s inside of a {@link Stack}.
	 */
	public static Stack<MoneyCard> readMoneyCSV(File file) {
		try {
			Scanner scanner = new Scanner(file);
			Stack<MoneyCard> result = new Stack<>();
			String firstLine = scanner.nextLine().replace(" ", "");
			String[] header = firstLine.split(",");
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().replace(" ", "");

				String[] col = line.split(",");
				if (col.length < header.length) {
					continue;
				}
				int value = -1;
				CardType typ = null;
				for (int i = 0; i < col.length; i++) {
					if (header[i].equals("Waehrung")) {
						typ = CardType.valueOf(col[i].toUpperCase());
					} else if (header[i].equals("Wert")) {
						value = Integer.parseInt(col[i]);
					}
				}
				result.add(new MoneyCard(value, typ));
			}
			scanner.close();
			Collections.reverse(result);
			return result;
		} catch (Exception ex) {
			System.out.println(ex.getLocalizedMessage());
			return new Stack<>();
		}
	}

	/**
	 * This is called when there is an empty file in order prevent exceptions
	 */
	/*
	 * private static void initializeFile() { List<HighScore> highScores = new
	 * ArrayList<HighScore>(); for(int i = 0; i<10; i++){ highScores.add(new
	 * HighScore(0," ",0)); }
	 * 
	 * try { System.out.println("Hi1"); ObjectOutputStream obj = new
	 * ObjectOutputStream(new FileOutputStream("HighScores.dat"));
	 * obj.writeObject(highScores); obj.close(); } catch (IOException e) {
	 * e.printStackTrace(); } }
	 * 
	 * 
	 * //Adds a new HighScore to the .dat file and maintains the proper order public
	 * void addNewHighScore(HighScore highScore){ List<HighScore> highScores =
	 * readHighScore(); HighScore last = highScores.get(9);
	 * 
	 * if(last.getScore() < highScore.getScore()){ highScores.remove(9);
	 * highScores.add(highScore);
	 * 
	 * for (int i = 8; i>=0; i--) { if (highScores.get(i+1).getScore() >
	 * highScores.get(i).getScore()) { HighScore temp = highScores.get(i);
	 * highScores.set(i,highScores.get(i+1)); highScores.set(i+1,temp); } } try {
	 * ObjectOutputStream obj = new ObjectOutputStream(new
	 * FileOutputStream("HighScores.dat")); obj.writeObject(highScores);
	 * obj.close(); } catch (IOException e) { e.printStackTrace(); } } }
	 * 
	 * //Reads the .dat file and returns the constants
	 * 
	 * @SuppressWarnings("unchecked") public List<HighScore> readHighScore(){
	 * 
	 * if (!new File("HighScores.dat").exists()) initializeFile(); try {
	 * ObjectInputStream obj = new ObjectInputStream(new
	 * FileInputStream("HighScores.dat")); List<HighScore> highScoreList =
	 * (List<HighScore>)obj.readObject(); return highScoreList.subList(0,9); } catch
	 * (IOException | ClassNotFoundException e) { e.printStackTrace(); } return
	 * null;
	 * 
	 * }
	 */
	public static void save(ConfigUtil.SaveGame game) throws IOException {
		checkFolder();
		File file= new File(game.getPath());
		FileOutputStream fileOutputStream=new FileOutputStream(file);
		ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
		objectOutputStream.writeObject(game);
		ConfigUtil.save();
	}
	public static void checkFolder () {
		File theDir = new File("saves");
		if (!theDir.exists()) {
				theDir.mkdir();
		}
	}
	public  static List<ConfigUtil.SaveGame> loadGame() throws IOException, ClassNotFoundException {
		LinkedList <ConfigUtil.SaveGame> list=new LinkedList<>();
		File saveFolder=new File("saves");
		if (saveFolder.exists() && saveFolder.isDirectory() ){
				for (File current:saveFolder.listFiles()){
					list.add(deserialization(current));
				}
		}
		return  list;
	}

	private static ConfigUtil.SaveGame deserialization(File file) throws IOException, ClassNotFoundException {
			FileInputStream fileInputStream= new FileInputStream(file);
			ObjectInputStream objectInputStream= new ObjectInputStream(fileInputStream);
		return  (ConfigUtil.SaveGame)objectInputStream.readObject();
	}

}
