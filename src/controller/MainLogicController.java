package controller;

import controller.ai.NaiveAIController;
import model.GameInstance;
import model.game.GameRound;
import model.game.GameTurn;
import model.strategy.EvaluationStrategy;
import model.strategy.RefillStrategy;
import model.strategy.StartGameStrategy;
import util.exception.InvalidGameTurnExecption;

public class MainLogicController {
    private GameLogicController gameLogicController;
    private IOLogicController ioLogicController;
    private GameInstance gameInstance;

    public MainLogicController(GameInstance gameInstance) {
        this.gameInstance = gameInstance;
        gameLogicController=new GameLogicController(this);
        ioLogicController=new IOLogicController(this);

    }

    public GameLogicController getGameLogicController() {
        return gameLogicController;
    }

    public IOLogicController getIoLogicController() {
        return ioLogicController;
    }

    public GameInstance getGameInstance() {
        return gameInstance;
    }

    public void execute(GameTurn... turns) throws InvalidGameTurnExecption { for (GameTurn turn:turns){
        boolean invalidTurn = false;
        if (turn == null) {
            invalidTurn = true;
        } else if (!turn.validate()||gameInstance.isFinish()) {
            invalidTurn = true;
        }

        if (invalidTurn) {
            turn = NaiveAIController.getTip(gameInstance.getCurrentPlayer(),gameInstance);
            if (!turn.validate()) {
                throw new InvalidGameTurnExecption();
            }
        }

        if ((turn instanceof StartGameStrategy)){
            apendTurn(new RefillStrategy(null,gameInstance));
        }else {
            apendTurn(turn);
        }


        gameInstance.trigger();

    } }

    public void apendTurn( GameTurn turn) {
        gameInstance.getRounds().getLast().add(turn);
        gameInstance.getRedoStack().clear();
        gameLogicController.performAction(turn);
        if (turn instanceof RefillStrategy && gameInstance.isEval()>0){
            System.out.println("Refill Strategy"+gameInstance.isEval());
            if (gameInstance.isEval()==3){
                gameInstance.setLastRound(true);
            }
            gameInstance.setEvaluationStrategy(new EvaluationStrategy(null,gameInstance,gameInstance.isEval()));
            gameLogicController.performAction(gameInstance.getEvaluationStrategy());
        }
    }
}
