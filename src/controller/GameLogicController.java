package controller;

import controller.ai.NaiveAIController;
import model.GameInstance;
import model.game.Bot;
import model.game.GameRound;
import model.game.GameTurn;
import model.game.Player;
import model.strategy.EvaluationStrategy;
import model.strategy.RefillStrategy;
import model.strategy.StartGameStrategy;
import util.exception.InvalidGameLogicException;
import util.exception.InvalidGameTurnExecption;

import java.util.List;
import java.util.Stack;
import java.util.logging.Logger;

public class GameLogicController {
	private MainLogicController mainLogicController;

	public GameLogicController(MainLogicController mainLogicController) {
		this.mainLogicController = mainLogicController;
	}

	public MainLogicController getMainLogicController() {
		return mainLogicController;
	}

	public void performAction(GameTurn gameTurn) {
		gameTurn.doTurn();
	}

	/**
	 * Determines, if every player is a cheater
	 *
	 * @return True, if every player is a cheater
	 */
	public boolean isGameCorrupted() {
		for (Player player : mainLogicController.getGameInstance().getPlayerList()) {
			if (!player.getCheater()) {
				return false;
			}
		}
		return true;
	}

	public boolean getUndoAvailable() {
		List<GameRound> rounds = mainLogicController.getGameInstance().getRounds();
		GameTurn lastTurn = null;
		if (rounds.size() > 1 && rounds.get(rounds.size() - 1).size() > 0) {
			lastTurn = rounds.get(rounds.size() - 1).getLast();
		}
		if (lastTurn == null || lastTurn instanceof StartGameStrategy) {
			return false;
		}
		return true;

	}

	/**
	 * Undoes the last executed turn and puts it onto the redoStack
	 *
	 * @throws InvalidGameLogicException Thrown, if there is no last turn or
	 *                                   something different and unexpected happened
	 */
	public void undoLastTurn() throws InvalidGameLogicException, InvalidGameTurnExecption {
		if (!getUndoAvailable()) {
			throw new InvalidGameLogicException("No Last Turn");
		}

		GameInstance gInstance = mainLogicController.getGameInstance();
		List<GameRound> rounds = gInstance.getRounds();
		List<Player> playerList = gInstance.getPlayerList();
		GameTurn lastTurn = rounds.get(rounds.size() - 1).getLast();
		for (Player player : playerList) {
			player.setCheater(true);
		} // Set everybody to cheater

		rounds.get(rounds.size() - 1).remove(lastTurn);
		lastTurn.unDoTurn();
		Stack<GameTurn> redoStack = gInstance.getRedoStack();
		redoStack.push(lastTurn);

		if (rounds.get(rounds.size() - 1).size() == 0) {
			rounds.remove(rounds.size() - 1);
		}
		if (lastTurn instanceof RefillStrategy || lastTurn instanceof EvaluationStrategy) {
			undoLastTurn();
		}
	}

	public boolean getRedoAvailable() {
		return !mainLogicController.getGameInstance().getRedoStack().empty();
	}

	public void  redoTurn() throws InvalidGameLogicException, InvalidGameTurnExecption {
		GameInstance gInstance = mainLogicController.getGameInstance();

		if (!getRedoAvailable()) {
			throw new InvalidGameLogicException("Nothing to redo");
		}

		GameTurn redoTurn = gInstance.getRedoStack().pop();

		Stack<GameTurn> copy = new Stack<>();
		copy.addAll(gInstance.getRedoStack());
		mainLogicController.execute(redoTurn);
		gInstance.setRedoStack(copy);


		if (getRedoAvailable()) {
			GameTurn peek = gInstance.getRedoStack().peek();
			if (peek instanceof RefillStrategy || peek instanceof EvaluationStrategy || gInstance.getCurrentPlayer() instanceof Bot) {
				redoTurn();
			}
		}

		// if (getRedoAvailable()) {
		// if (redoTurn instanceof EvaluationStrategy || redoTurn instanceof
		// RefillStrategy) {
		// redoTurn();
		// } else if (redoTurn instanceof PlaceBuildingStrategy
		// && gi.getRedoStack().peek() instanceof BuyReserveBuildingStrategy) {
		// redoTurn();
		// }
		// }
	}

	public static GameTurn getHint(GameInstance gameInstance) {
		gameInstance.getCurrentPlayer().setCheater(true);
		return NaiveAIController.getTip(gameInstance.getCurrentPlayer(),gameInstance);
	}

	public void nextTurn() {
	}

}
