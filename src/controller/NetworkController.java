package controller;

import javafx.collections.ObservableList;

import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class NetworkController {
    MainLogicController mainLogicController;
        NetworkController (MainLogicController mainLogicController){
            this.mainLogicController=mainLogicController;
        }

    public static void fillList (ObservableList <String> list){
        list.clear();
        final ExecutorService executionService = Executors.newFixedThreadPool(20);
        final String ipAddress = "192.168.1.";
        final int timeout = 200;
        final List<Future<HostGame>> futures = new ArrayList<>();
        for (int counter = 1; counter<255 ; counter++) {
            futures.add(portIsOpen(executionService, ipAddress+counter, 3333, timeout));
        }
        executionService.shutdown();

        for (final Future<HostGame> f : futures) {
            try {
                if (f.get().isPeerUp()) {
                    System.out.println(f.get().getIpAddress());
                    list.add(f.get().ipAddress);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public static Future<HostGame> portIsOpen(final ExecutorService executorService, final String ipAddress, final int port, final int timeout) {
        return executorService.submit(() -> {
            try {
                Socket socket = new Socket();
                socket.connect(new InetSocketAddress(ipAddress, port), timeout);
                socket.close();
                return new HostGame(ipAddress,true);
            } catch (Exception ex) {
                return new HostGame(ipAddress,false);
            }
        });
    }

    public static class HostGame {
        private String ipAddress;
        private boolean peerUp;

        public HostGame(String ipAddress, boolean peerUp) {
            this.ipAddress = ipAddress;
            this.peerUp = peerUp;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public boolean isPeerUp() {
            return peerUp;
        }
    }
}
