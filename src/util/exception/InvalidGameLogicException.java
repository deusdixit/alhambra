package util.exception;

public class InvalidGameLogicException extends Exception {
    public InvalidGameLogicException() { super(); }
    public InvalidGameLogicException(String message) { super(message); }
}