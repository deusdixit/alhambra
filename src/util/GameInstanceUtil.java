package util;

import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Player;

import java.util.Collections;
import java.util.List;
import java.util.Stack;
@SuppressWarnings("PMD")

public class GameInstanceUtil {

  public static void printStatistic(GameInstance gameInstance) {

    String println = "\n# GameStatistic #\n";
    println += "Player " + gameInstance.getPlayerList().size();

    println += "\nMoneyCards " + gameInstance.getCardStack().size()+" Wertung01: "+getWertung(gameInstance.getCardStack(),1)+" Wertung02: "+getWertung(gameInstance.getCardStack(),2) ;

    println += "\n\nBuildingCards " + gameInstance.getBuildingStack().size();
    println += "\nBuldigyard:\n" + buildingYard(gameInstance.getBuildingYard());

    println += "\nGraveyard " + gameInstance.getGraveyard().size();
    println = playerInfo(gameInstance, println);
    System.out.println(println);
  }

  private static String buildingYard(BuildingTile[] yard){
      String line ="";
      for (int i = 0; i < yard.length; i++) {
          switch (i){
              case 0:
                  line += "GULDEN: ";
                  break;
              case 1:
                  line += "DIRHAM: ";
                  break;
              case 2:
                  line += "DENAR: ";
                  break;
              case 3:
                  line += "DUKATEN: ";
                  break;
              default:
                  break;
          }
          if(yard[i] != null)
            line += yard[i].getType() +" " + yard[i].getPrice() + "\n";
          else
              line += "\n";
      }
      return line;
  }

    private static int getWertung(Stack<MoneyCard> cardStack, int i) {
        Stack revList;
        revList = (Stack) cardStack.clone();
        Collections.reverse(revList);
      int pos=0;
      for (Object card:revList){
          if (card instanceof MoneyCard){
              MoneyCard card1= (MoneyCard) card;
          if (card1.getType()== ModelTypeUtil.CardType.WERTUNG&&card1.getValue()==i){
                return pos;
          }else {
              pos++;
          }}
      }
      return -1;
    }

    private static String playerInfo(GameInstance gameInstance, String println) {
        for (Player player:gameInstance.getPlayerList()){
                println+="\n"+player.getName()+" "+player.getColor().name();
                printPlayerDetail(player);
            }
        return println;
    }

    public static void printPlayerDetail(Player player){
      String println = "\n# Details to "+player.getName()+" #\n";

      for (MoneyCard moneyCard:player.getMoneyCards()){
          println+="\n"+moneyCard.toString();
      }
      println+="\n";

      for (BuildingTile buildingTile:player.getGameBoard().getBuildingList()){
          println+="\n"+buildingTile.toString();
      }
      System.out.println(println);
  }

    public static Stack<MoneyCard> generateMoneyCards() {
        Stack<MoneyCard> moneyCards = new Stack<>();
        for (int i = 1; i < 10; i++) {
            for (int g = 0; g < 4; g++) {
                for (int j = 1; j < 4; j++) {
                    switch (g) {
                        case 0:
                            moneyCards.push(new MoneyCard(i, ModelTypeUtil.CardType.DENAR));
                            break;
                        case 1:
                            moneyCards.push(new MoneyCard(i, ModelTypeUtil.CardType.DIRHAM));
                            break;
                        case 2:
                            moneyCards.push(new MoneyCard(i, ModelTypeUtil.CardType.DUKATEN));
                            break;
                        case 3:
                            moneyCards.push(new MoneyCard(i, ModelTypeUtil.CardType.GULDEN));
                            break;
                    }

                }
            }
        }

        return moneyCards;
    }

    public static Stack<BuildingTile> generateBuildingTile() {
        Stack<BuildingTile> buildingTile = new Stack<>();

        //PAVILION
        buildingTile.push(new BuildingTile(2, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(true, true, false, true)));
        buildingTile.push(new BuildingTile(3, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(false, true, true, false)));
        buildingTile.push(new BuildingTile(4, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(false, false, true, true)));
        buildingTile.push(new BuildingTile(5, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(true, true, false, false)));
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(true, false, false, false)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(false, false, false, true)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.PAVILLON, new BuildingTile.Wall(false, false, false, false)));


        //SERAIL
        buildingTile.push(new BuildingTile(3, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, true, true, true)));
        buildingTile.push(new BuildingTile(4, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(true, false, false, true)));
        buildingTile.push(new BuildingTile(5, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, true, true, false)));
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, false, true, true)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, true, false, false)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, false, true, false)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.SERAIL, new BuildingTile.Wall(false, false, false, false)));




        //ARKADEN
        buildingTile.push(new BuildingTile(4, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(true, false, true, true)));
        buildingTile.push(new BuildingTile(5, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(true, true, false, false)));
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(false, true, true, false)));
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(true, false, false, true)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(false, false, true, true)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(true, false, false, false)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(false, false, false, true)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.ARKADEN, new BuildingTile.Wall(false, false, false, false)));

        //GEMAECHER
        buildingTile.push(new BuildingTile(5, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(true, true, true, false)));
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, false, true, true)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, true, true, false)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(true, false, false, true)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(true, true, true, false)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, false, true, false)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, true, false, false)));
        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(11, ModelTypeUtil.BuildingType.GEMAECHER, new BuildingTile.Wall(false, false, false, false)));



        //GARTEN
        buildingTile.push(new BuildingTile(6, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, true, true, true)));
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(true, true, true, false)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(true, true, false, false)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(true, false, false, true)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, true, true, false)));

        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, false, false, true)));

        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(true, false, false, false)));
        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, true, false, false)));

        buildingTile.push(new BuildingTile(11, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(12, ModelTypeUtil.BuildingType.GARTEN, new BuildingTile.Wall(false, false, true, false)));

        //GARTEN
        buildingTile.push(new BuildingTile(7, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(true, true, false, true)));
        buildingTile.push(new BuildingTile(8, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(true, false, true, true)));

        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(true, false, false, true)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(true, true, false, false)));
        buildingTile.push(new BuildingTile(9, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, false, true, true)));

        buildingTile.push(new BuildingTile(10, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, true, false, false)));

        buildingTile.push(new BuildingTile(11, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, false, true, false)));
        buildingTile.push(new BuildingTile(11, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(11, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(true, false, false, false)));

        buildingTile.push(new BuildingTile(12, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, false, false, false)));
        buildingTile.push(new BuildingTile(13, ModelTypeUtil.BuildingType.TURM, new BuildingTile.Wall(false, false, false, true)));




        return buildingTile;
    }

    public static BuildingTile randomBuildingTile() {
        List<BuildingTile> list =generateBuildingTile();
        Collections.shuffle(list);
        Collections.shuffle(list);
        Collections.shuffle(list);
        return  list.get(0);
    }

    public static int checkSize(MoneyCard[] arr){
        int count=0;
        for (MoneyCard object:arr){
            if (object==null){
                count++;
            }
        }
        return 4-count;
    }

    public static int checkSize(BuildingTile[] arr){
        int count=0;
        for (BuildingTile object:arr){
            if (object==null){
                count++;
            }
        }
        return 4-count;
    }
}
