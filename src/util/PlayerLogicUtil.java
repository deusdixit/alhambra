package util;

import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.game.Player;
import java.util.HashMap;
import java.util.List;

public class PlayerLogicUtil
{
	/**
	 * Counts the building of a type placed by all players
	 * @param gameInstance	current game instance
	 * @return	HashMap Player, Integer for every existing bulding type
	 */
	@SuppressWarnings("PMD")

	public static BuildingPerPlayerCount getBuildingTileCountForEveryPlayer(GameInstance gameInstance){
		BuildingPerPlayerCount eval = new BuildingPerPlayerCount();
		List<Player> playerList = gameInstance.getPlayerList();
		
		// Count every building tile of every palyer
		HashMap<Player, HashMap<ModelTypeUtil.BuildingType, Integer>> playerBuildingTypeCountMap = new HashMap<>();
		for(Player player : playerList){
			HashMap<ModelTypeUtil.BuildingType, Integer> buildingTypeIntegerMap = new HashMap<>();
			for (BuildingTile[] tileArray : player.getGameBoard().getBoard()) {
				for (BuildingTile tile : tileArray) {
					// Check if building type exists
					if(!buildingTypeIntegerMap.containsKey(tile.getType())){
						buildingTypeIntegerMap.put(tile.getType(), 0);
					}
					buildingTypeIntegerMap.put(tile.getType(), buildingTypeIntegerMap.get(tile.getType()) + 1);
				}
			}
			playerBuildingTypeCountMap.put(player, buildingTypeIntegerMap);
		}
		
		
		for(Player player : playerBuildingTypeCountMap.keySet()){
			for(ModelTypeUtil.BuildingType type: playerBuildingTypeCountMap.get(player).keySet()){
				HashMap<Player, Integer> toAdd = null;
				switch (type){
					case PAVILLON:
						toAdd = eval.getPavillion();
						break;
					case SERAIL:
						toAdd = eval.getSerail();
						break;
					case TURM:
						toAdd= eval.getTurm();
						break;
					case GARTEN:
						toAdd = eval.getGarten();
						break;
					case ARKADEN:
						toAdd = eval.getArkaden();
						break;
					case GEMAECHER:
						toAdd = eval.getGemaecher();
						break;
					default:
						break;
				}
				toAdd.put(player, playerBuildingTypeCountMap.get(player).get(type));
			}
		}
		
		return eval;
	}
	
	public static class BuildingPerPlayerCount
	{
		private HashMap<Player, Integer> pavillion;
		private HashMap<Player, Integer> serail;
		private HashMap<Player, Integer> arkaden;
		private HashMap<Player, Integer> gemaecher;
		private HashMap<Player, Integer> garten;
		private HashMap<Player, Integer> turm;
		
		
		public BuildingPerPlayerCount(){
			pavillion = new HashMap<>();
			serail = new HashMap<>();
			arkaden = new HashMap<>();
			gemaecher = new HashMap<>();
			garten = new HashMap<>();
			turm = new HashMap<>();
		}
		
		public HashMap<Player, Integer> getPavillion()
		{
			return pavillion;
		}
		
		public HashMap<Player, Integer> getSerail()
		{
			return serail;
		}
		
		public HashMap<Player, Integer> getArkaden()
		{
			return arkaden;
		}
		
		public HashMap<Player, Integer> getGemaecher()
		{
			return gemaecher;
		}
		
		public HashMap<Player, Integer> getGarten()
		{
			return garten;
		}
		
		public HashMap<Player, Integer> getTurm()
		{
			return turm;
		}
	}
	

}



