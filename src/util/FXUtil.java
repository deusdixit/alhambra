package util;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.Screen;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 */
public class FXUtil {
    static File file=new File("src/res/music/background.mp3");
    static MediaPlayer soundPlayer;
    final static MediaPlayer musicPlayer=new MediaPlayer(new Media(file.toURI().toString()));
    static double soundVolume = 0.25;
    static double musicVolume = 0;
    static boolean isMuted = false;

    public static double projectScale() {
        return Screen.getPrimary().getBounds().getWidth() < 1500 ? 0.75 : 1.0;
    }

    public static Node projectScale(Node node) {
        if (projectScale() != 1.0) {
            node.setScaleX(projectScale());
            node.setScaleY(projectScale());
        }
        return node;
    }

    public static ImageView getViewByPath(String path, int width, int height) {
        return new ImageView(new Image(path, width, height, false, false));
    }

    public static File fileLoader() {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Logbook files (*.logbook)",
                "*.logbook");
        fileChooser.getExtensionFilters().add(extFilter);

        return fileChooser.showOpenDialog(null);
    }

    public static File fileSaver(String type) {
        FileChooser fileChooser = new FileChooser();

        // Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(type + " files (*" + type + ")",
                "*" + type);
        fileChooser.getExtensionFilters().add(extFilter);

        return fileChooser.showSaveDialog(null);
    }

    public static void exceptionDialog(Exception exception) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Exception Dialog");
        alert.setHeaderText("Oops... that was not suspected ");
        alert.setContentText("An error occurred!");

        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        String exceptionText = stringWriter.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }

    public static void alertDialog(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static void successDialog(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(title);
        alert.setContentText(message);

        alert.showAndWait();
    }

    public static void timeHourListener(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("([01]?[0-9]|2[0-3])")) {
                textField.setText("00");
            }
        });
    }

    public static void timeMinListener(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[0-5][0-9]")) {
                textField.setText("00");
            }
        });
    }

    public static void numericListener(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d+\\.\\d")) {
                textField.setText(newValue.replaceAll("[^\\d+\\.\\d+]", ""));
            }
        });
    }

	public static void alphabeticListener(TextField textField) {
		textField.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("[A-z\\s]")) {
				textField.setText(newValue.replaceAll("[^A-Za-z]", ""));
			}
		});
	}

    public static void alphabeticAndNumericListener(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[A-z\\s\\d]")) {
                textField.setText(newValue.replaceAll("[^A-Za-z +\\d]", ""));
            }
        });
    }

	public static void setMusicPlayer(){

        musicPlayer.setVolume(ConfigUtil.load().getMusicVolume());
        musicPlayer.autoPlayProperty();
        musicPlayer.play();
    }
	public static void setMenuButtonSound(){
			File file = new File("src/res/sounds/buttonSound.mp3");
			soundPlayer = new MediaPlayer(new Media(file.toURI().toString()));
			soundPlayer.setVolume(soundVolume);
			soundPlayer.play();
	}
	public static void setCardSound(){
		if(!isMuted){
			File file=new File("src/res/sounds/cardSound.mp3");
			soundPlayer =new MediaPlayer(new Media(file.toURI().toString()));
			soundPlayer.setVolume(soundVolume);
			soundPlayer.play();
		}
	}
	public static void setDestroySound(){
		if(!isMuted){
			File file=new File("src/res/sounds/destroySound.mp3");
			soundPlayer =new MediaPlayer(new Media(file.toURI().toString()));
			soundPlayer.setVolume(soundVolume);
			soundPlayer.play();
		}
	}
	public static void setHandCardSound(){
		if(!isMuted){
			File file=new File("src/res/sounds/handCardSound.mp3");
			soundPlayer =new MediaPlayer(new Media(file.toURI().toString()));
			soundPlayer.setVolume(soundVolume);
			soundPlayer.play();
		}
	}
	public static void setPlacingSound(){
		if(!isMuted){
			File file=new File("src/res/sounds/placingSound.mp3");
			soundPlayer =new MediaPlayer(new Media(file.toURI().toString()));
			soundPlayer.setVolume(soundVolume);
			soundPlayer.play();
		}
	}
	public static MediaPlayer getSoundPlayer(){return soundPlayer;}
	public static void setSoundVolume(double volume){soundVolume=volume;}
	public static double getSoundVolume(){return soundVolume;}
	public static void setIsMuted(boolean value){
		isMuted =value;}
		public static boolean getIsMuted(){return isMuted;}
    public static MediaPlayer getMusicPlayer(){ return musicPlayer;}
    public static void setMusicVolume(double volume){
        musicVolume=volume;
        ConfigUtil.load().setMusicVolume(musicVolume);
        ConfigUtil.save();
    }
    public static double getMusicVolume(){return ConfigUtil.load().getMusicVolume();}

    public static String stringFormater(String in){
        int max=30;
        if (in.length()<=max){
            return in;
        }

        String[] current=in.split(" ");
        StringBuilder stringBuilder=new StringBuilder();
        String currentLine="";
        for (int i=0;i<current.length;i++){
            if (currentLine.length()>max){
                stringBuilder.append(currentLine+"\n");
                currentLine=current[i]+" ";
            }else {
                currentLine+=current[i]+" ";
            }
        }
        return stringBuilder.toString();
    }

}
