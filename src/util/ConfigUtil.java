package util;

import controller.IOLogicController;
import model.GameInstance;
import model.game.Player;

import javax.swing.*;
import java.io.*;
import java.util.Collections;
import java.util.LinkedList;

public class ConfigUtil {

    final static String path="config.tmp";

    private static Configuration configuration;

    public static  Configuration load()  {
        if (configuration==null){
            try {
                configuration=loadConfigFile();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return configuration;
    };
    public static void save(){
        if (configuration!=null){
            try {
                saveConfigFile(configuration);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static Configuration loadConfigFile  () throws IOException, ClassNotFoundException {
        try {
            File current=new File(path);
            FileInputStream fileInputStream=new FileInputStream(current);
            ObjectInputStream objectInputStream= new ObjectInputStream(fileInputStream);

            return (Configuration) objectInputStream.readObject();

        } catch (FileNotFoundException | StreamCorruptedException e){
            Configuration configuration=new Configuration();

            LinkedList<HighScore> list=new LinkedList<>();
            list.add(new HighScore(100,"Doris",70));
            list.add(new HighScore(80,"Doris",60));

            configuration.setHighScores(list);

            saveConfigFile(configuration);
            return configuration;
        }catch (InvalidClassException e){
            System.err.println("Deine config.tmp ist zu alt bitte lösche diese !!! ");
            System.err.println("Deine config.tmp ist zu alt bitte lösche diese !!! ");
            System.err.println("Deine config.tmp ist zu alt bitte lösche diese !!! ");
            System.err.println("Deine config.tmp ist zu alt bitte lösche diese !!! ");
            System.err.println("Deine config.tmp ist zu alt bitte lösche diese !!! ");
            System.exit(-1);
            return null;
        }
    }

    private static void saveConfigFile(Configuration configuration) throws IOException {
            File output=new File(path);
            FileOutputStream fileOutputStream=new FileOutputStream(output);
            ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(configuration);
    }

    public static class Configuration implements Serializable {
        LinkedList <HighScore> highScores;
        int currentSave;
        double soundVolume,musicVolume;
        Configuration(){
            highScores=new LinkedList<>();
            soundVolume=0.25;
            musicVolume=0.0;
        }

        public LinkedList<HighScore> getHighScores() {
            return highScores;
        }
        public void setHighScores(LinkedList <HighScore> highScores){
            //TODO NUR DIE BESTEN SPIELER
              this.highScores=highScores;
        }

        public int getCurrentSave() {
            return currentSave;
        }

        public void increaseSaves() {
            this.currentSave++;
        }

        public double getSoundVolume() {
            return soundVolume;
        }

        public void setSoundVolume(double soundVolume) {
            this.soundVolume = soundVolume;
        }

        public double getMusicVolume() {
            return musicVolume;
        }

        public void setMusicVolume(double musicVolume) {
            this.musicVolume = musicVolume;
        }
    }
    public static class SaveGame   implements Comparable<SaveGame>,Serializable{
        GameInstance gameInstance;
        int id;
        String path;

        public SaveGame(GameInstance gameInstance, int id, String path) {
            this.gameInstance = gameInstance;
            this.id = id;
            this.path = path;
        }

        public GameInstance getGameInstance() {
            return gameInstance;
        }

        public int getId() {
            return id;
        }

        public String getPath() {
            return path;
        }

        @Override
        public int compareTo(SaveGame o) {
            return  o.id-id;
        }
    }
    public static class HighScore implements Serializable,Comparable<HighScore>{
        int score;
        String name;
        int result;

        public HighScore(int score, String name, int result) {
            this.score = score;
            this.name = name;
            this.result = result;
        }
        public HighScore(Player player,GameInstance instance) {
            this.score = player.getScore();
            this.name = player.getName();
            this.result =(int) (player.getScore()*(instance.getPlayerList().size()/6.0)); //TODO
        }

        public int getScore() {
            return score;
        }

        public String getName() {
            return name;
        }

        public int getResult() {
            return result;
        }

        @Override
        public int compareTo(HighScore o) {

            return o.getResult()-getResult();
        }
    }
}
