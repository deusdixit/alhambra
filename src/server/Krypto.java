/* Klasse zum Verschluesseln von textnachrichten
 * Es war zwar nicht gefordert eine verschlssselung der nachrichten zu integrieren
 * Doch ohne ist keine minimale sicherheit gewhrleistet
 * Des weiteren sind nur die eigentlichen nachrichten verschlsselt und nicht die Protokoll Befehle
 * */
package server;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

@SuppressWarnings("PMD")
class Krypto {
    String keyStr;

    Krypto(String value) {
        keyStr = value;
    }

    Krypto() {
        keyStr = "Schiffe sind toll!";
    }

    void setKey(String key) {
        keyStr = key;
    }

    public String krypt(String message) {
        String geheim = "";
        try {

            // byte-Array erzeugen
            byte[] key = (keyStr).getBytes(StandardCharsets.UTF_8);
            // aus dem Array einen Hash-Wert erzeugen mit  SHA
            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            key = sha.digest(key);
            // nur die ersten 128 bit nutzen
            key = Arrays.copyOf(key, 16);
            // der fertige Schluessel

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

            // Verschluesseln
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] encrypted = cipher.doFinal(message.getBytes());

            // bytes zu Base64-String konvertieren (dient der Lesbarkeit)

            geheim = Base64.getEncoder().encodeToString(encrypted);

        } catch (Exception e) {
            System.err.println("FEHLER beim Verschlsseln");
        }

        return geheim;
    }

    public String deKrypt(String message) {
        String erg = "", msg = "";

        char[] arrH =
                message
                        .toCharArray(); // Uberpruefung von leerzeile in Msg Fehlerbehebung von lternen
        // Versionen
        if (arrH.length > 1 && arrH[arrH.length - 1] == ' ') {
            // System.err.println("Schau mal in die Nice Methode");
            msg = message.substring(0, arrH.length - 1);

        } else {
            msg = message;
        }
        try {

            // byte-Array erzeugen
            byte[] key = (keyStr).getBytes(StandardCharsets.UTF_8);
            // aus dem Array einen Hash-Wert erzeugen mit SHA
            MessageDigest sha = MessageDigest.getInstance("SHA-256");

            key = sha.digest(key);
            // nur die ersten 128 bit nutzen
            key = Arrays.copyOf(key, 16);
            // der fertige Schluessel

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

            // BASE64 String zu Byte-Array konvertieren

            byte[] crypted2 = Base64.getDecoder().decode(msg);

            // Entschluesseln
            Cipher cipher2 = Cipher.getInstance("AES");
            cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] cipherData2 = cipher2.doFinal(crypted2);
            erg = new String(cipherData2);
        } catch (Exception e) {
            System.err.println("FEHLER beim entschlsseln");
            System.err.println("Assert: " + msg);
            e.printStackTrace();
        }
        return erg;
    }
}
