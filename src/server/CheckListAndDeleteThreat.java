/*Thread der sich der verbunden Peers Annimmt und berprfft ob sie noch Online (lTime<60s) sind
 * */
package server;

import java.util.ArrayList;

@SuppressWarnings("PMD")
class CheckListAndDeleteThreat extends Thread {
    Client
            client; // Allen Thrats wird das "Kernel" object bergeben damit sie vollen zugriff auf die Daten
    // zu erhalten
    private ArrayList<Peer> peerList; // Hilfsvariable zum Zwischenspeichern
    private long nowTime; // Variable fr die Aktuelle Zeit

    CheckListAndDeleteThreat(Client client1) // jedem peer wird der Client bergeben (fuer Zugriff)
    {
        client = client1;
    }

    @Override
    public void run() {
        while (true) // Endloss Schleife, kein fall bei dem es Notwendig wre den Threat zu beenden
        {

            client.tryUnlock(); // Mutex
            peerList = client.knownPeers;
            nowTime = System.currentTimeMillis() / 1000; // Aktuelle zeit

            for (int i = 0; i < peerList.size() && peerList.get(i) != null; i++) // Geht die 10 Pers Durch
            {

                int scope = 30;
                if (nowTime - peerList.get(i).getTime() > scope) {
                    Alert a =
                            new Alert(
                                    peerList.get(i).getIP(), peerList.get(i).getPort(), peerList.get(i).getName(), peerList.get(i).getLastGPS());
                    System.out.println("Nutzer entfert " + peerList.get(i));
                    if (client instanceof GameClient) {
                        GameClient current = (GameClient) client;

                        GamePeer peer = (GamePeer) peerList.get(i);
                        current.gameInstance.getPlayerList().remove(peer.getPlayer());
                    }
                    peerList.remove(i); // Setzt die zeit des Letzten Pokes um 120s zurueck und wird somit unmittelbar

                    client.knownPeers = peerList;
                    client.sendAlert(a);
                    System.out.println("Nutzer entfert ---->Ende");

                } else {
                    client.knownPeers = peerList;
                }
            }

            client.unlock(); // Mutex
            try {
                Thread.sleep(5 * 1000);
            } catch (Exception e) {
                System.out.println("ABCD->e");
                e.printStackTrace();
            } // Definition Zyklus
        }
    }
}
