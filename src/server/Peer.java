/*Klasse zur abspeicherung und aktualisierung der verbundenen Peers
 */
package server;

public class Peer // Klasse fuer known hosts
{
    final String name, ipAddess;
    long lTime;
    public int port;
    private String lastGPS;
    private String lastStrenght;

    Peer(String name, String ipAddress, int port) {
        this.name = name;
        this.ipAddess = ipAddress;
        this.lTime = System.currentTimeMillis() / 1000; // Aktuelle Systemzeit in Sec
        this.port = port;
        lastGPS = "noGPSsignal";
        lastStrenght = "noSignal";
    }

    public String getLastGPS() {
        return lastGPS;
    }

    public void setLastGPS(String lastGPS) {
        this.lastGPS = lastGPS;
    }

    public String getLastStrenght() {
        return lastStrenght;
    }

    public void setLastStrenght(String lastStrenght) {
        this.lastStrenght = lastStrenght;
    }

    public String getName() {
        return name;
    }

    public String getIP() {
        return ipAddess;
    }

    public int getPort() {
        return port;
    }

    public String toString() {
        return name
                + "\n"
                + ipAddess
                + "\n"
                + port
                + "\n"
                + ((System.currentTimeMillis() / 1000) - lTime)
                + "s"
                + "\nGPS:"
                + lastGPS
                + "\n"
                + lastStrenght;
    }

    public boolean checkIP(String ipAddress) {
        return this.ipAddess.equals(ipAddress);
    }

    public boolean checkName(String name) {
        return this.name.equals(name);
    }

    public boolean checkPort(int port) {
        return this.port == port;
    }

    public void updateTime() {
        lTime = System.currentTimeMillis() / 1000;
    }

    public void
    downTime() // sollte ein client von einem der Anderen Peers  als Disconnect gemarkt werden
    // dann wird die "Last Connect" time zurckgesetzt und damit vom CLAD Thread
    // automatisch ausssortiert
    {
        lTime -= 120;
    }

    public long getTime() {
        return lTime;
    }

    public void nextPort() {
        port++;
    }
}
