/*Klasse zum Modelieren und verabeiten der ein und ausgehenden Nachrichten (Anlehung linked list -> Message wie Element)
 *
 */
package server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class Message {
    String name;
    String ipAddress;
    String content;
    Date date;
    Message next;
    private static final DateFormat SDF =
            new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); // Dateiformat fr Datumsanzeige

    boolean isMe; // Unterscheidet eingehende und ausgehende nachrichten

    Message(String user, String ipAddress, boolean isEnum, String message) {
        this.name = user;
        this.ipAddress = ipAddress;
        this.isMe = isEnum; // true == me
        this.content = message;
        this.date = new Date();
    }

    boolean hasNext() {
        return next != null;
    }

    public String toString() {
        if (isMe) {
            return "[" + SDF.format(date) + "]Nachricht an " + name + ": " + content;
        } else {
            return "[" + SDF.format(date) + "]Nachricht von " + name + ": " + content;
        }
    }

    void addNext(Message message) {
        next = message;
    }
}
