package server;

import model.GameInstance;
import model.ModelTypeUtil;
import model.game.Player;

import java.net.ServerSocket;

@SuppressWarnings("PMD")
public class GameClient extends Client {
    public GameInstance gameInstance;
    ServerSocket dataServer;
    GameDataThread dataThread;
    boolean master = false;
    boolean conected = false;

    public GameClient(GameInstance gameInstance) {
        super();
        this.gameInstance = gameInstance;
    }

    @Override
    void ackDisconnect(String number, String position, int value) // Handling von eingehenden Disconnect Befehlen
    {
        int perOne = checkPeers(position, value);
        if (perOne < 0) {

        } else {
            GamePeer peer = (GamePeer) knownPeers.get(perOne);
            gameInstance.getPlayerList().remove(peer.getPlayer());
            knownPeers.remove(
                    perOne); // Setzt die zeit des Letzten Pokes um 120s zurueck und wird somit unmittelbar

            // entfert von "CheckListAndDeleteThreat"
        }
    }

    @Override
    public void
    startServer() // Startet die ServerNotwendigen Threats und erzeugt einen neuen freien Server
    // Socket
    {
        thread01 = new CheckListAndDeleteThreat(this);
        thread01.start();

        findFreeSocket();

        try {

            dataServer = new ServerSocket(itsMe.port + 1);
            dataThread = new GameDataThread(this);
            System.out.println("Syncronisation gestartet");
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread02 = new ListenThreat(this);
        thread02.start();
        thread03 = new PokeThread(this);
        thread03.start();
    }

    @Override
    void actionHandler(
            String
                    a) // Aktion Handler fuer eingehende Befehle -> Kann sein das hier noch Fehrler Handling
    // notwendig ist
    {
        if (a.length() > 2) {
            String[] arg = a.split(" ");
            switch (arg[0]) {
                case "ALARM":
                    addAlert(arg[1], Integer.parseInt(arg[2]), arg[3], arg[4]);
                    break;
                case "POKE":
                    ackPoke(arg[1], arg[2], Integer.parseInt(arg[3]));
                    break;
                case "CON":
                    requestRegistration(arg[1], arg[2]);
                case "ACK":
                    receiveAce(arg[1], arg[2]);
                    break;
                case "GPS":
                    if (arg.length == 5) {
                        ackGPS(arg[2], Integer.parseInt(arg[3]), arg[4]);
                    } else if (arg.length == 6) {
                        ackGPSandStrength(arg[2], Integer.parseInt(arg[3]), arg[4], arg[5]);
                    }
                    break;
                case "DISCONNECT":
                    ackDisconnect(arg[1], arg[2], Integer.parseInt(arg[3]));
                    break;
                case "MESSAGE":
                    int max = arg.length;
                    String ins = "";
                    for (int x = 4; x < max; x++) {
                        ins += arg[x] + " ";
                    }
                    ackMessage(arg[1], arg[2], Integer.parseInt(arg[3]), ins);
                    break;

                default:
                    System.out.println("Invalid Stream -> " + a);
                    break;
            }
        }
    }

    public void addMe(String name, String ipAddress) // erzeugt ein Peer Object mit den Host Informationen
    {

        tryUnlock();
        Player player = new Player(name, ModelTypeUtil.PlayerColor.values()[0]);
        GamePeer current = new GamePeer(name, ipAddress, 3333, player);
        itsMe = current;
        this.gameInstance.addUser(player);
        unlock();
        System.out.println("Hallo " + name + "");
    }

    public void setMaster(boolean master) {
        this.master = master;
    }

    public void addServer(String ipAddress) // erzeugt ein Peer Object mit den Host Informationen
    {
        itsMe = new Peer("Server", ipAddress, 3333);
        System.out.println("Hallo Server");
        setMaster(true);
    }

    private void requestRegistration(String name, String ipAddress) {
        if (master && knownPeers.size() < 7) {
            tryUnlock();
            Player player =
                    new Player(
                            name,
                            ModelTypeUtil.PlayerColor.values()[
                                    knownPeers.size() + (itsMe instanceof GamePeer ? 1 : 0)]);

            knownPeers.add(new GamePeer(name, ipAddress, 3333, player));
            this.gameInstance.addUser(player);
            unlock();
            sendAck(ipAddress);
        }
    }

    private void receiveAce(String name, String ipAddress) {

        tryUnlock();
        knownPeers.add(new Peer(name, ipAddress, 3333));
        conected = true;
        unlock();


    }

    public void register(String ipAddress) {
        String fin = "CON " + itsMe.name + " " + itsMe.ipAddess;
        send(fin, ipAddress, 3333);
    /*POKE <Name> <IP> <Port>
    Teilt mit, dass ein Peer mit dem Namen Name derzeit unter IP und Port erreichbar ist. Diese Nachricht kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
    */
    }

    public void sendAck(String ipAddress) {
        String fin = "ACK " + itsMe.name + " " + itsMe.ipAddess;
        send(fin, ipAddress, 3333);
    /*POKE <Name> <IP> <Port>
    Teilt mit, dass ein Peer mit dem Namen Name derzeit unter IP und Port erreichbar ist. Diese Nachricht kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
    */
    }
}
