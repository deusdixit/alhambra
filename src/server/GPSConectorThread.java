/*Thread der sich der verbunden Peers Annimmt und berprfft ob sie noch Online (lTime<60s) sind
 * */
package server;

public class GPSConectorThread extends Thread {
    Client
            client; // Allen Thrats wird das "Kernel" object bergeben damit sie vollen zugriff auf die Daten
    // zu erhalten
    String gps;
    String strength;

    public GPSConectorThread(
            Client client, String gps) // jedem peer wird der Client bergeben (fuer Zugriff)
    {
        this.client = client;
        this.gps = gps;
        strength = null;
    }

    public GPSConectorThread(
            Client client, String gps, String strength) // jedem peer wird der Client bergeben (fuer Zugriff)
    {
        this.client = client;
        this.gps = gps;
        this.strength = strength;
    }

    @Override
    public void run() {
        client.tryUnlock(); // Mutex
        if (strength == null) {
            client.broadCastGPS(gps);
        } else {
            client.broadCastFull(gps, strength);
        }
        client.unlock(); // Mutex
    }
}
