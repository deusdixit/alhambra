package server;

public class AlertThread extends Thread {
    Client client;
    Alert alert;

    public AlertThread(Client client, Alert alert) {
        this.client = client;
        this.alert = alert;
    }

    @Override
    public void run() {
        super.run();
        client.sendAlert(alert);
    }
}
