package server;

import model.game.Player;

public class GamePeer extends Peer {
    Player player;
    boolean newPlayer;

    GamePeer(String name, String ipAddress, int port, Player player) {
        super(name, ipAddress, port);
        this.player = player;
        newPlayer = true;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return name
                + "\n"
                + ipAddess
                + "\n"
                + port
                + "\n"
                + ((System.currentTimeMillis() / 1000) - lTime)
                + "s"
                + "\n"
                + player.getColor().name()
                + "\n";

    }

    public void acknowledged() {
        newPlayer = false;
    }

    public boolean isNewPlayer() {
        return newPlayer;
    }
}
