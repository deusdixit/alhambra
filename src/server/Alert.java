package server;

public class Alert {
    private String name, ipAdress, lastGPS;
    int port;

    public Alert(String ipAdress, int port, String name) {
        this.ipAdress = ipAdress;
        this.port = port;
        this.name = name;
        this.lastGPS = "noGPSonTest";
    }

    public Alert(String ipAdress, int port, String name, String lastGPS) {
        this.ipAdress = ipAdress;
        this.port = port;
        this.name = name;
        this.lastGPS = lastGPS;
    }

    public Alert(Peer peer) {
        name = peer.name;
        ipAdress = peer.ipAddess;
        port = peer.port;
        lastGPS = peer.getLastGPS();
    }

    public String getLastGPS() {
        return lastGPS;
    }

    public String getName() {
        return name;
    }

    public String getIpAdress() {
        return ipAdress;
    }

    public int getPort() {
        return port;
    }
}
