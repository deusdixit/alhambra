package server;
/*Thread zum Warten und verarbeiten von eingehenden Signallen
 *
 */

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class GameDataThread extends Thread // thread zum haendeln der eingehen Befehle
{
    GameClient client;
    boolean running = true;

    GameDataThread(GameClient client) {
        this.client = client;
    }

    @Override
    public void run() {


        while (running) {
            Socket socket01 = null;

            try {
                // System.out.println("Threat wartet auf eingang");
                if (client.dataServer != null) {
                    socket01 = client.dataServer.accept();
                    handleConnection(socket01);
                } else {
                    running = false;
                }

                // System.out.println("Threat Verarbeitet daten");

            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("PORT SCHON BELEGT!!!\n " + e.toString());
                off();
            } finally {
                if (socket01 != null) {
                    try {
                        socket01.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    void off() {
        running = false;
        if (client.dataServer != null) {
            try {
                client.dataServer.close();
                client.dataServer = null;
                System.out.println("Erfolgreich server beendet");
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Fehler beim beenden des Servers");
            }
        }
    }

    void handleConnection(Socket socket) {
        Scanner scanner;
        try {
            scanner = new Scanner(socket.getInputStream());
            if (scanner.hasNext()) {
                String stringLine = scanner.nextLine();
                client.actionHandler(stringLine);
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
