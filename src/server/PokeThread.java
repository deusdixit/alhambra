/*Thread -> wird verwendet um alle 20 sekunden alle Bekannten Peers anzupoken
 *
 *
 */
package server;

public class PokeThread extends Thread // Thread zum ueberpruefen ob die Peers noch leben
{
    Client client;
    boolean running = true;
    String finish;

    PokeThread(Client client) // Auch hier wird die Hauptkontrollklasse bvergeben
    {
        this.client = client;
    }

    @Override
    public void run() {
        while (running) {
            try {
                PokeThread.sleep(5 * 1000);
                client.tryUnlock(); // Sicherung der Bekannten peers durch sicherheitsvariable
                for (int i = 0; i < client.knownPeers.size(); i++) {
                    client.poke(client.knownPeers.get(i).getIP(), client.knownPeers.get(i).getPort());
                    // System.out.println("Es Wurde Erfolgreich Gepoked::THREAT");
                }
                client.unlock(); // hier wird entsichert

            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                System.out.println("Fehler bei aufbau der Verbindung zu Peer");
                // e.printStackTrace();
            } finally {
                if (client.key == true) {
                    client.unlock();
                }
            }
        }
    }

    void off() {
        running = false;
    }
}
