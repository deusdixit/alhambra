/*Klasse die Ip Adressen aus den Interfaces auflistet und berprft
 * */

package server;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

@SuppressWarnings("PMD")
public class ListNets {

    ArrayList<String> ipArr = new ArrayList<>(); // Speichert alle IP4 adressen (berfprfung mit che)

    Enumeration<NetworkInterface> nets;

    public ListNets() throws SocketException {
        nets = NetworkInterface.getNetworkInterfaces();
        for (NetworkInterface netint : Collections.list(nets)) {
            getIp(netint);
        }
    }

    void getIp(NetworkInterface netint) throws SocketException {

        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            add(inetAddress.getHostAddress());
        }
    }

    void add(String ipAddress) {
        if (checkIp(ipAddress)) {

            ipArr.add(ipAddress);
        }
    }

    public boolean checkIp(String input) // berprft ob die eingegebene IP gltig ist
    {
        if (input.equals("localhost")) {
            return true;
        }

        int count = 0;

        boolean bool1 = true, bool2 = true, bool3 = true, bool4 = true, bool5 = true;

        char[] hArr = input.toCharArray();
        char ttp = '.';
        for (int i = 0; i < input.length(); i++) // b1 abfrage ob 3x"." in dem string enthalten ist(IP4)
        {

            if (hArr[i] == ttp) {
                count++;
            }
        }
        int temp = 3;
        if (count != temp) {

            bool1 = false;
        }

        for (int i = 1; i < input.length(); i++) {
            if (hArr[i - 1] == hArr[i] && hArr[i] == '.') {
                bool2 = false;
                break;
            }
        }
        if (input.length() > 15 && input.length() < 7) {
            bool3 = false;
        }

        for (char b : hArr) {
            if (((int) b) <= 45 || ((int) b) >= 58 || ((int) b) == 47) {

                bool4 = false;
            }
        }
        if (bool1 && bool2 && bool3
                && bool4) // fehler Split funktioniert nicht -> berprfung ob num 1 oder num 4 "0 ist"
        {

            // System.out.println("Lnge von"+p+" "+p.length());
        }

        return bool1 && bool2 && bool3 && bool4 && bool5;
    }

    boolean checkPort(String inPut) // ueberprueft die gueltigkeit des ports
    {
        try {
            int port = Integer.parseInt(inPut);
            return port > 0;

        } catch (Exception e) {
            return false;
        }
    }

    public String getLocal() {

        for (int i = 0; i < ipArr.size(); i++) {
            if (!ipArr.get(i).substring(0, 3).equals("127")) {
                return ipArr.get(i);
            }
        }
        return "127.0.0.1";
    }

    public List<String> getLocals() {

        return ipArr;

    }
}
