/*
 * showChatTreat-> klasse zum printen der Texnachrichten die im Log{Linked list}
 * gespeichert sind
 * wird als thread ausgefrht
 *
 */

package server;

import java.util.LinkedList;

public class ShowChatTreat extends Thread {
    Client client;
    private boolean hasExport;
    LinkedList<String> output;

    // Dem Thread wird die Hauptkontrollkasse bergeben um vollen zufriff auf alle inhalte
    // zu bekomen

    public ShowChatTreat(Client client) {
        this.client = client;
        hasExport = false;
    }

    public ShowChatTreat(Client client, LinkedList<String> output) {
        this.output = output;
        this.client = client;
        hasExport = true;
    }

    @Override
    public void run() {
        while (true) {
            String[] out;
            if (client.log.getSize() > 0) {
                out = client.log.get();

                for (int i = 0; i < out.length; i++) {
                    if (!hasExport) {
                        System.out.println(out[i]);
                    } else {
                        System.out.println("EXPORT ====>>" + out[i]);
                        output.add(out[i]);
                    }
                }
            }
            try {
                ShowChatTreat.sleep(2 * 1000);
            } catch (Exception e) {
            } // Ess wird alle 2 sec aktualisiert
        }
    }
}
