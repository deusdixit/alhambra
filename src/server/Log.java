/**
 * Kopf der Linked list zur oragnisation von neuen nachrichten Besonderheit der get() methode ist
 * das sie die Nachrichten als String Array zurck gibt und die liste komplett leert
 */
package server;

import java.util.concurrent.TimeUnit;

public class Log {
    Message first, last;
    int size;
    boolean key = false;

    Log() {
        first = last = null;
        size = 0;
    }

    void add(Message neu) {
        tryUnlock();
        if (size == 0) {
            first = last = neu;
        } else {
            last.addNext(neu);
            last = neu;
        }
        size++;
        unlock();
    }

    public int getSize() {
        return size;
    }

    public String[] get() {
        tryUnlock();
        Message current = first;
        String[] help = new String[size];
        int run = 0;
        while (run < size) {
            help[run] = current.toString();
            run++;
            current = current.next;
        }

        clear();
        unlock();

        return help;
    }

    void clear() // Leert die liste
    {
        first = last = null;
        size = 0;
    }

    public void tryUnlock() // wird benutzt um das object zu sperren
    {
        if (key == false) {
            key = true;
        } else {
            try {
                TimeUnit.SECONDS.sleep(10);
                tryUnlock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void unlock() // entsperrt das object
    {
        key = false;
    }
}
