/*
 *To do Liste Fehlerbehnadlung
 *	1. send() ->Kein Socket zum verbinden
 *	2. startServer() -> Port bereits belegt
 */

/*
 * HautKontrollKlasse (Kernel)
 * Fuehrt eigentliche funtionen fuer Sever und Clintseite aus
 */
package server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("PMD")
public class Client {
    /**
     * Variable fuer eigene Informationen
     */
    public  Peer itsMe;
    /**
     * Array zur Speicherung der informationen der Entfernten CLients
     */
    ArrayList<Peer> knownPeers;
    /**
     * Klasse zum Verschluesseln (Der Standart Konstruktor erzeugt Default PassWord)
     */
    Krypto krypto = new Krypto();
    /**
     * Anzwahl der Maximal Peers die in der IM liste sind
     */
    int maxPeers = 12;
    /**
     * schluesselvariable zum verhindern des paralelen benutzens
     */
    boolean key = false;

    boolean hasAlert = false;
    Alert alert;
    /**
     * zwei sockets der eine zum empfangen der andere zum senden (BORGABE simplex)
     */
    ServerSocket server = null;

    Socket client;
    /**
     * Threat's -> siehe jeweilige Klasse
     */
    CheckListAndDeleteThreat thread01;

    ListenThreat thread02;
    PokeThread thread03;
    AlertThread alertThread;
    GPSConectorThread gpsThread;

    public Log log = new Log(); // Hier wird ein neuer Msg-Log erstellt

    /**
     * Konstrucktoren und Methoden
     */
    public Client() {
        knownPeers = new ArrayList<>();
    }

    /**
     * Funktion gibt Informationen ueber Online Peers aus (Poketime+Name+Port+IP) "ls" // ist der
     * Befehl fuer das UI
     *
     * @return peerList
     */
    public String listPeers() {
        StringBuilder out = new StringBuilder();
        int size = 0;
        for (Peer a1 : knownPeers) {
            if (a1 != null) {
                System.out.println(a1.toString() + "\n");
                out.append(a1.toString() + "\n");
                size++;
            }
        }
        System.out.println("Es gibt " + size + " known Peers");
        System.out.println("############################");
        return out.toString();
    }

    /**
     * Funktiuon um bei doppeltbelegung eines ports den direkten Nachfolger zu benutzen
     */
    void findFreeSocket() {
        try {
            server = new ServerSocket(itsMe.port);
            System.out.println(itsMe.port);
        } catch (Exception e) {
            System.out.println(
                    "Eingegebener Socket Bereits Belegt. Folgender Port wird nun verwendet:"); // Fehlerhandling hinzufgen bei belegtem port
            itsMe.nextPort();
            findFreeSocket();
        }
    }

    public void
    startServer() // Startet die ServerNotwendigen Threats und erzeugt einen neuen freien Server
    // Socket
    {
        thread01 = new CheckListAndDeleteThreat(this);
        thread01.start();

        findFreeSocket();

        thread02 = new ListenThreat(this);
        thread02.start();
        thread03 = new PokeThread(this);
        thread03.start();
    }

    public void
    killServer() // Beendet die Poke und SocketListening Threats (Disconnect)-> Client nicht
    // Beendet
    {
        thread02.off();
        thread03.off();
    }

    void send(
            String export,
            String ipAddress,
            int port) // Eigentliche Funtion zur uebertragung von Nachrichten(String) zischen den Peers
    {

        try {
            client = null;
            client = new Socket(ipAddress, port);
            PrintWriter out = new PrintWriter(client.getOutputStream());
            out.println(export);
            out.close();

        } catch (UnknownHostException e) // fehlerhandling erweitern
        {
            System.err.println(
                    "Die Angegebene IP ist nicht vergeben, oder es gibt ein Problem mit ihrer Netzwerkkarte");

            System.out.println("Fehler bei aufbau der Verbindung zu " + ipAddress + " : " + port);
            // e.printStackTrace();
            /*
             * Fehlerbehandlung
             *
             *
             *
             *
             */
        } catch (IOException e) {

            System.out.println("Fehler bei aufbau der Verbindung zu " + ipAddress + " : " + port);

        } finally {
            try {
                if (client != null) {
                    client.close(); // Socket verbindung Schliessen
                }
            } catch (IOException e) {
                System.out.println("Fehler bei aufbau der Verbindung zu " + ipAddress + " : " + port);
            }
        }
    }

    void send(
            Object export,
            String ipAddress,
            int port) // Eigentliche Funtion zur uebertragung von Nachrichten(String) zischen den Peers
    {

        try {
            client = null;
            client = new Socket(ipAddress, port);

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(client.getOutputStream());
            objectOutputStream.writeObject(export);
            objectOutputStream.close();

        } catch (UnknownHostException e) // fehlerhandling erweitern
        {
            System.err.println(
                    "Die Angegebene IP ist nicht vergeben, oder es gibt ein Problem mit ihrer Netzwerkkarte");

            System.out.println("Fehler bei aufbau der Verbindung zu " + ipAddress + " : " + port);
            // e.printStackTrace();
            /*
             * Fehlerbehandlung
             *
             *
             *
             *
             */
        } catch (IOException e) {

            e.printStackTrace();

        } finally {
            try {
                if (client != null) {
                    client.close(); // Socket verbindung Schliessen
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void poke(
            String ipAddress, int port) // Sendet meinen Peer in String Form an die ausgewaehlte adresse
    {
        String fin = "POKE " + itsMe.name + " " + itsMe.ipAddess + " " + itsMe.port;

        send(fin, ipAddress, port);
    /*POKE <Name> <IP> <Port>
    Teilt mit, dass ein Peer mit dem Namen Name derzeit unter IP und Port erreichbar ist. Diese Nachricht kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
    */
    }

    public void sendGPSandSignal(
            String ipAddress,
            int port,
            String gps,
            String signal) // Sendet meinen Peer in String Form an die ausgewaehlte adresse
    {
        String fin =
                "GPS " + itsMe.name + " " + itsMe.ipAddess + " " + itsMe.port + " " + gps + " " + signal;

        System.out.println(fin + " to " + ipAddress + " port " + port);
        send(fin, ipAddress, port);
    /*POKE <Name> <IP> <Port>
    Teilt mit, dass ein Peer mit dem Namen Name derzeit unter IP und Port erreichbar ist. Diese Nachricht kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
    */
    }

    public void sendGPS(
            String ipAddress,
            int port,
            String gps) // Sendet meinen Peer in String Form an die ausgewaehlte adresse
    {
        String fin = "GPS " + itsMe.name + " " + itsMe.ipAddess + " " + itsMe.port + " " + gps;

        System.out.println(fin + " to " + ipAddress + " port " + port);
        send(fin, ipAddress, port);
    /*POKE <Name> <IP> <Port>
    Teilt mit, dass ein Peer mit dem Namen Name derzeit unter IP und Port erreichbar ist. Diese Nachricht kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
    */
    }

    void ackPoke(String name, String ipAddress, int port) {

        int position = checkPeers(ipAddress, port); //
        if (position == -1) {

            addPeer(name, ipAddress, port);

        } else {

            knownPeers.get(position).updateTime();
        }
    }

    void ackGPS(String ipAddress, int port, String gps) // Funtion zum Handling von eingehenden Poke Befehlen
    {
        int position = checkPeers(ipAddress, port); //
        if (position == -1) // in diesem Fall ist der Peer noch nicht in der Liste enthalten und wird
        // Hinzugefuegt
        {
            System.err.println("Get GPS of Unknown Peer");
        } else // ansonsten wird die Zeit des Bekannten peers Aktualisiert
        {

            knownPeers.get(position).setLastGPS(gps);
            // knownPeers[position].updateTime();
        }
    }

    void ackGPSandStrength(
            String ipAddress,
            int port,
            String gps,
            String strength) // Funtion zum Handling von eingehenden Poke Befehlen
    {
        int position = checkPeers(ipAddress, port); //
        if (position == -1) // in diesem Fall ist der Peer noch nicht in der Liste enthalten und wird
        // Hinzugefuegt
        {
            System.err.println("Get Update of Unknown Peer");
        } else // ansonsten wird die Zeit des Bekannten peers Aktualisiert
        {

            knownPeers.get(position).setLastGPS(gps);
            knownPeers.get(position).setLastStrenght(strength);
            knownPeers.get(position).updateTime();
        }
    }

    public void disconnect() // Sendet den Disconnect befehl an alle bekannten Peers
    {
        String fin = "DISCONNECT " + itsMe.name + " " + itsMe.ipAddess + " " + itsMe.port;
        for (int i = 0; i < knownPeers.size(); i++) {
            send(fin, knownPeers.get(i).getIP(), knownPeers.get(i).getPort());
        }
    }

    /**
     * DISCONNECT <Name> <IP> <Port> Teilt mit, dass der Peer Name/IP/Port nun nicht mehr erreichbar
     * ist. Kann sowohl von Name selbst als auch von einem anderen Peer versendet werden.
     */
    void ackDisconnect(String number, String position, int value) // Handling von eingehenden Disconnect Befehlen
    {
        int h1 = checkPeers(position, value);
        if (h1 < 0) {

        } else {
            knownPeers.remove(
                    h1); // Setzt die zeit des Letzten Pokes um 120s zurueck und wird somit unmittelbar
            // entfert von "CheckListAndDeleteThreat"
        }
    }

    public void messageX(
            String name,
            String
                    mes) // Befehl sendet Nachricht mes an alle Peers mit namen name ...Eigentlicher Message
    // befehlt (im GUI ist msg und msgX vertauscht-> nur hier in der klasse ist es
    // umgekehrt)
    {
        for (int i = 0; i < knownPeers.size(); i++) {
            if (name.equals(knownPeers.get(i).name)) {
                message(knownPeers.get(i).getIP(), knownPeers.get(i).getPort(), (mes));
            }
        }
    }

    public void message(
            String ipAddress, int port, String mes) // Sendet Nachriten in String form an die ausgewaehlte adresse
    {
        String remName =
                ""
                        + knownPeers.get(checkPeers(ipAddress, port))
                        .name; // Gibt den Namen zurueck der zu IP und Port gehoert
        String fin =
                "MESSAGE "
                        + itsMe.name
                        + " "
                        + itsMe.ipAddess
                        + " "
                        + itsMe.port
                        + " "
                        + krypto.krypt(mes); // wie hier gut sichtbar wird nur die eigentliche Nachricht (mes)
        // verschluesselt SHA->Base64->AES

        log.add(new Message(remName, ipAddress, true, mes));
        send(fin, ipAddress, port);
    }

    void ackMessage(
            String name, String ipAddress, int port, String mes) // Funktion zum Handling eingehender nachrichten
    {
        if (ipAddress.equals(itsMe.ipAddess) && port == itsMe.port) {
            log.add(new Message(name, ipAddress, true, mes));
        } else {
            log.add(
                    new Message(
                            name,
                            ipAddress,
                            false,
                            krypto.deKrypt(
                                    mes))); // Ist die nachricht von einem Entfertnen Peer dann wird diese vorher
            // Entschlsselt
        }
    }

    int checkPeers(
            String ipAddress,
            int port) // ueberprueft ob ip und port bereits vergeben sind  und gibt die stelle im arr
    // zurueck
    {

        for (int i = 0; i < knownPeers.size(); i++) {
            if (knownPeers.get(i).getIP().equals(ipAddress) && knownPeers.get(i).getPort() == port) {

                return i;
            }
        }

        return -1;
    }

    public void addMe(
            String name, String ipAddress, int port) // erzeugt ein Peer Object mit den Host Informationen
    {
        itsMe = new Peer(name, ipAddress, port);
        System.out.println("Hallo " + name + "");
    }

    void addPeer(String name, String ipAddress, int port) // Fuegt neue Peers der Datenstrucktur hinzu
    {
        tryUnlock();
        knownPeers.add(new Peer(name, ipAddress, port));
        unlock();
    }

    void actionHandler(
            String
                    a) // Aktion Handler fuer eingehende Befehle -> Kann sein das hier noch Fehrler Handling
    // notwendig ist
    {
        if (a.length() > 3) {
            String[] arg = a.split(" ");
            switch (arg[0]) {
                case "ALARM":
                    addAlert(arg[1], Integer.parseInt(arg[2]), arg[3], arg[4]);
                    break;
                case "POKE":
                    ackPoke(arg[1], arg[2], Integer.parseInt(arg[3]));
                    break;
                case "GPS":
                    if (arg.length == 5) {
                        ackGPS(arg[2], Integer.parseInt(arg[3]), arg[4]);
                    } else if (arg.length == 6) {
                        ackGPSandStrength(arg[2], Integer.parseInt(arg[3]), arg[4], arg[5]);
                    }
                    break;
                case "DISCONNECT":
                    ackDisconnect(arg[1], arg[2], Integer.parseInt(arg[3]));
                    break;
                case "MESSAGE":
                    int max = arg.length;
                    String ins = "";
                    for (int x = 4; x < max; x++) {
                        ins += arg[x] + " ";
                    }
                    ackMessage(arg[1], arg[2], Integer.parseInt(arg[3]), ins);
                    break;

                default:
                    break;
            }
        }
    }

    public void tryUnlock() // wird benutzt um das object zu sperren
    {
        if (key == false) {
            key = true;
        } else {
            try {
                TimeUnit.SECONDS.sleep(2);
                System.out.println("ASSERT:Mutex");
                tryUnlock();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void unlock() // entsperrt das object
    {
        key = false;
    }

    public Peer getItsMe() {
        return itsMe;
    }

    public Peer[] getKnownPeers() {
        return (Peer[]) knownPeers.toArray();
    }

    public Log getLog() {
        return log;
    }

    public void addServer(String ipAddress, int port) {
        addPeer("Server", ipAddress, port);
    }

    public boolean isHasAlert() {
        return hasAlert;
    }

    public void addAlert(String ipAddress, int port, String name) {
        System.out.println("Add Allert ->" + ipAddress + " ->" + port + " -> " + name);
        alert = new Alert(ipAddress, port, name);
        hasAlert = true;
    }

    public void addAlert(String ipAddress, int port, String name, String gps) {
        System.out.println("Add Allert ->" + ipAddress + " ->" + port + " -> " + name + " ->" + gps);
        alert = new Alert(ipAddress, port, name, gps);
        hasAlert = true;
    }

    public Alert getAlert() {
        return alert;
    }

    public void removeAlert() {
        hasAlert = false;
        alert = null;
    }

    public void startAlertThread(Alert alert) {

        alertThread = new AlertThread(this, alert);
        alertThread.start();
    }

    public void sendAlert(Alert alert) {
        if (alert != null) {
            sendAlert(alert.getIpAdress(), alert.getPort(), alert.getName(), alert.getLastGPS());
        } else {
            System.out.println("Empty Alert");
        }
    }

    public void sendAlert(String ipAddress, int port, String name, String gps) {
        String out = "";
        if (knownPeers.size() > 0) {
            out = "ALARM " + ipAddress + " " + port + " " + name + " " + gps;
            for (int i = 0; i < knownPeers.size() && knownPeers.get(i) != null; i++) {
                send(out, knownPeers.get(i).getIP(), knownPeers.get(i).getPort());
            }
            log.add(
                    new Message("Alle", "", true, "\nAlarm gesendet:" + "\nNutzer: " + name + " getrennt"));
        }
    }

    public void androidGPS(String gps) {
        gpsThread = new GPSConectorThread(this, gps);
        gpsThread.start();
    }

    public void androidFull(String gps, String strg) {
        gpsThread = new GPSConectorThread(this, gps, strg);
        gpsThread.start();
    }

    public void broadCastGPS(String gps) {
        System.out.println("Start BroadCast");

        if (knownPeers.size() > 0) {
            for (int i = 0; i < knownPeers.size() && knownPeers.get(i) != null; i++) {
                sendGPS(knownPeers.get(i).getIP(), knownPeers.get(i).getPort(), gps);
            }
        }
        System.out.println("End BroadCast");
    }

    public void broadCastFull(String gps, String strng) {
        System.out.println("Start BroadCast");

        if (knownPeers.size() > 0) {
            for (int i = 0; i < knownPeers.size() && knownPeers.get(i) != null; i++) {
                sendGPSandSignal(knownPeers.get(i).getIP(), knownPeers.get(i).getPort(), gps, strng);
            }
        }
        System.out.println("End BroadCast");
    }
}
