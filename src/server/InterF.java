/*Hauptklasse und GUI zur kontrolle und interaktion mit dem eigentlichen Client
 * */

package server;

import model.GameInstance;
import util.GameInstanceUtil;

import java.net.SocketException;
import java.util.Scanner;

@SuppressWarnings("PMD")
public class InterF {
    static final float version = 0.1f;
    static GameClient gameClient;
    static ShowChatTreat showChatThread;
    static boolean running = true; // Exit Variable

    static Scanner scanner = new Scanner(System.in); // Befahleingabe
    static ListNets listNetsUtil; // Klasse um IP adresse fr den quickStart zu bekommen

    public static void main(String[] args) // Start unterschiedlich je nach Args laenge
    {
        GameInstance gameInstance = new GameInstance();


        gameInstance.addObserver(evt -> {
            System.out.println("Changed");
        });

        gameInstance.setCardStack(GameInstanceUtil.generateMoneyCards());
        gameInstance.setBuildingStack(GameInstanceUtil.generateBuildingTile());


        gameClient = new GameClient(gameInstance);
        showChatThread = new ShowChatTreat(gameClient);

        if (args.length != 2) {
            try {
                listNetsUtil = new ListNets();
                wellcome();
            } catch (SocketException e) {
                // TODO Auto-generated catch block
                wellcome();
            }
        }
        if (args.length == 2) // Die Geforderte Konfiguration!!!!
        {
            try {
                listNetsUtil = new ListNets(); // Netzwerk Klasse mit einigen Funktionen
                quickStart(args[0], args[1]); // Schnellstart
            } catch (Exception e) {
                wellcome(); // Wizzard zum Starten des Chats
            }
        }

        showChatThread.start();

        while (running) {
            run(gameClient);
        }
    }

    static void run(
            Client a) // Methode zur eigentlichen abfrage der Befehle im Terminal(CMD) -> help 4 Hilfe
    {

        String in = "";

        in = scanner.nextLine();

        String[] inArr = in.split(" "); // Substrings -> zerstueckeln durch " "
        switch (inArr[0]) {
            case "con":
            case "Connect":
            case "connect":
                if (inArr.length == 3) {
                    gameClient.poke(inArr[1], Integer.parseInt(inArr[2]));
                } else if (inArr.length == 2) {
                    gameClient.poke(inArr[1], 3333);
                }
                break;
            case "reg":
            case "Register":
            case "register":
                if (inArr.length > 1) {
                    gameClient.register(inArr[1]);
                }
                break;
            case "info":
                GameInstanceUtil.printStatistic(gameClient.gameInstance);
                break;
            case "Disconnect":
            case "disconnect":
                gameClient.killServer();
                gameClient.disconnect();
                break;
            case "list":
            case "ls":
                gameClient.listPeers();
                break; // Befehl nicht Gefordert aber unabdinghbar fr die Einwandfreie Nutzung und ansicht
            // der verbundenen Clients
            case "Exit":
            case "exit":
            case "quit":
                gameClient.killServer();
                gameClient.disconnect();
                running = false;
                System.out.println("Bis zum naechsten mal");
                scanner.close();
                System.exit(0);
                break;
            case "kill":
                gameClient.killServer();
                break;
            case "M":
            case "m":
                gameClient.messageX(inArr[1], nice(inArr, 2));
                break;
            case "":
                System.out.println();
                break;
            case "MX":
            case "mx":
            case "Mx":
                gameClient.message(inArr[1], Integer.parseInt(inArr[2]), nice(inArr, 3));
                break;
            case "alert":
                gameClient.sendAlert(new Alert("192.168.1.22", 3333, "Testuser", "51.4928009-7.4142037"));
                break;
            case "gps":
                gameClient.androidGPS("51.4928009-7.4142037");
                break;
            // case "gps":c.broadCastGPS("71.00-1.231231");break;
            case "help":
            case "Help":
                help();
                break;

            default:
                System.err.println("Falsche eingabe!!!\nhelp fuer hilfe");
                break; // error
        }
    }

    static String nice(
            String[] arr,
            int
                    pos) // Funktion zur rekonstruktion von strings die vorher durch split geteilt wurden und
    // einen teil am anfang wegscheneidet
    {
        int max = arr.length;
        String ins = "";
        for (int x = pos; x < max - 1; x++) {
            ins += arr[x] + " ";
        }
        ins += arr[max - 1];
        return ins;
    }

    static void wellcome() // Startbildschirm zum einrichten des Chat Programms
    {
        head();
        String h2;
        System.out.println("Starte Alhambra Multilayer Server Standalone");

        int count = 0;
        for (String a : listNetsUtil.getLocals()) {
            System.out.println("[" + count + "] " + a);
            count++;
        }
        do {
            System.out.println("Bitte waehlen sie ihren IP  aus: [0-" + (count - 1) + "]");
            h2 = scanner.nextLine();
            if (h2.contains("auto")) {
                h2 = listNetsUtil.getLocal();
                System.out.println(h2);
                break;
            }
            try {
                int current = Integer.parseInt(h2);
                h2 = listNetsUtil.getLocals().get(current);
            } catch (Exception e) {
                System.err.println("Ungueltige eingabe");
            }
        } while (!listNetsUtil.checkIp(h2));


        System.out.println("Starte Socket auf PORT 3333");
        gameClient.addServer(h2);

        // legt mich selbst als peer an aber seperat //Java soll sich ip selber holen

        gameClient.startServer(); // start der Serverseitigen grundfuntkionen
        help();
        System.out.println("Was soll  erledigt werden ?");
    }

    private static void head() {
        System.out.println(
                "##################################################################################");
        System.out.println("# Willkommen bei Alhambra Multyplayer" + version + " 						#");
        System.out.println("#										#");
        System.out.println("# Server Startet					#");
        System.out.println(
                "##################################################################################");

        System.out.println(
                "                             ______________________________________________");
        System.out.println(
                "                          .-'                     _                        '.");
        System.out.println(
                "                        .'                       |-'                        |");
        System.out.println(
                "                      .'                         |                          |");
        System.out.println(
                "                   _.'               p         _\\_/_         p              |");
        System.out.println(
                "                _.'                  |       .'  |  '.       |              |");
        System.out.println(
                "           __..'                     |      /    |    \\      |              |");
        System.out.println(
                "     ___..'                         .T\\    ======+======    /T.             |");
        System.out.println(
                "  ;;;\\::::                        .' | \\  /      |      \\  / | '.           |");
        System.out.println(
                "  ;;;|::::                      .'   |  \\/       |       \\/  |   '.         |");
        System.out.println(
                "  ;;;/::::                    .'     |   \\       |        \\  |     '.       |");
        System.out.println(
                "        ''.__               .'       |    \\      |         \\ |       '.     |");
        System.out.println(
                "             ''._          <_________|_____>_____|__________>|_________>    |");
        System.out.println(
                "                 '._     (___________|___________|___________|___________)  |");
        System.out.println(
                "                    '.    \\;;;Sopra;;o;;;;;o;;;;;o;;;;;o;;;;;o;;;;;o;;;;/   |");
        System.out.println(
                "                      '.~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~   |");
        System.out.println(
                "                        '. ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~  |");
        System.out.println(
                "                          '-.______________________________________________.'");
    }

    static void help() // Zeigt die hilfe an und damit die befehlsbersicht
    {
        System.out.println(
                "##################################################################################");
        System.out.println("Connect <ip> <port> ");
        System.out.println("Disconnect -> Trennt alle Verbindungen  ");
        System.out.println("List -> Zeigt alle Peers an ");
        System.out.println("M <Name> <Text> -> Sendet ann alle Peers mit this.name==name ");
        System.out.println("Mx <ip> <port> <Text> -> Sendet nachtricht an explizieten Client");
        System.out.println("Exit -> Beendet das Programm");
        System.out.println("Help -> Zeigt diese Hilfe an");
        System.out.println("kill -> Schliesst den Socket");
        System.out.println("info -> zeigt server status");
        System.out.println("alert -> sendet einen Alarm an alle peers");
        // System.out.println("gps -> sendet einen TestGPS signal an alle peers");
        System.out.println(
                "Info: Generelle Startoptionen \"java -jar *.jar <Name> <Port>\"-> fr Schnellstart oder \"java -jar *.jar\"->ffnet den Wizzard ");
        System.out.println(
                "##################################################################################");
        // ausgabe von befehlen
    }

    static void quickStart(
            String inputOne,
            String
                    inputTwo) /// (Name,Port)ermglich quickStart durch args[] argumente. Bei Bereits Belegtem Port
    // wird der Nchsthhere Benutzt bis ein Freier Port Gefunden ist
    {
        try {
            gameClient.addMe(inputOne, listNetsUtil.getLocal(), Integer.parseInt(inputTwo));
            // legt mich selbst als peer an aber seperat //Java soll sich ip selber holen
            gameClient.startServer();
            head();
            System.out.println("Hallo " + inputOne);
            System.out.println("Ihre Daten sind: " + listNetsUtil.getLocal() + ":" + gameClient.itsMe.port);
            System.out.println(
                    " Moechten sie eine Andere Netzwerkkarte verwenden bitte benutzen sie den Wizzard (Args.length!=2)");
            help();
            System.out.println("Was soll  erledigt werden ?");
        } catch (Exception e) {
            System.out.println("Ihre Eingabe ist nicht Korrekt sie werden zum Wizzard weitergeleitet");
            wellcome();
        }
    }
}
