package component;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import static util.FXUtil.projectScale;

public class WindowBanner extends StackPane {
  static final int WIDTH = 640, HEIGHT = 326;
    ImageView imageView;
    Text text;

    public WindowBanner(String message) {
        super();
        this.imageView = new ImageView(new Image("/res/img/banner/windowBanner.png", WIDTH, HEIGHT * 0.5, false, false));

        projectScale(this);

        HBox hBox = new HBox();
        text = new Text();
        text.setText(message);
        text.setFill(Color.WHITE);
//        -fx-text-fill: #493202 ;-fx-font-size: 20;  -fx-font-family: 'Viner Hand ITC'; -fx-font-weight: bold;
          text.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, 29));

        hBox.getChildren().add(text);
        hBox.setAlignment(Pos.CENTER);

        this.getChildren().addAll(imageView, hBox);
    }
}
