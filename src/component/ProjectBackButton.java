package component;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ProjectBackButton extends Button {
    private final static int SIZE = 100;

    public ProjectBackButton() {
        super();
        this.setPadding(new Insets(15, 0, 0, 15));
        Image image = new Image("res/img/button/backbutton.png", SIZE, SIZE, false, false);
        this.setGraphic(new ImageView(image));
        this.setWidth(SIZE);
        this.setHeight(SIZE);
        this.setStyle(
                " -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;");
    }
}
