package component.mainwindow;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import model.board.MoneyCard;

public class HandCard extends Money {

    HandCardView handCardView;
    HandCard(MoneyCard card,HandCardView handCardView) {
        super(card);
        this.handCardView=handCardView;
    }

    @Override
    public void onClick() {
        if (handCardView.getType() == null) {
            handCardView.setType(card.getType());
            toggle();
        } else if (handCardView.getType() == card.getType()) {
            toggle();
        }

    }
    @Override
     public void toggle() {
        if (!selected) {
            this.setTranslateY(-20);
            number.setTextFill(Color.GREEN);

        } else {
            this.setTranslateY(0);
            number.setTextFill(Color.WHITE);

        }
        selected = !selected;
        handCardView.updateList();
    }

}
