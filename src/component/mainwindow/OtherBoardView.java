package component.mainwindow;

import controller.MainLogicController;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import model.BoardPosition;
import model.GameInstance;
import model.board.BuildingTile;
import model.game.GameBoard;
import model.game.Player;
import model.strategy.AddFromReserveStrategy;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.DestroyBuildingStrategy;
import model.strategy.SwapBuildingsStrategy;
import util.FXUtil;
import util.exception.InvalidGameTurnExecption;

import java.util.LinkedList;

import static util.GameLogicUtil.validField;

public class OtherBoardView extends GridPane {
    Player player;
    int numRows;
    int numColumns;
    int borderWidth;
    int borderHeight;
    MainWindowView.State state;
    GameBoard gameBoard;
    Pane paneBoard[][];
    MainLogicController api;
    GameInstance gameInstance;
    OtherBoardView(MainLogicController api, MainWindowView.State state, Player player) {
        this.player = player;
        this.gameInstance= api.getGameInstance();
        this.api=api;
        this.state=state;
        populateBoard();
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(6);
        innerShadow.setBlurType(BlurType.TWO_PASS_BOX);
        DropShadow dropShadow = new DropShadow();
        dropShadow.setWidth(3);
        paneBoard =new Pane[21][21];
        this.setEffect(dropShadow);
        this.setEffect(innerShadow);
        this.gameBoard = this.player.getGameBoard();
        this.setStyle("-fx-background-color: rgba(255,213,154,0.9)");
        this.numRows = 21;
        this.numColumns = 21;
        this.borderHeight = 900;
        this.borderWidth = 900;
        this.setPrefSize(borderWidth, borderHeight);

        for (int row = 0; row < numRows; row++) {
            RowConstraints rowContraits = new RowConstraints();
            rowContraits.setFillHeight(true);
            rowContraits.setVgrow(Priority.ALWAYS);
            this.getRowConstraints().add(rowContraits);
        }
        for (int col = 0; col < numColumns; col++) {
            ColumnConstraints columnContains = new ColumnConstraints();
            columnContains.setFillWidth(true);
            columnContains.setHgrow(Priority.ALWAYS);
            this.getColumnConstraints().add(columnContains);
        }

        populateBoard();
        this.setGridLinesVisible(true);
        //this.add(new BuildingTileView(new BuildingTile()),numRows/2,numColumns/2);
    }

    public void populateBoard() {

        for (int i = 0; i < this.numRows; i++) {
            for (int j = 0; j < this.numColumns; j++) {
                Pane node;
                if (gameBoard.getBoard()[i][j] != null) {
                    node = new BuildingTileView(gameBoard.getBoard()[i][j]);
                } else {
                    Pane pane = new Pane();
                    pane.setStyle("-fx-min-width: 200;-fx-min-height: 200");
                    pane.getChildren().add(new Text());
/*
                    pane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                        pane.getChildren().clear();
                        pane.getChildren().add(new BuildingTileView(GameInstanceUtil.randomBuildingTile()));

                    });
* */

                    node = pane;
                }
                this.add(node, i, j);
                //  pane.getChildren().add(new ImageView (new Image("/res/img/building/garten.png", 200, 200, false, false)));
                paneBoard[i][j]=node;

            }
        }
    }
}
