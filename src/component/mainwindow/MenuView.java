package component.mainwindow;

import component.MenuButton;
import controller.GameLogicController;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.web.WebView;
import model.GameInstance;
import util.FXUtil;

import static util.FXUtil.projectScale;


public class MenuView extends VBox {
    MenuButton back, save, settings, exit,  tutorial, hint;

    public MenuView(MainWindowView windowView, GameInstance gameInstance) {
        back = new MenuButton("Fortsetzen");
        save = new MenuButton("Speichern");
        settings = new MenuButton("Optionen");
        exit = new MenuButton("Beenden");

        tutorial = new MenuButton("Anleitung");
        hint = new MenuButton("Tipp");
        this.getChildren().addAll(back, hint, tutorial, save, settings, exit);
        this.setSpacing(10);
        this.setAlignment(Pos.CENTER);
        initializeButtonFunctions(windowView, gameInstance);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        initializeButtonFunctions(windowView, gameInstance);
    }

    private void initializeButtonFunctions(MainWindowView mainWindowView, GameInstance gameInstance) {
        back.setOnAction(actionEvent -> {
            //TODO
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
        tutorial.setOnAction(action -> {

            WebView view = new WebView();
            String url = getClass().getResource("/docs/site/index.html").toExternalForm();
            view.getEngine().load(url);
            view.setMaxWidth(1200);
            view.setMaxWidth(800);
            MenuButton backButton = new MenuButton("Zurück");
            backButton.setOnAction(backAction -> {
                mainWindowView.hideDialog();
            });
            VBox vBox = new VBox(view, backButton);
            vBox.setAlignment(Pos.CENTER);
            mainWindowView.showDialog(vBox);
        });
        settings.setOnAction(actionEvent -> {
            mainWindowView.showDialog(new MenuSetting(mainWindowView));
            FXUtil.setMenuButtonSound();
        });
        //TODO save action
        save.setOnAction(actionEvent -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.showDialog(new SaveGameDialog(mainWindowView,gameInstance));
        });
        //TODO add save before exit
        exit.setOnAction(actionEvent -> {
            mainWindowView.starViewController.init(false);

        });
        hint.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            if (gameInstance.getCurrentPlayer().getCheater()){
                mainWindowView.showDialog(new HintView(mainWindowView, GameLogicController.getHint(gameInstance)));
            }else {
                mainWindowView.showDialog(new WarningView(mainWindowView, gameInstance));
            }
        });
    }


}
