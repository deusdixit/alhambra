package component.mainwindow;

import component.HiScore;
import component.MenuButton;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import model.GameInstance;
import model.game.Player;
import model.strategy.EvaluationStrategy;
import util.ConfigUtil;
import util.FXUtil;
import util.GameLogicUtil;

import java.util.*;

import static util.FXUtil.projectScale;

public class EvaluationView extends StackPane {

    EvaluationView(MainWindowView mainWindowView, GameInstance gameInstance, int evalNumber, EvaluationStrategy lastEval) {
        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        String labelString;
        switch(evalNumber){
            case 1:labelString = "1. Wertung";break;
            case 2:labelString = "2. Wertung";break;
            case 3:labelString = "3. Und finale Wertung";break;
            default:  labelString = "Ups, falsche Nummer der Wertungsrunde";break;
        }

        Text playerName;
        Text wallScore;
        Text pavillonScore;
        Text serailScore;
        Text arkadenScore;
        Text gemaecherScore;
        Text gartenScore;
        Text turmScore;
        Text addedScore;
        Text totalScore;


        VBox playerInfo = new VBox();
        playerInfo.setAlignment(Pos.CENTER);
        playerInfo.setSpacing(30);

        Text nameHeader = new Text("Name");
        nameHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane nameHeaderPane  = new Pane();
        nameHeaderPane.setPrefWidth(75);
        //nameHeaderPane.setStyle("-fx-min-width: 50");
        nameHeaderPane.getChildren().add(nameHeader);
        Text pavillonHeader = new Text("Pavillon");
        pavillonHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane pavillonHeaderPane  = new Pane();
        pavillonHeaderPane.setPrefWidth(75);
        pavillonHeaderPane.getChildren().add(pavillonHeader);
        Text serailHeader = new Text("Serail");
        serailHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane serailHeaderPane  = new Pane();
        serailHeaderPane.setPrefWidth(75);
        serailHeaderPane.getChildren().add(serailHeader);
        Text arkadenHeader = new Text("Arkaden");
        arkadenHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane arkadenHeaderPane  = new Pane();
        arkadenHeaderPane.setPrefWidth(75);
        arkadenHeaderPane.getChildren().add(arkadenHeader);
        Text gemaecherHeader = new Text("Gemächer");
        gemaecherHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane gemaecherHeaderPane  = new Pane();
        gemaecherHeaderPane.setPrefWidth(100);
        gemaecherHeaderPane.getChildren().add(gemaecherHeader);
        Text gartenHeader = new Text("Garten");
        gartenHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane gartenHeaderPane  = new Pane();
        gartenHeaderPane.setPrefWidth(75);
        gartenHeaderPane.getChildren().add(gartenHeader);
        Text turmHeader = new Text("Turm");
        turmHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane turmHeaderPane  = new Pane();
        turmHeaderPane.setPrefWidth(75);
        turmHeaderPane.getChildren().add(turmHeader);
        Text wallHeader = new Text("Mauer");
        wallHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane wallHeaderPane  = new Pane();
        wallHeaderPane.setPrefWidth(75);
        wallHeaderPane.getChildren().add(wallHeader);
        Text addedScoreHeader = new Text("Summe");
        addedScoreHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane addedScoreHeaderPane  = new Pane();
        addedScoreHeaderPane.setPrefWidth(75);
        addedScoreHeaderPane.getChildren().add(addedScoreHeader);
        Text totalScoreHeader = new Text("Neuer Score");
        totalScoreHeader.setFont(Font.font("Viner Hand ITC", 22));
        Pane totalScoreHeaderPane  = new Pane();
        totalScoreHeaderPane.setPrefWidth(75);
        totalScoreHeaderPane.getChildren().add(totalScoreHeader);


        HBox evalHeader = new HBox(nameHeaderPane,pavillonHeaderPane,serailHeaderPane,arkadenHeaderPane,gemaecherHeaderPane,
                gartenHeaderPane,turmHeaderPane,wallHeaderPane,addedScoreHeaderPane,totalScoreHeaderPane);
        evalHeader.setSpacing(10);

        playerInfo.getChildren().add(evalHeader);

            int[][] scoreStore = lastEval.getAllScores();

            for (int playerIndex = 0; playerIndex < gameInstance.getPlayerList().size(); playerIndex++) {
                playerName = new Text(gameInstance.getPlayerList().get(playerIndex).getName());
                playerName.setFont(Font.font("Viner Hand ITC", 22));
                Pane playerNamePane = new Pane();
                playerNamePane.setPrefWidth(75);
                playerNamePane.getChildren().add(playerName);
                pavillonScore = new Text(scoreStore[playerIndex][0] + "");
                pavillonScore.setFont(Font.font("Viner Hand ITC", 22));
                pavillonScore.setTextAlignment(TextAlignment.RIGHT);
                Pane pavillonScorePane = new Pane();
                pavillonScorePane.setPrefWidth(75);
                pavillonScorePane.getChildren().add(pavillonScore);
                serailScore = new Text(scoreStore[playerIndex][1] + "");
                serailScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane serailScorePane = new Pane();
                serailScorePane.setPrefWidth(75);
                serailScorePane.getChildren().add(serailScore);
                arkadenScore = new Text(scoreStore[playerIndex][2] + "");
                arkadenScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane arkadenScorePane = new Pane();
                arkadenScorePane.setPrefWidth(75);
                arkadenScorePane.getChildren().add(arkadenScore);
                gemaecherScore = new Text(scoreStore[playerIndex][3] + "");
                gemaecherScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane gemaecherScorePane = new Pane();
                gemaecherScorePane.setPrefWidth(100);
                gemaecherScorePane.getChildren().add(gemaecherScore);
                gartenScore = new Text(scoreStore[playerIndex][4] + "");
                gartenScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane gartenScorePane = new Pane();
                gartenScorePane.setPrefWidth(75);
                gartenScorePane.getChildren().add(gartenScore);
                turmScore = new Text(scoreStore[playerIndex][5] + "");
                turmScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane turmScorePane = new Pane();
                turmScorePane.setPrefWidth(75);
                turmScorePane.getChildren().add(turmScore);
                wallScore = new Text(GameLogicUtil.getLargestOuterWallLength
                        (gameInstance.getPlayerList().get(playerIndex).getGameBoard().getBoard()) + "");
                wallScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane wallScorePane = new Pane();
                wallScorePane.setPrefWidth(75);
                wallScorePane.getChildren().add(wallScore);
                addedScore = new Text(0 + "");
                addedScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane addedScorePane = new Pane();
                addedScorePane.setPrefWidth(75);
                addedScorePane.getChildren().add(addedScore);
                totalScore = new Text(gameInstance.getPlayerList().get(playerIndex).getScore() + "");
                totalScore.setFont(Font.font("Viner Hand ITC", 22));
                Pane totalScorePane = new Pane();
                totalScorePane.setPrefWidth(75);
                totalScorePane.getChildren().add(totalScore);
                HBox temp = new HBox(playerNamePane, pavillonScorePane, serailScorePane, arkadenScorePane,
                        gemaecherScorePane, gartenScorePane, turmScorePane, wallScorePane, addedScorePane, totalScorePane);
                temp.setSpacing(10);
                playerInfo.getChildren().add(temp);

            }





        Label label = new Label(labelString);
        label.setFont(Font.font("Viner Hand ITC", 30));
        label.setAlignment(Pos.CENTER);
        label.setTextFill(Color.PURPLE);
        MenuButton cont = new MenuButton("Weiter");
        //MenuButton cancel = new MenuButton("Zurück");
        HBox buttonLine = new HBox(cont);
        buttonLine.setAlignment(Pos.CENTER);
        buttonLine.setSpacing(10);
        VBox vBox = new VBox(label, playerInfo, buttonLine);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(900);
        this.setMinWidth(900);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);
        cont.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            if (evalNumber==3){
                LinkedList<ConfigUtil.HighScore> tmpList=ConfigUtil.load().getHighScores();
                LinkedList<ConfigUtil.HighScore> newList=new LinkedList<>();

                for (Player current:gameInstance.getPlayerList()){
                    if (!current.getCheater()) {
                        tmpList.add(new ConfigUtil.HighScore(current,gameInstance));
                    }
                }
                Collections.sort(tmpList);

                for (int i=0;i<tmpList.size()&&i<10;i++){
                    newList.add(tmpList.get(i));
                }
                ConfigUtil.load().setHighScores(newList);
                ConfigUtil.save();

                    mainWindowView.showDialog(new HiScore(mainWindowView.starViewController));

            }else {
                mainWindowView.hideDialog();
                mainWindowView.resume();
            }
        });
    }
}