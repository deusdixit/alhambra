package component.mainwindow;

import controller.MainLogicController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import model.GameInstance;
import model.board.MoneyCard;
import model.strategy.RobBankStrategy;
import server.Log;
import util.exception.InvalidGameTurnExecption;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static util.FXUtil.projectScale;


public class Bank extends HBox {
    int robmax = 5;
    int minsize = 1;
    MoneyCard[] bankCards;
    List<BankCard> selectedCards;
    GameInstance gameInstance;
    MainLogicController logicApi;

    Bank(MainLogicController mainLogicController) {
        super();
        logicApi = mainLogicController;
        this.bankCards = mainLogicController.getGameInstance().getBank();
        this.gameInstance = mainLogicController.getGameInstance();
        Arrays.stream(bankCards).forEach(moneyCard ->
        {
            if (moneyCard != null) {
                this.getChildren().add(new BankCard(moneyCard, this));
            }
        });
        this.setAlignment(Pos.BASELINE_CENTER);
        this.setSpacing(5);


    }

    /**
     * Update Ui
     * and rob if you can't select any more cards.
     */
    public void updateList() {
        ObservableList<Node> tmp = this.getChildren();
        selectedCards = new ArrayList<>();
        for (Node node : tmp) {
            if (node instanceof BankCard && ((BankCard) node).selected) {
                selectedCards.add((BankCard) node);
            }
        }
        rob();
    }

    private void rob() {
        int sum = 0;
        for (int i = 0; i < selectedCards.size(); i++) {
            sum = sum + selectedCards.get(i).getCard().getValue();
        }
        if (selectedCards.size() == minsize) {
            if (selectedCards.get(0).getCard().getValue() >= robmax) {
                robBank();
            } else {
                if (isRobable(sum)) {
                    robBank();
                }
            }
        }
        rob2(sum);
    }

    private void rob2(int sum) {
        if (selectedCards.size() > minsize) {
            if (selectedCards.size() == bankCards.length) {
                robBank();
            }
            if (selectedCards.size() == bankCards.length - 1) {
                int summ = 0;
                for (int i = 0; i < bankCards.length; i++) {
                    summ = summ + bankCards[i].getValue();
                }
                if (summ > robmax) {
                    robBank();
                }
            } else {//Fall: 2 karten
                if (isRobable(sum)) {
                    robBank();
                }
            }
        }
    }

    private boolean isRobable(int sum) {
        int card = 0;
        boolean robable = true;
        for (int i = 0; i < bankCards.length; i++) {
            if (notSelected(bankCards[i])) {
                card = bankCards[i].getValue();//if NullPointer here its because the bank has been robbed
                if (sum + card <= robmax) {
                    robable = false;
                }
            }
        }
        return robable;
    }

    private boolean notSelected(MoneyCard moneyCard) {//fehler bei zwei gleichen karten?
        for (BankCard scard : selectedCards) {
            if (scard.getCard() == moneyCard) {
                return false;
            }
        }
        return true;
    }

    private void robBank() {

        MoneyCard[] robCards = new MoneyCard[selectedCards.size()];
        for (int i = 0; i < selectedCards.size(); i++) {
            robCards[i] = selectedCards.get(i).getCard();
        }
        //System.out.println("hände hoch");
        /*RobBankStrategy robN =*/
        try {
            logicApi.execute(new RobBankStrategy(gameInstance, gameInstance.getCurrentPlayer(), robCards));
        } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
            invalidGameTurnExecption.printStackTrace();
        }
    }

}
