package component.mainwindow;

import controller.MainLogicController;
import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
import model.BoardPosition;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.BuildingTile;
import model.game.GameBoard;
import model.strategy.AddFromReserveStrategy;
import model.strategy.BuyPlaceBuildingStrategy;
import model.strategy.DestroyBuildingStrategy;
import model.strategy.SwapBuildingsStrategy;
import util.FXUtil;
import util.GameLogicUtil;
import util.exception.InvalidGameTurnExecption;

import java.util.LinkedList;
import java.util.Stack;

import static util.GameLogicUtil.validField;

public class BoardView extends GridPane {
    int numRows;
    int numColumns;
    int borderWidth;
    int borderHeight;
    MainWindowView.State state;
    GameBoard gameBoard;
    Pane paneBoard[][];
    MainLogicController api;
    GameInstance gameInstance;

    BoardView(MainLogicController api, MainWindowView.State state) {
        this.gameInstance = api.getGameInstance();
        this.api = api;
        this.state = state;
        setStateBuildingObserver();
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(6);
        innerShadow.setBlurType(BlurType.TWO_PASS_BOX);
        DropShadow dropShadow = new DropShadow();
        dropShadow.setWidth(3);
        paneBoard = new Pane[21][21];
        this.setEffect(dropShadow);
        this.setEffect(innerShadow);
        this.gameBoard = gameInstance.getCurrentPlayer().getGameBoard();
        this.setStyle("-fx-background-color: rgba(255,213,154,0.9)");
        this.numRows = 21;
        this.numColumns = 21;
        this.borderHeight = 900;
        this.borderWidth = 900;
        this.setPrefSize(borderWidth, borderHeight);

        for (int row = 0; row < numRows; row++) {
            RowConstraints rowContraits = new RowConstraints();
            rowContraits.setFillHeight(true);
            rowContraits.setVgrow(Priority.ALWAYS);
            this.getRowConstraints().add(rowContraits);
        }
        for (int col = 0; col < numColumns; col++) {
            ColumnConstraints columnContains = new ColumnConstraints();
            columnContains.setFillWidth(true);
            columnContains.setHgrow(Priority.ALWAYS);
            this.getColumnConstraints().add(columnContains);
        }

        populateBoard();
        this.setGridLinesVisible(true);
        //this.add(new BuildingTileView(new BuildingTile()),numRows/2,numColumns/2);
    }

    private void setStateBuildingObserver() {
        state.addBuildingObserver(act -> {
            if (((MainWindowView.State) act.getNewValue()).current != null) {
                showPossibleCandidates(((MainWindowView.State) act.getNewValue()).current);
            } else {
                populateBoard();
            }
        });
    }

    public void populateBoard() {

        for (int i = 0; i < this.numRows; i++) {
            for (int j = 0; j < this.numColumns; j++) {
                Pane node;
                if (gameBoard.getBoard()[i][j] != null) {
                    node = new BuildingTileView(gameBoard.getBoard()[i][j]);
                } else {
                    Pane pane = new Pane();
                    pane.setStyle("-fx-min-width: 200;-fx-min-height: 200");
                    pane.getChildren().add(new Text());
/*
                    pane.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                        pane.getChildren().clear();
                        pane.getChildren().add(new BuildingTileView(GameInstanceUtil.randomBuildingTile()));

                    });
* */

                    node = pane;
                }
                this.add(node, i, j);
                //  pane.getChildren().add(new ImageView (new Image("/res/img/building/garten.png", 200, 200, false, false)));
                paneBoard[i][j] = node;

            }
        }
    }

    private static LinkedList<BoardPosition> getValidPositions(BuildingTile tile, GameBoard gameBoard) {
        BuildingTile[][] board = gameBoard.getBoard();
        int alhambraPosX = board.length/2;
        int alhambraPosY= board[alhambraPosX].length/2;
        LinkedList<BoardPosition> validPosition = new LinkedList<>();

        boolean flag[][] = new boolean[board.length][board[0].length];
        Stack<BoardPosition> stack = new Stack<>();
        stack.push(new BoardPosition(alhambraPosX, alhambraPosY));

        while (!stack.isEmpty()){
            BoardPosition current = stack.pop();

            for(int x = -1; x <=1; x++){
                for(int y = -1; y <= 1; y++){
                    if(Math.abs(x) + Math.abs(y) == 2)
                        continue;
                    if(x == 0 && y == 0)
                        continue;
                    if(current.getX()+x < 0 || current.getY()+y < 0)
                        continue;
                    if(current.getX() + x >= board.length || current.getY() + y >= board.length)
                        continue;

                    BoardPosition newPosition = new BoardPosition(current.getX() + x, current.getY() + y);
                    if(flag[newPosition.getX()][newPosition.getY()])
                        continue;

                    flag[newPosition.getX()][newPosition.getY()] = true;
                    if(board[newPosition.getX()][newPosition.getY()] == null){
                        if(GameLogicUtil.validField(gameBoard, tile, newPosition))
                            validPosition.add(newPosition);

                    }else{
                        stack.push(newPosition);
                    }
                }
            }
        }
        return validPosition;
    }

    public void showPossibleCandidates(BuildingTile tile) {
        LinkedList<BoardPosition> positions = new LinkedList<>();
        BuildingTile[][] board = gameBoard.getBoard();

        long a = System.currentTimeMillis();
        positions= getValidPositions(tile, gameBoard);
        long b = System.currentTimeMillis();

        System.out.println(b-a);

        for (BoardPosition position : positions) {
            Rectangle cur = new Rectangle(200, 200);
            cur.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                try {
                    FXUtil.setPlacingSound();
                    api.execute(new BuyPlaceBuildingStrategy(tile, gameInstance, position, state.getSelected()));
                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                    invalidGameTurnExecption.printStackTrace();
                }

            });
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setNode(cur);
            fadeTransition.setDuration(Duration.seconds(2));
            fadeTransition.setCycleCount(Animation.INDEFINITE);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(0.5);
            fadeTransition.setAutoReverse(true);
            fadeTransition.play();

            fadeTransition.play();
            cur.setFill(Color.GREEN);
            paneBoard[position.getX()][position.getY()].getChildren().add(cur);

        }
    }

    public void showPossibleReserveCandidates(BuildingTile tile) {
        LinkedList<BoardPosition> positions = new LinkedList<>();
        BuildingTile[][] board = gameBoard.getBoard();
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] == null && validField(gameBoard, tile, new BoardPosition(x, y))) {
                    positions.add(new BoardPosition(x, y));
                }
            }
        }
        for (BoardPosition position : positions) {
            Rectangle cur = new Rectangle(200, 200);
            cur.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                try {
                    FXUtil.setPlacingSound();
                    api.execute(new AddFromReserveStrategy(gameInstance.getCurrentPlayer(), tile, gameInstance, position));
                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                    invalidGameTurnExecption.printStackTrace();
                }

            });
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setNode(cur);
            fadeTransition.setDuration(Duration.seconds(2));
            fadeTransition.setCycleCount(Animation.INDEFINITE);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(0.5);
            fadeTransition.setAutoReverse(true);
            fadeTransition.play();
            fadeTransition.play();
            cur.setFill(Color.GREEN);
            paneBoard[position.getX()][position.getY()].getChildren().add(cur);

        }
    }

    public void showPossibleSwapCandidates(BuildingTile tile) {
        LinkedList<BoardPosition> positions = new LinkedList<>();
        BuildingTile[][] board = gameBoard.getBoard();
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] != null && validField(gameBoard, tile, new BoardPosition(x, y))) {
                    positions.add(new BoardPosition(x, y));
                }
            }
        }
        for (BoardPosition position : positions) {
            Rectangle cur = new Rectangle(200, 200);
            cur.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                try {
                    FXUtil.setPlacingSound();
                    api.execute(new SwapBuildingsStrategy(gameInstance.getCurrentPlayer(), tile, gameInstance, position));
                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                    invalidGameTurnExecption.printStackTrace();
                }

            });
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setNode(cur);
            fadeTransition.setDuration(Duration.seconds(2));
            fadeTransition.setCycleCount(Animation.INDEFINITE);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(0.5);
            fadeTransition.setAutoReverse(true);
            fadeTransition.play();

            fadeTransition.play();
            cur.setFill(Color.BLUE);
            paneBoard[position.getX()][position.getY()].getChildren().add(cur);

        }
    }


    public void destroyBuilding(boolean modeActive) {
        LinkedList<BoardPosition> positions = new LinkedList<>();
        BuildingTile[][] board = gameBoard.getBoard();
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board.length; y++) {
                if (board[x][y] != null && modeActive && board[x][y].getType()!= ModelTypeUtil.BuildingType.BRUNNEN && validField(gameBoard, null, new BoardPosition(x, y))) {
                    positions.add(new BoardPosition(x, y));
                }
            }
        }
        for (BoardPosition position : positions) {
            Rectangle cur = new Rectangle(200, 200);
            cur.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                try {
                        FXUtil.setDestroySound();
                        api.execute(new DestroyBuildingStrategy(gameInstance.getCurrentPlayer(), gameInstance, position));
                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                    invalidGameTurnExecption.printStackTrace();
                }
            });
            FadeTransition fadeTransition = new FadeTransition();
            fadeTransition.setNode(cur);
            fadeTransition.setDuration(Duration.seconds(2));
            fadeTransition.setCycleCount(Animation.INDEFINITE);
            fadeTransition.setFromValue(0);
            fadeTransition.setToValue(0.5);
            fadeTransition.setAutoReverse(true);
            fadeTransition.play();

            fadeTransition.play();
            cur.setFill(Color.RED);
            paneBoard[position.getX()][position.getY()].getChildren().add(cur);
        }
    }


}
