package component.mainwindow;

import component.ProjectButton;
import controller.MainLogicController;
import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.ImagePattern;
import javafx.util.Duration;
import model.GameInstance;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.GameRound;
import model.game.Player;
import model.strategy.BuyReserveBuildingStrategy;
import model.strategy.RefillStrategy;
import model.strategy.StartGameStrategy;
import util.FXUtil;
import util.exception.InvalidGameLogicException;
import util.exception.InvalidGameTurnExecption;
import view.StarViewController;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static util.FXUtil.projectScale;


public class MainWindowView extends BorderPane {
    final static int FRAMESIZE = 500;
    @FXML
    private HBox handCards, butt1;
    @FXML
    private VBox players, reserve;
    @FXML
    private ImageView scoretable;
    @FXML
    private Button menuButton, next, back;

    boolean stop;
    int posX;
    int posY;
    double scale;
    boolean destroypress = false;
    ProjectButton destroy;
    Pane alhambraPane;
    BoardView boardView;
    OtherBoardView otherBoardView;
    HandCardView handCardView;
    MainLogicController mainLogicController;
    GameInstance gameInstance;
    BuildingYard buildingYard;
    HBox top;
    PlayerView[] playerViews;
    State state;
    StarViewController starViewController;
    FadeTransition hideRound,showRound,transition;

    /**
     * Window for the Game itself.
     */
    BorderPane dialog;

    public MainWindowView(StarViewController starViewController, GameInstance gameInstance, BorderPane dialog, BorderPane logPane) {
        stop=false;
        showRound=new FadeTransition();
        showRound.setNode(this);
        showRound.setDuration(Duration.seconds(1));
        showRound.setFromValue(0);
        showRound.setToValue(1);

        hideRound=new FadeTransition();
        hideRound.setNode(this);
        hideRound.setDuration(Duration.seconds(1));
        hideRound.setFromValue(1);
        hideRound.setToValue(0);
        hideRound.setDelay(Duration.seconds(4));

        hideRound.setOnFinished(action->{
            reInit();
            showRound.play();
        });
        showRound.setOnFinished(action->{
            performBotTurn();

        });


        this.dialog = dialog;
        this.starViewController = starViewController;
        init(gameInstance);

    }

    private void init(GameInstance gameInstance) {
        this.mainLogicController = new MainLogicController(gameInstance);
        for (Player player : gameInstance.getPlayerList()) {
            if (player instanceof Bot) {
                ((Bot) player).startBot(this.mainLogicController.getGameLogicController());
            }
        }
        this.gameInstance = gameInstance;
        this.state = new State();
        this.state.addMoneyObserver(evt -> {
            if (this.state.selected.isEmpty()) {
                FXUtil.setHandCardSound();
                reInit();
            } else {
                FXUtil.setHandCardSound();
                buildingYard.update((State) evt.getNewValue());
            }

        });
        gameInstance.addObserver(listener -> {
            reInit();
        });
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/layout/GameWindow.fxml"));
        loader.setController(this);


        try {
            this.mainLogicController.execute(new StartGameStrategy());
        } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
            invalidGameTurnExecption.printStackTrace();
        }
        BorderPane loadingPane;
        scale = 0.1;
        posX = -100;
        posY = -50;


        try {
            //HandCardView
            this.alhambraPane = new Pane();
            this.alhambraPane.setScaleY(scale);
            this.alhambraPane.setScaleX(scale);
            this.alhambraPane.setTranslateX(posX);
            this.alhambraPane.setTranslateY(posY);

            loadingPane = loader.load();

            BorderPane innerBorder = (BorderPane) loadingPane.getCenter();

            top = (HBox) loadingPane.getTop();
            top.setSpacing(5);

            innerBorder.setCenter(this.alhambraPane);
            this.setCenter(innerBorder);
            this.setTop(projectScale(top));
            this.setRight(projectScale(loadingPane.getRight()));
            this.setLeft(projectScale(loadingPane.getLeft()));
            this.setBottom(projectScale(loadingPane.getBottom()));

            //alhambraPane.getChildren().add(button);
            boardView = new BoardView(mainLogicController, state);
            alhambraPane.getChildren().add(boardView);
            starViewController.getMaistage().addEventHandler(KeyEvent.KEY_PRESSED, t -> {
                //System.out.println(t.getCode());
                switch (t.getCode()) {
                    case ESCAPE:
                        resume();
                        showDialog(new MenuView(this, gameInstance));
                        break;

                    case RIGHT:
                    case D:
                        posX += 40;
                        this.alhambraPane.setTranslateX(posX);
                        break;
                    case LEFT:
                    case A:

                        posX -= 40;
                        this.alhambraPane.setTranslateX(posX);

                        break;
                    case DOWN:
                    case S:
                        posY += 40;
                        this.alhambraPane.setTranslateY(posY);
                        break;
                    case UP:
                    case W:
                        posY -= 40;
                        this.alhambraPane.setTranslateY(posY);

                        break;
                }
            });
            starViewController.getMaistage().addEventHandler(ScrollEvent.SCROLL, t -> {
                this.scale += (t.getDeltaY() / 1000);
                if (this.scale < 0) {
                    this.scale = 0.1;
                }
                alhambraPane.setScaleY(scale);
                alhambraPane.setScaleX(scale);
            });

            handCardView = new HandCardView(gameInstance, this.state, this);
            handCards.getChildren().add(handCardView);
            //GameInstanceUtil.printStatistic(gameInstance);
            //PlayerStats

            buildingYard = new BuildingYard(gameInstance, this);
            top.getChildren().addAll(new Bank(mainLogicController), buildingYard, new MoBiStack(mainLogicController));

            selectCurrentPlayer();
            scoretable.setImage(new Image("/res/img/werrtung.png"));
            reserve.setBackground(new Background(new BackgroundFill(new ImagePattern(new Image("/res/img/building/reserve.png")), null, null)));

            reserve.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
                FXUtil.setPlacingSound();
                System.out.println(t.getX() + " + " + t.getY());
                BuildingTile realEstate = null;
                if (handCardView.getType() != null) {
                    switch (handCardView.getType()) {
                        case DENAR:
                            realEstate = gameInstance.getBuildingYard()[2];
                            break;
                        case DIRHAM:
                            realEstate = gameInstance.getBuildingYard()[1];
                            break;
                        case DUKATEN:
                            realEstate = gameInstance.getBuildingYard()[3];
                            break;
                        case GULDEN:
                            realEstate = gameInstance.getBuildingYard()[0];
                            break;
                        default:
                            System.out.println("please select Money");
                    }
                }
                if (handCardView.getSelected() != null) {
                    MoneyCard[] payMoney = new MoneyCard[handCardView.getSelected().size()];
                    for (int i = 0; i < handCardView.getSelected().size(); i++) {
                        payMoney[i] = handCardView.getSelected().get(i).getCard();
                    }
                    try {
                        mainLogicController.execute(new BuyReserveBuildingStrategy(gameInstance.getCurrentPlayer(), realEstate, gameInstance, payMoney));
                    } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                        System.out.println("Gebäude: " + realEstate.toString());
                        System.out.println("Geld: ");
                        for (MoneyCard mc : payMoney) {
                            System.out.println("" + mc.toString());
                        }
                        invalidGameTurnExecption.printStackTrace();
                    }
                }
            });


            this.setBackground(new Background(new BackgroundImage(new Image("/res/img/background/Background.png", starViewController.getWidth(), starViewController.getHeight(), false, false), BackgroundRepeat.ROUND,
                    BackgroundRepeat.ROUND,
                    BackgroundPosition.CENTER,
                    BackgroundSize.DEFAULT)));
            menuButton.setOnAction(actionEvent -> {
                FXUtil.setMenuButtonSound();
                showDialog(new MenuView(this, gameInstance));
            });
            updateRedoUndoButton(gameInstance);


        } catch (IOException e){
            e.printStackTrace();
        }

        destroy = new ProjectButton("Abbauen");
        destroy.setScaleX(0.75);
        destroy.setScaleY(0.75);
        butt1.getChildren().add(destroy);

        destroy.addEventFilter(MouseEvent.MOUSE_CLICKED, t -> {
            FXUtil.setMenuButtonSound();
            if (!destroypress) {
                destroyUpdate();
            } else {
                reInit();
            }

        });

        transition = new FadeTransition();
        transition.setFromValue(1);
        transition.setDelay(Duration.seconds(2));
        transition.setToValue(1);
        transition.setNode(this);
        transition.setDuration(Duration.seconds(2));
        transition.setOnFinished(event -> {
            performBotTurn();
        });
        transition.play();

    }

    private void updateRedoUndoButton(GameInstance gameInstance) {
        if (mainLogicController.getGameLogicController().getRedoAvailable()) {
            next.setStyle("-fx-min-width: 80;-fx-min-height: 40;-fx-max-width: 80;-fx-max-height: 40; -fx-text-fill: #493202 ;-fx-font-size: 14; -fx-font-weight: bold; -fx-font-family: 'Viner Hand ITC'; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/buttonMenu.png')");
            next.setOnAction(event -> {
                FXUtil.setMenuButtonSound();
                //mainLogicController.getGameLogicController().nextTurn();
                try {
                    mainLogicController.getGameLogicController().redoTurn();
                } catch (InvalidGameLogicException e) {
                    e.printStackTrace();
                } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                    invalidGameTurnExecption.printStackTrace();
                }
                reInit();
            });
        } else {
            next.setOnAction(event-> {});
            next.setStyle("-fx-min-width: 80;-fx-min-height: 40;-fx-max-width: 80;-fx-max-height: 40; -fx-text-fill: #493202 ;-fx-font-size: 14; -fx-font-weight: bold; -fx-font-family: 'Viner Hand ITC'; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/buttonMenuGrey.png')");
        }
        if (mainLogicController.getGameLogicController().getUndoAvailable()) {
            back.setStyle("-fx-min-width: 80;-fx-min-height: 40;-fx-max-width: 80;-fx-max-height: 40; -fx-text-fill: #493202 ;-fx-font-size: 14; -fx-font-weight: bold; -fx-font-family: 'Viner Hand ITC'; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/buttonMenu.png')");

            back.setOnAction(event -> {
                FXUtil.setMenuButtonSound();
                if(!mainLogicController.getGameInstance().getCurrentPlayer().getCheater()){
                    mainLogicController.getGameInstance().getPlayerList().stream().forEach(player -> {
                        player.setCheater(true);
                    });
                    showDialog(new WarningView(this, gameInstance, true));
                }
                else {
                    try {
                        mainLogicController.getGameLogicController().undoLastTurn();
                        gameInstance.trigger();
                    } catch (InvalidGameLogicException e) {
                        e.printStackTrace();
                    } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                        invalidGameTurnExecption.printStackTrace();
                    }

                    reInit();
                }
            });
        } else {
            back.setOnAction(event-> {});
            back.setStyle("-fx-min-width: 80;-fx-min-height: 40;-fx-max-width: 80;-fx-max-height: 40; -fx-text-fill: #493202 ;-fx-font-size: 14; -fx-font-weight: bold; -fx-font-family: 'Viner Hand ITC'; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/buttonMenuGrey.png')");
        }
    }

    private void performBotTurn() {
        if (!gameInstance.isFinish() && gameInstance.getCurrentPlayer() instanceof Bot) {
            Bot bot = (Bot) gameInstance.getCurrentPlayer();
            try {
                mainLogicController.execute(bot.getNextTurn());
            } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                invalidGameTurnExecption.printStackTrace();
            }

        }
    }

    private void selectCurrentPlayer() {
        playerViews = new PlayerView[gameInstance.getPlayerList().size()];
        for (int i = 0; i < playerViews.length; i++) {
            playerViews[i] = new PlayerView(gameInstance.getPlayerList().get(i), gameInstance.getPlayerList().indexOf(gameInstance.getCurrentPlayer()) == i);
            int current = i;
            playerViews[current].addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
                switchBoard(playerViews[current].getPlayer());
                for (int j = 0; j < playerViews.length; j++) {
                    playerViews[j].setShown(j == current);
                }
            });
        }
        players.getChildren().clear();
        players.getChildren().addAll(playerViews);
        players.setSpacing(2);
        players.setMaxHeight(800);
        this.state.clear();
    }

    private void switchBoard(Player temp) {
        alhambraPane.getChildren().clear();
        if (mainLogicController.getGameInstance().getCurrentPlayer() != temp) {
            otherBoardView = new OtherBoardView(mainLogicController, state, temp);
            alhambraPane.getChildren().add(otherBoardView);
            switchReserve(temp);
        } else {
            boardView = new BoardView(mainLogicController, state);
            alhambraPane.getChildren().add(boardView);
            updateReserve();
            if (handCards != null && !handCards.getChildren().isEmpty()) {
                handCardView = new HandCardView(gameInstance, this.state, this);
                handCards.getChildren().replaceAll(node -> handCardView);

                updateTop();
            }
        }
    }

    private void switchReserve(Player temp) {
        reserve.getChildren().clear();
        reserve.snapSpaceY(5);
        int num = temp.getReserveBuildings().size();
        int newheight = (int) (reserve.getHeight() / num);
        for (int i = 0; i < num; i++) {
            BuildingTile resTile = temp.getReserveBuildings().get(i);
            int flexheight = Math.min(100, newheight);
            BuildingTileView reserveBuilding = new BuildingTileView(resTile, 100, flexheight);
            reserve.getChildren().add(reserveBuilding);
            int norm = 100;
            reserveBuilding.setTranslateX(norm);
            reserveBuilding.setTranslateY(25);
        }
    }


    void reInit() {
        if (!stop){
            if (gameInstance.getEvaluationStrategy()!=null){
                stop=true;
                showDialog(new EvaluationView(this,gameInstance,gameInstance.getEvaluationStrategy().getEval(),gameInstance.getEvaluationStrategy()));
                gameInstance.setEvaluationStrategy(null);
            }
            else if (gameInstance.isLastRound()){
                boolean flag=false;
                for (Player player:gameInstance.getPlayerList()) {
                    if (player.getReserveBuildings().size()>0){
                        flag=true;
                        gameInstance.setCurrentPlayer(player);
                    }

                }
                if (!flag){
                    gameInstance.setEval(3);
                }

            }
            else if (handCards != null && !handCards.getChildren().isEmpty()) {
                handCardView = new HandCardView(gameInstance, this.state, this);
                handCards.getChildren().replaceAll(node -> handCardView);

                updateRedoUndoButton(gameInstance);
                updateTop();
                selectCurrentPlayer();
                destroypress = true;
                destroyUpdate();
                updateBoard();
                updateReserve();
                System.out.println(gameInstance.getPlayerList().indexOf(gameInstance.getCurrentPlayer()));
                nextRound();

            }
        }


    }

    private void nextRound() {
        if (gameInstance.isBusy()){
            gameInstance.setBusy(false);

            if (gameInstance.indexOfCurrentPlayer()+1<gameInstance.getPlayerList().size()){
                mainLogicController.apendTurn(new RefillStrategy(null,gameInstance));
                gameInstance.setCurrentPlayer(gameInstance.getPlayerList().get(gameInstance.indexOfCurrentPlayer()+1));
            }else {
                gameInstance.getRounds().add(new GameRound());
                gameInstance.setCurrentPlayer(gameInstance.getPlayerList().get(0));
                mainLogicController.apendTurn(new RefillStrategy(null,gameInstance));

            }
            hideRound.play();
        }else {
            if (!gameInstance.isLastRound())
                {
                    transition.play();
                }

        }
    }

    public void updateBoard() {
        alhambraPane.getChildren().clear();
        boardView = new BoardView(mainLogicController, state);
        alhambraPane.getChildren().add(boardView);
    }

    public void resume(){
        stop=false;
        reInit();
    }

    public void destroyUpdate() {
        if (!destroypress) {
            destroy.setStyle("-fx-min-width: 260;-fx-min-height: 90;-fx-max-width: 260;-fx-max-height: 90; -fx-text-fill: #bbbbbb ;-fx-font-size: 20;  -fx-font-family: 'Viner Hand ITC'; -fx-font-weight: bold; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/button.png')");
        } else {
            destroy.setStyle("-fx-min-width: 260;-fx-min-height: 90;-fx-max-width: 260;-fx-max-height: 90; -fx-text-fill: #493202 ;-fx-font-size: 20;  -fx-font-family: 'Viner Hand ITC'; -fx-font-weight: bold; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/button.png')");
        }
        destroypress = !destroypress;
        boardView.destroyBuilding(destroypress);
    }

    public void updateTop() {
        top.getChildren().clear();
        buildingYard = new BuildingYard(gameInstance, this);

        top.getChildren().addAll(new Bank(mainLogicController), buildingYard, new MoBiStack(mainLogicController));
        //System.out.println("update, update");
    }

    public void updateReserve() {
        reserve.getChildren().clear();
        reserve.snapSpaceY(5);
        int num = gameInstance.getCurrentPlayer().getReserveBuildings().size();
        int newheight = (int) (reserve.getHeight() / num);
        for (int i = 0; i < num; i++) {
            BuildingTile resTile = gameInstance.getCurrentPlayer().getReserveBuildings().get(i);
            int flexheight = Math.min(100, newheight);
            BuildingTileView reserveBuilding = new BuildingTileView(resTile, 100, flexheight);
            reserve.getChildren().add(reserveBuilding);
            int norm = 100;
            reserveBuilding.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
                if (reserveBuilding.getTranslateX() == norm) {
                    for (Node node : reserve.getChildren()) {
                        node.setTranslateX(norm);
                    }
                    reserveBuilding.setTranslateX(norm + 33);
                    updateBoard();
                    boardView.showPossibleReserveCandidates(resTile);
                    boardView.showPossibleSwapCandidates(resTile);
                } else {
                    reserveBuilding.setTranslateX(norm);
                    updateBoard();
                }
            });
            reserveBuilding.setTranslateX(norm);
            reserveBuilding.setTranslateY(25);
        }
    }

    public void showDialog(Node node) {
        dialog.setCenter(node);
        dialog.setVisible(true);
    }

    public void hideDialog() {
        dialog.setVisible(false);
    }

    public static class State {
        PropertyChangeSupport money = new PropertyChangeSupport(State.this);
        PropertyChangeSupport building = new PropertyChangeSupport(State.this);
        List<HandCard> selected;
        BuildingTile current;

        public State() {
            selected = new ArrayList<>();
            current = null;
        }

        public BuildingTile getCurrent() {
            return current;
        }

        public void setCurrent(BuildingTile current) {
            this.current = current;
            buildingTrigger();
        }

        public void setSelected(List<HandCard> sel) {
            if (sel == null) {
                this.clear();
            } else {
                selected = sel;
            }
            moneyTrigger();
        }

        public List<HandCard> getSelected() {
            return selected;
        }

        public void addBuildingObserver(PropertyChangeListener listener) {
            building.addPropertyChangeListener("theProperty", listener);
        }

        public void buildingTrigger() {
            building.firePropertyChange("theProperty", new Object(), State.this);

        }

        public void addMoneyObserver(PropertyChangeListener listener) {
            money.addPropertyChangeListener("theProperty", listener);
        }

        public void moneyTrigger() {
            money.firePropertyChange("theProperty", new Object(), State.this);

        }

        public void clear() {
            selected.clear();
        }
    }
}
