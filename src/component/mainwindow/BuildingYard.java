package component.mainwindow;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import model.GameInstance;
import model.board.BuildingTile;

class BuildingYard extends HBox {
    Yard cur = null;
    private Yard yellow;
    private Yard green;
    private Yard blue;
    private Yard orange;
    MainWindowView view;

    BuildingYard(GameInstance gameInstance, MainWindowView view) {
        BuildingTile[] yard = gameInstance.getBuildingYard();
        this.view = view;
        yellow = new Yard(yard[0], 0);
        green = new Yard(yard[1], 1);
        blue = new Yard(yard[2], 2);
        orange = new Yard(yard[3], 3);

        this.setPadding(new Insets(0, 0, 0, 5));
        this.setSpacing(5);
        this.setAlignment(Pos.TOP_CENTER);
        this.getChildren().addAll(yellow, green, blue, orange);

    }

    public Yard getYellow() {
        return yellow;
    }

    public Yard getGreen() {
        return green;
    }

    public Yard getBlue() {
        return blue;
    }

    public Yard getOrange() {
        return orange;
    }


    public void update(MainWindowView.State newValue) {
        yellow.unselect();
        blue.unselect();
        green.unselect();
        orange.unselect();
        //System.out.println("UpdateYard");
        if (newValue.getSelected() != null && newValue.getSelected().size() > 0) {
            int ges = 0;
            for (HandCard card : newValue.getSelected()) {
                ges += card.getCard().getValue();
            }

            switch (newValue.getSelected().get(0).getCard().getType()) {
                case GULDEN:
                    cur = yellow;
                    break;
                case DIRHAM:
                    cur = green;
                    break;
                case DENAR:
                    cur = blue;
                    break;
                case DUKATEN:
                    cur = orange;
                    break;
            }
            if (cur != null && cur.getTile()!=null && cur.getTile().getPrice() <= ges) {
                cur.select();
                newValue.setCurrent(cur.getTile());
            } else {

            }
        }
    }
}
