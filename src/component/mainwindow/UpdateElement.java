package component.mainwindow;

import model.GameInstance;

public interface UpdateElement {
    abstract void updateElement(GameInstance gameInstance);
}
