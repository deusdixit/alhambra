package component.mainwindow;

public interface ToggleElement {

    void onClick();
    void toggle() ;
}
