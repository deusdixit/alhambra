package component.mainwindow;

import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.board.BuildingTile;

public class BuildingTileView extends StackPane {

    private Image background;
    int width,height;

    public BuildingTileView(BuildingTile buildingTile){
        super();
        BorderPane pane = new BorderPane();
        this.width=200;
        this.height=200;
        VBox value = new VBox();
        this.getChildren().addAll(pane, value);
        this.setMinHeight(200);
        this.setMaxHeight(200);
        this.setMinWidth(200);
        this.setMaxWidth(200);
        if (buildingTile!=null){
            selectBaclground(buildingTile);
            this.setBackground(new Background(new BackgroundImage(background, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                    BackgroundSize.DEFAULT)));
            setWalls(pane, buildingTile);
            Text cardValue=new Text();
            if(0<buildingTile.getPrice()) {
                cardValue.setText("" + buildingTile.getPrice());
            }
            cardValue.setFont(Font.font("SansSerif", 30));

            value.getChildren().add(cardValue);
            value.setAlignment(Pos.BOTTOM_RIGHT);
            value.setTranslateX(-38);
            value.setTranslateY(-20);
        }

    }

    public BuildingTileView(BuildingTile tile, int width, int height) {
        super();
        this.width=width;
        this.height=height;
        BorderPane pane = new BorderPane();
        VBox value = new VBox();
        this.getChildren().addAll(pane, value);
        this.setMinHeight(height);
        this.setMaxHeight(width);
        this.setMinWidth(width);
        this.setMaxWidth(height);
        pane.setMaxWidth(width);
        pane.setMaxHeight(height);
        if (tile!=null){
            selectBaclground(tile);
            this.setBackground(new Background(new BackgroundImage(background, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                    BackgroundSize.DEFAULT)));
            setWalls(pane, tile);
            Text cardValue=new Text();
            cardValue.setText(""+tile.getPrice());
            cardValue.setFont(Font.font("SansSerif", 18));

            value.getChildren().add(cardValue);
            value.setAlignment(Pos.BOTTOM_RIGHT);
            value.setTranslateX(-20);
            value.setTranslateY(-8);
    }
    }

    private void selectBaclground(BuildingTile buildingTile) {
        switch (buildingTile.getType()) {
            case BRUNNEN:
                background = new Image("/res/img/building/brunnen.png", width, height, false, false);
                break;
            case PAVILLON:
                background = new Image("/res/img/building/pavilion.png", width, height, false, false);
                break;
            case SERAIL:
                background = new Image("/res/img/building/serail.png", width, height, false, false);
                break;
            case ARKADEN:
                background = new Image("/res/img/building/arkaden.png", width, height, false, false);
                break;
            case GEMAECHER:
                background = new Image("/res/img/building/gemeacher.png", width, height, false, false);
                break;
            case GARTEN:
                background = new Image("/res/img/building/garten.png", width, height, false, false);
                break;
            case TURM:
                background = new Image("/res/img/building/turm.png", width, height, false, false);
                break;
        }
    }

    private void setWalls(BorderPane pane, BuildingTile buildingTile){

        if(buildingTile.isWallTop()){
            topWall(pane);
        }
        if (buildingTile.isWallBottom()){
            bottomWall(pane);
        }
        if(buildingTile.isWallLeft()){
            leftWall(pane);
        }
        if(buildingTile.isWallRight()){
            rightWall(pane);
        }
    }

    private void topWall (BorderPane pane){
        Rectangle top=getVerticalRectangle();
        VBox vertical  = new VBox(top);
        vertical.setAlignment(Pos.TOP_CENTER);
        pane.setTop(vertical);
    }
    private void bottomWall (BorderPane pane){
        Rectangle bottom=getVerticalRectangle();
        VBox vertical  = new VBox(bottom);
        vertical.setAlignment(Pos.BOTTOM_CENTER);
        pane.setBottom(vertical);
    }
    private void rightWall (BorderPane pane){
        Rectangle right=getHorizontalRectangle();
        HBox horizontal  = new HBox(right);
        horizontal.setAlignment(Pos.CENTER);
        pane.setRight(horizontal);
    }
    private void leftWall (BorderPane pane){
        Rectangle left=getHorizontalRectangle();
        HBox horizontal  = new HBox(left);
        horizontal.setAlignment(Pos.CENTER);
        pane.setLeft(horizontal);
    }

    private Rectangle getVerticalRectangle(){
        Rectangle rectangle=new Rectangle(width-5, height*0.05);
        rectangle.setFill(Color.color(0.4,0.2,0.0));
        return  rectangle;
    }
    private Rectangle getHorizontalRectangle(){
        Rectangle rectangle=new Rectangle(width*0.05, height-5);
        rectangle.setFill(Color.color(0.4,0.2,0.0));
        return  rectangle;
    }
}
