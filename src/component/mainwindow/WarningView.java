package component.mainwindow;

import component.MenuButton;
import controller.GameLogicController;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import model.GameInstance;
import model.game.GameTurn;
import model.game.Player;
import util.FXUtil;
import util.exception.InvalidGameLogicException;
import util.exception.InvalidGameTurnExecption;

import static util.FXUtil.projectScale;

public class WarningView extends StackPane {

    WarningView(MainWindowView mainWindowView, GameInstance gameInstance){

        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        Text warning = new Text("Wenn Sie den Tipp nehmen, werden sie nicht in die Highscoreliste aufgenommen.");
        warning.setFont(Font.font("Viner Hand ITC", 22));
        Label label = new Label("Warnung");
        label.setFont(Font.font("Viner Hand ITC", 30));
        label.setAlignment(Pos.CENTER);
        label.setTextFill(Color.RED);
        MenuButton getHint = new MenuButton("Verstanden");
        MenuButton cancel = new MenuButton("Zurück");
        HBox buttonLine=new HBox(getHint, cancel);
        buttonLine.setAlignment(Pos.CENTER);
        buttonLine.setSpacing(10);
        VBox vBox=new VBox(label, warning, buttonLine);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(600);
        this.setMinWidth(600);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);
        getHint.setOnAction(event -> {
            FXUtil.setMenuButtonSound();

            mainWindowView.showDialog(new HintView(mainWindowView, GameLogicController.getHint(gameInstance)));
        });
        cancel.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
    }

    WarningView(MainWindowView mainWindowView, GameInstance gameInstance, boolean undo){
        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        Text warning = new Text("Wenn Sie den Zug rückgängig machen, \nwird keiner der Spieler in die Highscoreliste aufgenommen.");
        warning.setFont(Font.font("Viner Hand ITC", 22));
        warning.setTextAlignment(TextAlignment.CENTER);
        Label label = new Label("Warnung");
        label.setFont(Font.font("Viner Hand ITC", 30));
        label.setAlignment(Pos.CENTER);
        label.setTextFill(Color.RED);
        MenuButton undoButton = new MenuButton("Verstanden");
        MenuButton cancel = new MenuButton("Zurück");
        HBox buttonLine=new HBox(undoButton, cancel);
        buttonLine.setAlignment(Pos.CENTER);
        buttonLine.setSpacing(10);
        VBox vBox=new VBox(label, warning, buttonLine);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(15);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(600);
        this.setMinWidth(600);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);
        undoButton.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            gameInstance.getCurrentPlayer().setCheater(true);
            mainWindowView.hideDialog();
            try {
                mainWindowView.mainLogicController.getGameLogicController().undoLastTurn();
                gameInstance.trigger();
            } catch (InvalidGameLogicException e) {
                e.printStackTrace();
            } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                invalidGameTurnExecption.printStackTrace();
            }
        });
        cancel.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
    }


}
