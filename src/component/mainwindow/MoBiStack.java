package component.mainwindow;

import controller.MainLogicController;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class MoBiStack extends HBox {
    ImageView money,building;
    Text mon,bui;
        MoBiStack(MainLogicController mainLogicController){
            StackPane moneyPane=new StackPane();
            StackPane buildingPane=new StackPane();
            mon=new Text(""+mainLogicController.getGameInstance().getCardStack().size());
            mon.setFont(new Font(20));
            mon.setTranslateX(-5);
            bui=new Text(""+mainLogicController.getGameInstance().getBuildingStack().size());
            bui.setFont(new Font(20));
            bui.setTranslateX(-5);
            Rectangle circle01=new Rectangle(30,30);
            circle01.setFill(Color.WHITE);
            Rectangle circle02=new Rectangle(30,30);
            circle02.setFill(Color.WHITE);
            circle01.setStroke(Color.BLACK);
            circle02.setStroke(Color.BLACK);
            moneyPane.setAlignment(Pos.BOTTOM_RIGHT);
            buildingPane.setAlignment(Pos.BOTTOM_RIGHT);
            moneyPane.setMinWidth(220*0.45);
            moneyPane.setMinHeight(290*0.45);
            moneyPane.setMaxWidth(220*0.45);
            moneyPane.setMaxHeight(290*0.45);
            buildingPane.setMinWidth(130);
            buildingPane.setMinHeight(130);
            buildingPane.setMaxWidth(130);
            buildingPane.setMaxHeight(130);
            money=new ImageView(new Image("res/img/cards/cardBackground.png",220*0.45,290*0.45,false,false));
            building=new ImageView(new Image("res/img/building/background.png",130,130,false,false));
            moneyPane.getChildren().addAll(money,circle01,mon);
            buildingPane.getChildren().addAll(building,circle02,bui);
            this.getChildren().addAll(moneyPane,buildingPane);
        }
    }

