package component.mainwindow;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import model.GameInstance;
import model.ModelTypeUtil;
import model.board.MoneyCard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HandCardView extends HBox {
    List<MoneyCard> list;
    List<HandCard> selected;
    ModelTypeUtil.CardType type;
    MainWindowView.State state;
    MainWindowView mainWindowView;

    HandCardView(GameInstance gameInstance, MainWindowView.State state, MainWindowView mwv) {
        super();
        this.state = state;
        this.mainWindowView = mwv;
        this.list = gameInstance.getCurrentPlayer().getMoneyCards();
       // Collections.sort(list, Comparator.comparing(MoneyCard::getType));
        list.forEach(element -> {
            this.getChildren().add(new HandCard(element, this));
        });

    }

    public ModelTypeUtil.CardType getType() {
        return type;
    }

    public List<HandCard> getSelected() {
        return selected;
    }

    public void setType(ModelTypeUtil.CardType type) {
        this.type = type;
    }

    public void updateList() {
        ObservableList<Node> tmp = this.getChildren();
        selected = new ArrayList<>();
        for (Node node : tmp) {
            if (node instanceof HandCard && ((HandCard) node).selected) {
                selected.add((HandCard) node);

            }
        }
        if (selected.size() == 0) {
            type = null;
        }
        ////

        if (type != null) {
            int sum = 0;
            for (int i = 0; i < selected.size(); i++) {
                sum = sum + selected.get(i).getCard().getValue();
            }
            if (state.getCurrent() != null && state.getCurrent().getPrice() > sum) {
                mainWindowView.updateTop();
                mainWindowView.updateBoard();
            }
            state.setSelected(selected);
        } else {
            state.setSelected(null);
        }
        ////
    }
}
