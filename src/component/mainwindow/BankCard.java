package component.mainwindow;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import util.FXUtil;

import java.io.File;

public class BankCard extends Money {
    Bank bank;
    BankCard(model.board.MoneyCard card,Bank bank) {
        super(card);
        this.bank=bank;
    }

    @Override
   public void onClick() {
        toggle();
    }

    @Override
  public   void toggle() {
        if (!selected) {
            this.setTranslateY(20);
            number.setTextFill(Color.GREEN);

        } else {
            this.setTranslateY(0);
            number.setTextFill(Color.WHITE);

        }
        selected = !selected;
        FXUtil.setCardSound();
        bank.updateList();
    }
}
