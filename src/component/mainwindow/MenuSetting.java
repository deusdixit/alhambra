package component.mainwindow;

import component.MenuButton;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import util.FXUtil;

import static util.FXUtil.projectScale;

public class MenuSetting extends StackPane {
    Slider musicSlider;
    Slider soundSlider;

    MenuSetting(MainWindowView mainWindowView) {
        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        musicSlider = new Slider();
        soundSlider = new Slider();
        setSliderStyle(musicSlider);
        setSliderStyle(soundSlider);
        HBox musicLine = generateHBox(musicSlider, "Musik");
        HBox soundLine = generateHBox(soundSlider, "Sounds");
        MenuButton back = new MenuButton("Beenden");
        back.setTranslateY(10);
        VBox vBox = new VBox(musicLine, soundLine, back);
        vBox.setAlignment(Pos.CENTER);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        back.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(600);
        this.setMinWidth(600);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);
        setButtonFunctions();
    }

    private void setSliderStyle(Slider slider) {
        slider.setMin(0);
        slider.setMax(1);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(0.05);
        slider.setMinorTickCount(0);
        slider.setBlockIncrement(1);
        slider.setSnapToTicks(true);
        slider.setPrefHeight(70);
        slider.setPrefWidth(200);
    }

    private HBox generateHBox(Slider slider, String name) {
        HBox box = new HBox();
        Label label = new Label(name);
        label.setFont(Font.font("Viner Hand ITC", 26));
        box.getChildren().addAll(label, slider);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);
        return box;
    }

    private void setButtonFunctions() {
        musicSlider.setValue(FXUtil.getMusicVolume());
        soundSlider.setValue(FXUtil.getSoundVolume());
        musicSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            musicSlider.setValue(newValue.doubleValue());
            FXUtil.getMusicPlayer().setVolume(newValue.doubleValue());
            FXUtil.setMusicVolume(newValue.doubleValue());
        });
        soundSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            soundSlider.setValue(newValue.doubleValue());
            FXUtil.getSoundPlayer().setVolume(newValue.doubleValue());
            new FXUtil().setSoundVolume(newValue.doubleValue());
        });
    }
}
