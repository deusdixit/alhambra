package component.mainwindow;

import component.MenuButton;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.game.GameTurn;
import util.ConfigUtil;
import util.FXUtil;
import util.exception.InvalidGameTurnExecption;

import static util.FXUtil.projectScale;

public class HintView extends StackPane {
    HintView(MainWindowView mainWindowView, GameTurn turn) {
        System.err.println(turn.toString());
        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        Text hint = new Text(FXUtil.stringFormater(turn.printTurn()));
        hint.setFont(Font.font("Viner Hand ITC", 22));
        Label label = new Label("Tipp");
        label.setFont(Font.font("Viner Hand ITC", 30));
        label.setAlignment(Pos.CENTER);
        MenuButton execute = new MenuButton("Ausführen");
        execute.setOnAction(action->{
            try {
                ConfigUtil.save();
                mainWindowView.mainLogicController.execute(turn);
                mainWindowView.hideDialog();
            } catch (InvalidGameTurnExecption invalidGameTurnExecption) {
                invalidGameTurnExecption.printStackTrace();
            }
        });
        MenuButton back = new MenuButton("Zurück");
        HBox buttonLine=new HBox(execute, back);
        buttonLine.setAlignment(Pos.CENTER);
        buttonLine.setSpacing(10);
        VBox vBox = new VBox(label, hint, buttonLine);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(600);
        this.setMinWidth(600);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);
        back.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
    }
}
