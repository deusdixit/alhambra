package component.mainwindow;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import model.GameInstance;
import model.game.Player;
import model.board.*;
import javafx.scene.text.*;
import util.GameInstanceUtil;

import java.io.IOException;

public class PlayerView extends BorderPane {
    Player player;
    boolean isCurrent;
    @FXML
    Text gulden, dirham, dukaten, denar, pavillon, serail, arkaden, gemaecher, garten, turm, name, score;
    @FXML
    ImageView dukatenImg, denarImg, dirhamImg, guldenImg;
    String border;

    /**
     * Create a new PlayerStats FXML for the referred Player.
     *
     */
    PlayerView(Player player, boolean isCurrent){
        this.player = player;
        this.isCurrent = isCurrent;
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/layout/PlayerStat.fxml"));
        loader.setController(this);
        border = "-fx-border-color:black; -fx-border-width:3;";
        selectPlayercolor();

        try {
            BorderPane pane=loader.load();
                this.setTop(pane.getTop());
                this.setLeft(pane.getLeft());
                this.setRight(pane.getRight());

            dukatenImg.setImage(new Image("res/img/stamp/Dukaten.png", 15,15,false,false));
            denarImg.setImage(new Image("res/img/stamp/Denar.png", 15,15,false,false));
            dirhamImg.setImage(new Image("res/img/stamp/Dirham.png", 15,15,false,false));
            guldenImg.setImage(new Image("res/img/stamp/Gulden.png", 15,15,false,false));

            int int1 = 10;
            int int2 = 100;
            if(player.getScore()< int1){score.setText("00"+ player.getScore());}
            if(int1 <=player.getScore() && player.getScore()< int2){score.setText("0"+ player.getScore());}
            if(int2 <=player.getScore()){score.setText(""+ player.getScore());}
            name.setText(player.getName());
            setMoneyCount();
            setBuildingCount();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public Player getPlayer(){
        return this.player;
    }

    private void selectPlayercolor(){
        if(isCurrent){
            this.setTranslateX(-20);
            switch (player.getColor()){
                case RED: this.setStyle("-fx-background-color: rgba(180, 0, 0, 0.7); " +border);  break;
                case BLUE: this.setStyle("-fx-background-color: rgba(100, 100, 255, 0.7);"+border);  break;
                case GREEN: this.setStyle("-fx-background-color: rgba(0, 180, 0, 0.7);"+border);  break;
                case ORANGE: this.setStyle("-fx-background-color: rgba(255, 155, 0, 0.7);"+border);  break;
                case WHITE: this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7);"+border);  break;
                case YELLOW: this.setStyle("-fx-background-color: rgba(255, 255, 0, 0.7);"+border);  break;
                default: this.setStyle("-fx-background-color: rgba(100, 100, 100, 0.7);"+border);
            }
        }
        else{
            this.setTranslateX(0);
            switch (player.getColor()){
                case RED: this.setStyle("-fx-background-color: rgba(180, 0, 0, 0.7);");  break;
                case BLUE: this.setStyle("-fx-background-color: rgba(100, 100, 255, 0.7);");  break;
                case GREEN: this.setStyle("-fx-background-color: rgba(0, 180, 0, 0.7);");  break;
                case ORANGE: this.setStyle("-fx-background-color: rgba(255, 155, 0, 0.7);");  break;
                case WHITE: this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7);");  break;
                case YELLOW: this.setStyle("-fx-background-color: rgba(255, 255, 0, 0.7);");  break;
                default: this.setStyle("-fx-background-color: rgba(100, 100, 100, 0.7);");
            }
        }
    }

    private void setMoneyCount(){
        for(MoneyCard mcard : player.getMoneyCards()){
            switch (mcard.getType()) {
                case GULDEN: if(gulden.getText().equals("0")){gulden.setText("" + mcard.getValue());
                }else{gulden.setText(gulden.getText() + "+" + mcard.getValue());}  break;

                case DIRHAM: if(dirham.getText().equals("0")){dirham.setText("" + mcard.getValue());
                }else{dirham.setText(dirham.getText() + "+" + mcard.getValue());}  break;

                case DENAR: if(denar.getText().equals("0")){denar.setText("" + mcard.getValue());
                }else{denar.setText(denar.getText() + "+" + mcard.getValue());}  break;

                case DUKATEN: if(dukaten.getText().equals("0")){dukaten.setText("" + mcard.getValue());
                }else{dukaten.setText(dukaten.getText() + "+" + mcard.getValue());}  break;

                default: System.out.println("unexpected CardType on Hand");
            }
        }
    }

    private void setBuildingCount(){
        if(player.getGameBoard().getBuildings() != null) {
            for (int i = 0; i < player.getGameBoard().getBuildings().length; i++) {
                switch (i) {
                    case 0:
                        pavillon.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    case 1:
                        serail.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    case 2:
                        arkaden.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    case 3:
                        gemaecher.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    case 4:
                        garten.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    case 5:
                        turm.setText("" + player.getGameBoard().getBuildings()[i]); break;
                    default: System.out.println("zu weit");
                }
            }
        }
        int pavillonNum=0, serailNum=0, arkadenNum=0, gemaecherNum=0, gartenNum=0, turmNum=0;
        for(BuildingTile btile: player.getReserveBuildings() ){
            switch (btile.getType()){
                case PAVILLON: pavillonNum++;  break;
                case SERAIL: serailNum++;  break;
                case ARKADEN: arkadenNum++;  break;
                case GEMAECHER: gemaecherNum++;  break;
                case GARTEN: gartenNum++;  break;
                case TURM: turmNum++;  break;
                default: throw new IllegalArgumentException("Brunnen in der Reserve");
            }
        }
        if(pavillonNum!=0){pavillon.setText(pavillon.getText() +"(" + pavillonNum +")");}
        if(serailNum!=0){serail.setText(serail.getText() +"(" + serailNum +")");}
        if(arkadenNum!=0){arkaden.setText(arkaden.getText() +"(" + arkadenNum +")");}
        if(gemaecherNum!=0){gemaecher.setText(gemaecher.getText() +"(" + gemaecherNum +")");}
        if(gartenNum!=0){garten.setText(garten.getText() +"(" + gartenNum +")");}
        if(turmNum!=0){turm.setText(turm.getText() +"(" + turmNum +")");}
    }

    public void setShown(boolean shown){
        if(isCurrent){
            switch (player.getColor()){
                case RED: this.setStyle("-fx-background-color: rgba(180, 0, 0, 0.7); " +border);  break;
                case BLUE: this.setStyle("-fx-background-color: rgba(0, 140, 255, 0.7);"+border);  break;
                case GREEN: this.setStyle("-fx-background-color: rgba(0, 210, 0, 0.7);"+border);  break;
                case ORANGE: this.setStyle("-fx-background-color: rgba(255, 155, 0, 0.7);"+border);  break;
                case WHITE: this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7);"+border);  break;
                case YELLOW: this.setStyle("-fx-background-color: rgba(255, 255, 0, 0.7);"+border);  break;
                default: this.setStyle("-fx-background-color: rgba(100, 100, 100, 0.7);"+border);
            }
        }
        else if(shown){
            String borderShown = "-fx-border-color:grey; -fx-border-width:3;";
            switch (player.getColor()){
                case RED: this.setStyle("-fx-background-color: rgba(180, 0, 0, 0.7); " +borderShown);  break;
                case BLUE: this.setStyle("-fx-background-color: rgba(0, 140, 255, 0.7);"+borderShown);  break;
                case GREEN: this.setStyle("-fx-background-color: rgba(0, 210, 0, 0.7);"+borderShown);  break;
                case ORANGE: this.setStyle("-fx-background-color: rgba(255, 155, 0, 0.7);"+borderShown);  break;
                case WHITE: this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7);"+borderShown);  break;
                case YELLOW: this.setStyle("-fx-background-color: rgba(255, 255, 0, 0.7);"+borderShown);  break;
                default: this.setStyle("-fx-background-color: rgba(100, 100, 100, 0.7);"+borderShown);
            }
        }
        else {
            switch (player.getColor()){
                case RED: this.setStyle("-fx-background-color: rgba(180, 0, 0, 0.7);");  break;
                case BLUE: this.setStyle("-fx-background-color: rgba(0, 140, 255, 0.7);");  break;
                case GREEN: this.setStyle("-fx-background-color: rgba(0, 210, 0, 0.7);");  break;
                case ORANGE: this.setStyle("-fx-background-color: rgba(255, 155, 0, 0.7);");  break;
                case WHITE: this.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7);");  break;
                case YELLOW: this.setStyle("-fx-background-color: rgba(255, 255, 0, 0.7);");  break;
                default: this.setStyle("-fx-background-color: rgba(100, 100, 100, 0.7);");
            }
        }
    }
}
