package component.mainwindow;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.IOException;

public  abstract class Money extends Pane  implements ToggleElement{

    @FXML
    Label number;
    @FXML
    ImageView image;
    model.board.MoneyCard card;
    boolean selected;
    Image background;

    Money(model.board.MoneyCard card) {
        super();

        Tooltip tooltip=new Tooltip(card.getValue()+" "+card.getType());
        Tooltip.install(this,tooltip);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/layout/GameCard.fxml"));

        this.card = card;
        selected = false;

        loader.setController(this);
        this.setStyle("-fx-border-color: white");
        try {

            this.getChildren().add(loader.load());
            this.addEventHandler(MouseEvent.MOUSE_CLICKED,event->{
                this.onClick();
            });
            number.setText("" + card.getValue());

            getBackround(card);
            this.setBackground(new Background(new BackgroundImage(background,
                    BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                    BackgroundSize.DEFAULT)));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getBackround(model.board.MoneyCard card) {
        genBackground(card);
    }

    //for the bank


    private void genBackground(model.board.MoneyCard card) {
        switch (card.getType()) {
            case DENAR:
                background = new Image("/res/img/cards/blue.png", 100, 135, false, false);
                break;
            case DIRHAM:
                background = new Image("/res/img/cards/green.png", 100, 135, false, false);
                break;
            case DUKATEN:
                background = new Image("/res/img/cards/orange.png", 100, 135, false, false);
                break;
            case GULDEN:
                background = new Image("/res/img/cards/yellow.png", 100, 135, false, false);
                break;
        }
    }


    public model.board.MoneyCard getCard() {
        return card;
    }

    public boolean isSelected() {
        return selected;
    }
}
