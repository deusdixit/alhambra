package component.mainwindow;

import component.MenuButton;
import controller.IOLogicController;
import javafx.geometry.Pos;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.GameInstance;

import util.ConfigUtil;
import util.FXUtil;

import java.io.IOException;

import static util.FXUtil.projectScale;

public class SaveGameDialog extends StackPane {

    SaveGameDialog(MainWindowView mainWindowView, GameInstance gameInstance)  {
        ImageView background = new ImageView();
        background.setImage(new Image("/res/img/banner/windowBanner.png"));
        background.setScaleX(1.5);
        background.setScaleY(1.5);
        ConfigUtil.Configuration configuration=ConfigUtil.load();

        ConfigUtil.SaveGame game=new ConfigUtil.SaveGame(gameInstance,configuration.getCurrentSave(),"saves/"+configuration.getCurrentSave()+".save");
        configuration.increaseSaves();
        Text warning;
        try {
            IOLogicController.save(game);
            warning= new Text("Spiel gespeichert.");
        }catch (IOException e){
            warning= new Text("Spiel konnte nicht gespeichert werden.");
        }


        warning.setFont(Font.font("Viner Hand ITC", 22));

        MenuButton okButton = new MenuButton("OK");
        HBox buttonLine=new HBox( okButton);
        buttonLine.setAlignment(Pos.CENTER);
        buttonLine.setSpacing(10);
        VBox vBox=new VBox( warning, buttonLine);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);
        this.getChildren().addAll(background, vBox);
        this.setAlignment(Pos.CENTER);
        this.setMaxHeight(400);
        this.setMinHeight(400);
        this.setMaxWidth(600);
        this.setMinWidth(600);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(2);
        this.setEffect(innerShadow);

        okButton.setOnAction(event -> {
            FXUtil.setMenuButtonSound();
            mainWindowView.hideDialog();
        });
    }



}
