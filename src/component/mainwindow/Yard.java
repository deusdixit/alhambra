package component.mainwindow;

import javafx.animation.Animation;
import javafx.animation.ScaleTransition;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.CycleMethod;
import javafx.util.Duration;
import model.board.BuildingTile;

public class Yard extends StackPane implements ToggleElement{

        private BuildingTile tile;
        private BuildingTileView tileView;
        private boolean isBuyable;
        private static final String SELECTED ="-fx-border-color: red;-fx-border-width: 3;-fx-padding:10,10,10,10;";
        private static final String UNSELECTED ="-fx-border-color: transparent;-fx-padding: 15;-fx-border-width: 3";
        private static final double HEIGHT =284*0.40;
        private static final double WIDTH=443*0.40;
        ScaleTransition scaleTransition;

    Yard(BuildingTile tile,int pos){
        scaleTransition=new ScaleTransition();
        this.tile=tile;
        this.tileView=new BuildingTileView(this.tile,(int)100,(int)100);
        scaleTransition.setDuration(Duration.seconds(3));
        scaleTransition.setAutoReverse(true);
        scaleTransition.setFromX(1);
        scaleTransition.setFromY(1);
        scaleTransition.setToX(1.15);
        scaleTransition.setToY(1.15);
        scaleTransition.setNode(this);
        this.setStyle(UNSELECTED);

        isBuyable=false;
        ImageView view = new ImageView();
        HBox box=new HBox();
        this.getChildren().addAll(view,box);
        this.setMinHeight(HEIGHT);
        this.setMaxHeight(HEIGHT);
        this.setMinWidth(WIDTH);
        this.setMaxWidth(WIDTH);

        this.tileView.setTranslateX(-10);
        String path;
        Image background;
        switch (pos){
            case 0:path="/res/img/yard/yellow.png";break;
            case 1:path="/res/img/yard/green.png";break;
            case 2:path="/res/img/yard/blue.png";break;
            default:
            case 3:path="/res/img/yard/orange.png";
                break;
        }
         background=new Image(path,WIDTH,HEIGHT,false,false);
        view.setImage( background);
        box.getChildren().addAll(this.tileView);
        box.setAlignment(Pos.TOP_LEFT);
    }
    public void toggle(){
        if (!isBuyable){
            unselect();
        }else {
            select();
        }
        isBuyable=!isBuyable;
    }

    public void unselect() {

        if (isBuyable){
            isBuyable=false;
            scaleTransition.pause();
            scaleTransition.setFromX(1.15);
            scaleTransition.setFromY(1.15);
            scaleTransition.setToX(1);
            scaleTransition.setToY(1);
            scaleTransition.setCycleCount(0);
            scaleTransition.setAutoReverse(false);
            this.setStyle(UNSELECTED);
            scaleTransition.play();
        }

    }

    public void select() {
        isBuyable=true;
        this.setStyle(SELECTED);
        scaleTransition.setFromX(1);
        scaleTransition.setFromY(1);
        scaleTransition.setToX(1.15);
        scaleTransition.setToY(1.15);
        scaleTransition.setAutoReverse(true);
        scaleTransition.setCycleCount(Animation.INDEFINITE);
        scaleTransition.play();
    }

    public void onClick(){
            toggle();

    }

    public BuildingTile getTile() {
        return tile;
    }
}

