package component;

import javafx.scene.control.Button;

public class MenuButton extends Button {

    public MenuButton() {
        super();
        init();
    }

    public MenuButton(String text) {
        super(text);
        init();

    }

    private void init (){
        this.setStyle(
                "-fx-min-width: "+130+";-fx-min-height: "+45+";-fx-max-width: "+130+";-fx-max-height: "+45+"; -fx-text-fill: #493202 ;-fx-font-size: 20;  -fx-font-family: 'Viner Hand ITC'; -fx-font-weight: bold; -fx-background-color: transparent; -fx-padding: 0,0,10,0;-fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/buttonSmallMenu.png')");
    }

}
