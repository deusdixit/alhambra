package component;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import util.ConfigUtil;
import util.FXUtil;
import view.StarViewController;
import java.util.LinkedList;
import java.util.stream.Collectors;
import static util.FXUtil.projectScale;

public class HiScore extends BorderPane {
    int fontSize = 25;
    int titleFontSize = 30;


    public HiScore(StarViewController starViewController) {
        Label title = new Label("Highscore");
        title.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, 56));
        title.setTextFill(Color.BLACK);
        VBox titleBox = new VBox(title);
        titleBox.setAlignment(Pos.CENTER);
        this.setTop(titleBox);

        VBox nameBox = new VBox();
        VBox resultBox = new VBox();
        VBox scoreBox = new VBox();
        HBox content = new HBox(nameBox, scoreBox, resultBox);
        content.setAlignment(Pos.TOP_CENTER);
        content.setTranslateY(20);
        content.setTranslateX(20);
        content.setSpacing(20);
        content.setMaxSize(500, 600);
        content.setMinSize(500, 600);

        ImageView banner = new ImageView();
        banner.setImage(new Image("/res/img/banner/windowBanner.png", 600, 500, false, false));
        banner.setRotate(90);
        StackPane stackPane = new StackPane(banner, content);
        stackPane.setAlignment(Pos.CENTER);
        stackPane.setMaxSize(500, 600);
        stackPane.setMinSize(500, 600);
        this.setCenter(stackPane);

        ProjectButton back = new ProjectButton("Zurück");
        back.setTranslateY(30);
        VBox buttonBox = new VBox(back);
        buttonBox.setTranslateY(-20);
        buttonBox.setAlignment(Pos.CENTER);
        this.setBottom(buttonBox);

        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(5);

        back.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            starViewController.init(false);
        });
        LinkedList<ConfigUtil.HighScore> highscoreList = ConfigUtil.load().getHighScores();
        String outputName = highscoreList.stream().map(ConfigUtil.HighScore::getName).collect(Collectors.joining("\n"));
        String outputScore = highscoreList.stream().map(highScore -> String.valueOf(highScore.getScore())).collect(Collectors.joining("\n"));
        String outputResult = highscoreList.stream().map(highScore -> String.valueOf(highScore.getResult())).collect(Collectors.joining("\n"));
        Text names = new Text(outputName);
        Text scores = new Text(outputScore);
        Text results = new Text(outputResult);

        names.setFont(Font.font("Viner Hand ITC", fontSize));
        scores.setFont(Font.font("Viner Hand ITC", fontSize));
        results.setFont(Font.font("Viner Hand ITC", fontSize));
        Text nameTitle = new Text("Name");
        Text scoreTitle = new Text("Punkte");
        Text resultTitle = new Text("Wertung");
        nameTitle.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, titleFontSize));
        scoreTitle.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, titleFontSize));
        resultTitle.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, titleFontSize));
        nameBox.getChildren().addAll(nameTitle, names);
        scoreBox.getChildren().addAll(scoreTitle, scores);
        resultBox.getChildren().addAll(resultTitle, results);
        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
    }
}
