package component;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import util.FXUtil;
import view.StarViewController;
import static util.FXUtil.projectScale;

public class Settings extends BorderPane {

    Slider musicSlider;
    Slider soundSlider;

    public Settings(StarViewController starViewController) {
        ImageView banner = new ImageView();
        banner.setImage(new Image("/res/img/banner/windowBanner.png"));
        musicSlider = new Slider();
        soundSlider = new Slider();
        setSliderStyle(musicSlider);
        setSliderStyle(soundSlider);
        HBox musicLine = generateHBox(musicSlider, "Musik");
        HBox soundLine = generateHBox(soundSlider, "Sounds");
        ProjectButton back = new ProjectButton("Zurück");
        back.setTranslateY(10);
        VBox contentBox = new VBox(musicLine, soundLine, back);
        StackPane stackPane = new StackPane(banner, contentBox);
        contentBox.setAlignment(Pos.CENTER);
        contentBox.setSpacing(5);
        stackPane.setAlignment(Pos.CENTER);
        this.setCenter(stackPane);
        Label title = new Label("Optionen");
        title.setFont(Font.font("Viner Hand ITC", FontWeight.BOLD, 56));
        title.setTextFill(Color.BLACK);
        VBox titleBox = new VBox(title);
        titleBox.setAlignment(Pos.CENTER);
        this.setTop(titleBox);
        back.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            starViewController.init(false);
        });

        this.setScaleX(projectScale());
        this.setScaleY(projectScale());
        setButtonFunctions();
    }


    private void setSliderStyle(Slider slider) {
        slider.setMin(0);
        slider.setMax(1);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(0.05);
        slider.setMinorTickCount(0);
        slider.setBlockIncrement(1);
        slider.setSnapToTicks(true);
        slider.setPrefHeight(70);
        slider.setPrefWidth(200);
    }

    private HBox generateHBox(Slider slider, String name) {
        HBox box = new HBox();
        Label label = new Label(name);
        label.setFont(Font.font("Viner Hand ITC", 26));
        box.getChildren().addAll(label, slider);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(10);
        return box;
    }

    private void setButtonFunctions() {
        musicSlider.setValue(FXUtil.getMusicVolume());
        soundSlider.setValue(FXUtil.getSoundVolume());
        musicSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            musicSlider.setValue(newValue.doubleValue());
            FXUtil.getMusicPlayer().setVolume(newValue.doubleValue());
            FXUtil.setMusicVolume(newValue.doubleValue());
        });
        soundSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            soundSlider.setValue(newValue.doubleValue());
            FXUtil.getSoundPlayer().setVolume(newValue.doubleValue());
            new FXUtil().setSoundVolume(newValue.doubleValue());
        });
    }
}