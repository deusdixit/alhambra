package component;

import javafx.scene.control.Button;

public class ProjectButton extends Button {

    public ProjectButton() {
        super();
        init();
    }

    public ProjectButton(String text) {
        super(text);
        init();

    }

    private void init() {
        this.setStyle(
                "-fx-min-width: 260;-fx-min-height: 90;-fx-max-width: 260;-fx-max-height: 90; -fx-text-fill: #493202 ;-fx-font-size: 20;  -fx-font-family: 'Viner Hand ITC'; -fx-font-weight: bold; -fx-background-color: transparent; -fx-border-color: transparent;-fx-border-width: 0;-fx-background-image: url('/res/img/button/button.png')");
    }

}
