package component;
import component.mainwindow.MainWindowView;
import controller.IOLogicController;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import util.ConfigUtil;
import util.FXUtil;
import view.StarViewController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class LoadGame extends StackPane {
    @FXML
    Button next, back;

    private Integer start=0;
    private HBox rowOne,rowTwo;
    private   ArrayList <VBox> saveList;
    List<ConfigUtil.SaveGame> list;
    BorderPane loadingPane;
    public LoadGame(StarViewController starViewController)  {


        loadingPane= new BorderPane();
        this.getChildren().addAll(loadingPane);
        this.setAlignment(Pos.CENTER);

        try {
          list=IOLogicController.loadGame();
        } catch (IOException | ClassNotFoundException e) {
            list = new LinkedList<>();
        }
        Collections.sort(list);

         rowOne=new HBox();
         rowTwo=new HBox();
        VBox rows= new VBox(rowOne,rowTwo);
        rows.setAlignment(Pos.CENTER);
        rows.setSpacing(10);
        rowOne.setSpacing(10);
        rowTwo.setSpacing(10);
        rowOne.setAlignment(Pos.CENTER);
        rowTwo.setAlignment(Pos.CENTER);

         saveList= new ArrayList<>();
        int size =150;
        VBox tmp;
        for (ConfigUtil.SaveGame saveGame:list){
            tmp=new VBox(new ImageView(new Image("/res/img/building/background.png",size,size,false,false)));
            tmp.setStyle("-fx-border-color: black;-fx-background-color: rgba(255,213,154,0.8)");
            tmp.setAlignment(Pos.CENTER);
            tmp.setMinWidth(300);
            tmp.setMinHeight(300);
            tmp.getChildren().addAll(new Text(saveGame.getId()+"\n"+saveGame.getGameInstance().getGameCreated()));
            tmp.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                StackPane betterOverlay=new StackPane();
                BorderPane dialog=new BorderPane();
                dialog.setStyle("-fx-background-color: rgba(0,0,0,0.64)");
                dialog.setVisible(false);
                BorderPane logPane=new BorderPane();
                logPane.setStyle("-fx-background-color: rgba(0,0,0,0.50)");
                logPane.setVisible(false);
                logPane.setCenter(new Text("Sperrbildschirm"));

                betterOverlay.getChildren().addAll(new MainWindowView(starViewController, saveGame.getGameInstance(),dialog, logPane),logPane,dialog);

                starViewController.setCenter(betterOverlay);

            });
            saveList.add(tmp);
        }

            rerenderView();


        loadingPane.setCenter(rows);
        ProjectButton back = new ProjectButton("Zurück");
        back.setOnAction(action->{
            if (start-4>=0){
                start-=4;
                rerenderView();
            }
        });


        ProjectButton next = new ProjectButton("Vor");
        next.setOnAction(action->{
            if (start+1<list.size()){
                start+=4;
                rerenderView();

            }
        });

        HBox buttonLine = new HBox(back, next);
        loadingPane.setBottom(buttonLine);
        buttonLine.setSpacing(10);
        buttonLine.setAlignment(Pos.CENTER);

        Button backButton = new ProjectBackButton();
        backButton.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            starViewController.init(false);
        });

        ImageView cardView = new ImageView();
        cardView.setImage(new Image("/res/img/building/background.png"));

        HBox hBox = new HBox(backButton);
        hBox.setAlignment(Pos.TOP_LEFT);
        StackPane stackPane = new StackPane();
        loadingPane.setTop(stackPane);
        stackPane.getChildren().addAll(new WindowBanner("Spiel laden"), hBox);
    }
    private void rerenderView(){
        rowOne.getChildren().clear();
        rowTwo.getChildren().clear();
        if (start<saveList.size()){ rowOne.getChildren().add(saveList.get(start)); }else {
            rowOne.getChildren().add(new WindowBanner("Keine Spielstände vorhanden"));
        }
        if (start+1<saveList.size()){ rowOne.getChildren().add(saveList.get(start+1)); }
        if (start+2<saveList.size()){ rowTwo.getChildren().add(saveList.get(start+2)); }
        if (start+3<saveList.size()){ rowTwo.getChildren().add(saveList.get(start+3)); }

    }
}
