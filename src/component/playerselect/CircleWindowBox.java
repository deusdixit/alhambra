package component.playerselect;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import model.ModelTypeUtil;

public class CircleWindowBox extends CircleBox {

    StackPane stackPane;
    ModelTypeUtil.PlayerColor color;

    public CircleWindowBox(ModelTypeUtil.PlayerColor color) {
        super();
        this.color = color;
        stackPane = new StackPane();
        Color rcolor;
        String auswahl;
        switch (this.color) {
            case YELLOW:
                auswahl = "Gelb";
                rcolor = Color.YELLOW;
                break;
            case BLUE:
                auswahl = "Blau";
                rcolor = Color.BLUE;
                break;
            case RED:
                auswahl = "Rot";
                rcolor = Color.RED;
                break;
            case WHITE:
                auswahl = "Weiss";
                rcolor = Color.WHITE;
                break;
            case ORANGE:
                auswahl = "Orange";
                rcolor = Color.ORANGE;
                break;
            case GREEN:
            default:
                auswahl = "Gruen";
                rcolor = Color.GREEN;
                break;
        }
        circle.setFill(rcolor);
        this.setText(auswahl);
        stackPane.getChildren().addAll(circle);

        this.setLeft(stackPane);
        this.setRight(text);
    }

    public ModelTypeUtil.PlayerColor getColor() {
        return color;
    }
}
