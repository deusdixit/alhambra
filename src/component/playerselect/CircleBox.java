package component.playerselect;

import javafx.scene.effect.BlurType;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public abstract class CircleBox extends BorderPane {
    Text text;
    Circle circle;

    CircleBox() {
        this.setStyle("-fx-border-color: black;-fx-border-width: 1;-fx-cursor: pointer;-fx-padding: 5;-fx-background-color: rgba(0,0,0,0.1) ;-fx-min-width: 200;-fx-max-width: 200");
        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setColor(Color.BLACK);
        innerShadow.setWidth(4);
        innerShadow.setBlurType(BlurType.ONE_PASS_BOX);
        this.setEffect(innerShadow);
        text = new Text();
        text.setStyle("-fx-font-size: 18pt");
        circle = new Circle(25);
        circle.setFill(null);
        circle.setStroke(Color.BLACK);
        circle.setStrokeWidth(2);
    }

    public void setText(String text) {
        this.text.setText(text);
    }
}
