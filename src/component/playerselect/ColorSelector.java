package component.playerselect;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import model.ModelTypeUtil;

import java.util.LinkedList;


public class ColorSelector extends Pane {
    CircleWindowBox current;
    HBox hBox;
    LinkedList<ModelTypeUtil.PlayerColor> list;

    ColorSelector(LinkedList<ModelTypeUtil.PlayerColor> list, BorderPane dialog) {
        hBox = new HBox(new CircleImageWindowBox("Zufall", new Image("/res/img/icons/dice.png", 25, 25, false, false)));
        this.list=list;
        this.getChildren().add(hBox);
        current = null;
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            dialog.getChildren().clear();

            VBox colors = new VBox();
            CircleImageWindowBox random = new CircleImageWindowBox("Zufall", new Image("/res/img/icons/dice.png", 25, 25, false, false));
            random.addEventHandler(MouseEvent.MOUSE_CLICKED, eventC -> {
                onClick(list, dialog, random);
            });
            colors.getChildren().add(random);
            for (ModelTypeUtil.PlayerColor color : list) {
                CircleWindowBox circleWindowBox = new CircleWindowBox(color);
                circleWindowBox.addEventHandler(MouseEvent.MOUSE_CLICKED, eventC -> {
                    onSelect(list, dialog, color, circleWindowBox);
                });
                colors.getChildren().add(circleWindowBox);
            }
            colors.setSpacing(5);
            colors.setAlignment(Pos.CENTER);
            dialog.setTop(new Text("Farbe auswaehlen"));
            dialog.setCenter(colors);
            dialog.setVisible(true);
        });
    }

    private void onSelect(LinkedList<ModelTypeUtil.PlayerColor> list, BorderPane dialog, ModelTypeUtil.PlayerColor color, CircleWindowBox circleWindowBox) {
        if (current != null) {
            list.add(current.getColor());
        }
        current = circleWindowBox;
        hBox.getChildren().clear();
        hBox.getChildren().add(current);
        list.remove(color);
        dialog.setVisible(false);
    }

    private void onClick(LinkedList<ModelTypeUtil.PlayerColor> list, BorderPane dialog, CircleImageWindowBox random) {
        if (current != null) {
            list.add(current.getColor());
        }
        current = null;
        hBox.getChildren().clear();
        hBox.getChildren().add(random);
        dialog.setVisible(false);
    }


    ModelTypeUtil.PlayerColor getPlayerColor(){
        return current==null ? list.pop():current.color;
    }

}
