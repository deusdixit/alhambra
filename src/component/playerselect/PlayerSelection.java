package component.playerselect;

import component.ProjectBackButton;
import component.WindowBanner;
import component.mainwindow.MainWindowView;
import controller.BoardLogicControllerUtil;
import controller.IOLogicController;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.GameInstance;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.Bot;
import model.game.Player;
import org.apache.commons.lang3.StringUtils;
import util.FXUtil;
import util.exception.NoMoneyException;
import view.StarViewController;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Stack;

import static util.FXUtil.*;

public class PlayerSelection extends StackPane {
    @FXML
    Button startButton;
    @FXML
    Slider slider;
    @FXML
    VBox rowContainer;
    @FXML
    CheckBox checkBox;

    LinkedList<PlayerColor> remaining;
    StackPane stackPane;
    StarViewController starViewController;
    BorderPane content;
    public PlayerSelection(StarViewController starViewController) {
        this.starViewController = starViewController;
        stackPane = new StackPane();
        content= new BorderPane();
        initRemaining();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/res/layout/PlayerSelection.fxml"));
        loader.setController(this);
        Pane loadingPane;
        try {
            loadingPane = loader.load();
            InnerShadow innerShadow = new InnerShadow();
            innerShadow.setColor(Color.BLACK);
            innerShadow.setWidth(50);
            loadingPane.setEffect(innerShadow);
            loadingPane.setScaleX(projectScale());
            loadingPane.setScaleY(projectScale());
            BorderPane dialog = new BorderPane();
            dialog.setMinWidth(900);
            dialog.setMinHeight(500);
            dialog.setMaxWidth(900);
            dialog.setMaxHeight(500);
            dialog.setStyle("-fx-background-color: rgba(232,210,161,0.9)");
            dialog.setEffect(innerShadow);
            dialog.setVisible(false);
            stackPane.getChildren().addAll(loadingPane, dialog);
            this.getChildren().add(content);
            content.setCenter(stackPane);
            setPlayerSlider(dialog);
            rowContainer.getChildren().add(new PlayerRow(remaining, dialog, 1));
            rowContainer.getChildren().add(new PlayerRow(remaining, dialog, 2));
            setButtonFunctions();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setBackButton();
    }

    private class PlayerRow extends HBox {
        LinkedList<PlayerColor> remaining;
        PlayerColor selected;
        TextField nameField;
        int playerType;
        ColorSelector selectColor;
        TypeSelector selectType;

        PlayerRow(LinkedList<PlayerColor> list, BorderPane dialog, int playerIndex) {
            super();
            this.remaining = list;
            this.setStyle("-fx-padding: 5;-fx-background-color: rgba(0,0,0,0.1)");
            selected = null;
            playerType = -1;
            nameField = new TextField("Player"+playerIndex);
//            alphabeticListener(nameField);
            selectColor = new ColorSelector(list, dialog);
            selectType = new TypeSelector(dialog);
            this.setSpacing(5);
            VBox name = new VBox(new Text("Name:"), nameField);
            alphabeticAndNumericListener(nameField);
            name.setSpacing(5);
            this.getChildren().addAll(selectColor, selectType, name);
        }

        public Player generatePlayer() {
            switch (selectType.getType()) {
                case 0:
                    return new Player(StringUtils.left(nameField.getText().trim(), 10), selectColor.getPlayerColor());
                default:
                    return new Bot("Bot_"+StringUtils.left(nameField.getText().trim(),10), selectColor.getPlayerColor(),selectType.getType());
            }
        }

        public boolean isValid() {
            return playerType >= 0 && selected != null;
        }
    }

    private void setPlayerSlider(BorderPane dialog) {
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            rowContainer.getChildren().clear();
            initRemaining();
            for (int i = 0; i < newValue.intValue(); i++) {
                rowContainer.getChildren().add(new PlayerRow(remaining, dialog, i+1));
            }
        });
    }

    private void initRemaining() {
        remaining = new LinkedList<>();
        remaining.add(PlayerColor.BLUE);
        remaining.add(PlayerColor.ORANGE);
        remaining.add(PlayerColor.YELLOW);
        remaining.add(PlayerColor.GREEN);
        remaining.add(PlayerColor.RED);
        remaining.add(PlayerColor.WHITE);
    }

    private void setButtonFunctions() {
        startButton.setOnAction(action -> {
            System.out.println("action");
            FXUtil.setMenuButtonSound();
            ObservableList<Node> list = rowContainer.getChildren();
            Player[] players = new Player[list.size()];
            for (int i = 0; i < players.length; i++) {
                if (list.get(i) instanceof PlayerRow) {
                    players[i] = ((PlayerRow) list.get(i)).generatePlayer();
                }
            }
            GameInstance gameInstance = new GameInstance();
            try {
                if (!checkBox.isSelected()){
                    BoardLogicControllerUtil.initBoard(gameInstance, players);
                }else {
		    File buildingFile = new File("gebaeude.csv");
		    File moneyFile = new File("geld.csv");
		    if (buildingFile.exists() && moneyFile.exists()) {
			Stack<BuildingTile> buildings = IOLogicController.readBuildingCSV(buildingFile);
			Stack<MoneyCard> money = IOLogicController.readMoneyCSV(moneyFile);
			BoardLogicControllerUtil.initBoard(gameInstance,buildings,money,players);
			gameInstance.setTournament(true);
		    } else {
			starViewController.setCenter(new WindowBanner("Konnte geld.csv und gebaeude.csv\nnicht im root Verzeichnis finden."));
			throw new Exception("Can't find csv files");
		    }
                }

                StackPane betterOverlay=new StackPane();
                BorderPane logPane=new BorderPane();
                logPane.setStyle("-fx-background-color: rgba(0,0,0,0.50)");
                logPane.setVisible(false);
                logPane.setCenter(new Text("Sperrbildschirm"));

                BorderPane dialog=new BorderPane();
                dialog.setStyle("-fx-background-color: rgba(0,0,0,0.64)");
                dialog.setVisible(false);
                betterOverlay.getChildren().addAll(new MainWindowView(starViewController, gameInstance,dialog,logPane),logPane,dialog);

                starViewController.setCenter(betterOverlay);
            } catch (NoMoneyException | Exception e) {
                e.printStackTrace();

            }
        });

    }

    private void setBackButton() {
        Button backButton = new ProjectBackButton();
        backButton.setOnAction(action -> {
            FXUtil.setMenuButtonSound();
            starViewController.init(false);
        });
        HBox hBox = new HBox(backButton);
        hBox.setAlignment(Pos.TOP_LEFT);
        StackPane stackPane = new StackPane();
        content.setTop(stackPane);
        stackPane.getChildren().addAll(new WindowBanner("Lokales Spiel erstellen"), hBox);
    }
}
