package component.playerselect;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class CircleImageWindowBox extends CircleBox {


    StackPane stackPane;

    CircleImageWindowBox(String auswahl, Image image) {
        super();
        ImageView imageView = new ImageView(image);
        stackPane = new StackPane();
        stackPane.getChildren().addAll(imageView, this.circle);
        this.setText(auswahl);
        this.setLeft(stackPane);
        this.setRight(text);
    }

    public String getText(){
        return this.getText();
    }

}
