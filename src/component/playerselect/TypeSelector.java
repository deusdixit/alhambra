package component.playerselect;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class TypeSelector extends Pane {
    CircleImageWindowBox current;
    HBox hBox;

    TypeSelector(BorderPane dialog) {
        current = new CircleImageWindowBox("Mensch", new Image("/res/img/icons/user.png", 25, 25, false, false));
        hBox = new HBox(current);
        this.getChildren().add(hBox);

        this.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            dialog.getChildren().clear();

            VBox types = new VBox();
            CircleImageWindowBox person = new CircleImageWindowBox("Mensch", new Image("/res/img/icons/user.png", 25, 25, false, false));
            CircleImageWindowBox computer1 = new CircleImageWindowBox("Leicht", new Image("/res/img/icons/robot.png", 30, 30, false, false));
            CircleImageWindowBox computer2 = new CircleImageWindowBox("Mittel", new Image("/res/img/icons/robot.png", 30, 30, false, false));
            CircleImageWindowBox computer3 = new CircleImageWindowBox("Schwer", new Image("/res/img/icons/robot.png", 30, 30, false, false));
            selfSelect(person, dialog);
            selfSelect(computer1, dialog);
            selfSelect(computer2, dialog);
            selfSelect(computer3, dialog);

            types.getChildren().add(person);
            types.getChildren().add(computer1);
            types.getChildren().add(computer2);
            types.getChildren().add(computer3);

            types.setSpacing(5);
            types.setAlignment(Pos.CENTER);


            dialog.setTop(new Text("Typ auswaehlen"));

            dialog.setCenter(types);


            dialog.setVisible(true);
        });
    }

    public int getType(){
        switch (current.text.getText()){
            case "Mensch":
                return 0;
            case "Leicht":
                return 1;
            case "Mittel":
                return 2;
            case "Schwer":
                return 3;
            default:
                return -1;
        }
    }

    private void selfSelect(CircleImageWindowBox box, BorderPane dialog) {
        box.addEventHandler(MouseEvent.MOUSE_CLICKED, event1 -> {
            dialog.setVisible(false);
            current = box;
            hBox.getChildren().clear();
            hBox.getChildren().add(current);
        });

    }
}
