# Willkommen bei der Alhambra Hilfe
![alt text](img/cover.png "Cover1")

![Generic badge](https://img.shields.io/badge/Language-Java-lightgrey.svg)
![Generic badge](https://img.shields.io/badge/Version-1.0-orange.svg)
![Generic badge](https://img.shields.io/badge/Gruppe-01-blueviolet.svg)

> Alhambra, der Klassiker unter den modernen Brettspielen schlechthin liegt Ihnen hier als Videospiel vor. Das renommierte Entwicklerstudio Gruppe_1 des Sopras 20a setzt hier wie gewohnt neue Maßstäbe!


## Spielmaterial
- 6 Startplättchen
- 54 Gebäudeplättchen (7x Serail, 7x Pavillon, 9x Arkaden, 9x Gemächer, 11x Garten, 11x Turm), 1 Bauhof, 1 Zähltafel für Siegpunkte, 12 Zählsteine (2 je Spieler*in)
- 108 Geldkarten in 4 Währungen (jeweils 3 im Wert von 1-9), 2 Wertungskarten, 6 Reservefelder

## Spielvorbereitung
Die Spieler*innen erhalten je ein Reservefeld, ein Startplättchen und zwei Zählsteine. Ein Zählstein wird direkt auf das eigene Startplättchen gelegt, der andere auf das Feld 0 der Zähltafel. Die Geldkarten (ohne Wertungskarten) werden gemischt und zu einem verdeckten Nachziehstapel in die Tischmitte gelegt. Die erste Spieler*in zieht nun so lange Geldkarten vom Nachziehstapel, bis die Geldsumme 20 oder mehr beträgt. Es folgt die nächste Spieler*in, usw. Anschließend werden vier Geldkarten offen in die Tischmitte gelegt. Die erste Wertungskarte wird unter das zweite Fünftel des Nachziehstapels gemischt, die zweite Karte unter das vierte Fünftel. Der Bauhof und die Zähltafel werden in die Tischmitte gelegt. Die 54 Gebäudeplättchen werden verdeckt gemischt und vier Gebäudeplättchen in der vorgegebenen Reihenfolge auf den Bauhof gelegt. Ziel des Spiels ist es, jeweils zum Zeitpunkt der insgesamt drei Wertungen die meisten Gebäudeplättchen einer Sorte in der eigenen Alhambra verbaut zu haben.

## Bedienung

- Mit W A S D kann die Alhambra verschoben werden.

- Mit dem Mausrad kann in die Alhambra raus und rein gezoomt werden

- Klicke auf ein Spielerkärtchen an der rechten Seite um die Alhambra und Reserve des
 jeweiligen Spielers zu sehen.

- Klicke auf Geldkarten in der Bank (oben links) um Geld zu nehmen. Bei 2 oder mehr Karten
 maximal bis zu 5 Wertigkeit. Es wird automatisch Geld genohmen wenn man mit keiner
 weiteren Geldkarte unter 5 bleibt.

- Klicke auf Geldkarten in der Hand (mitte unten) um Gebäude zu kaufen. Wenn genug Geld
 ausgewählt wurde wird das Gebäude im Bauhof (mitte oben) gehighlightet und
 Anbaumöglichkeiten leuchten auf. Klicke auf eine der leuchtenden Felder um das Gebäude
 anzubauen oder in die Reserve um es dorthin zu legen.

- Klicke auf den “Abbauen”-Knopf um eines der gehighlighteten Gebäude abzureißen und
 in der Reserve zu platzieren.

- Klicke auf ein Gebäude in der Reserve um es an eines der gehighlighteten Felder
 anzubauen (grün) oder auszutauschen (blau).

- Klicke auf den “Menü”-Knopf oder Esc um das Menü aufzurufen.


## Spielablauf
Startspieler*in ist, wer bei der Geldverteilung am wenigsten Karten erhalten hat. Die Startspieler *in beginnt, die anderen folgen im Uhrzeigersinn. Es muss jeweils einer der folgenden Spielzüge ausgeführt werden:

- Geld nehmen
- Gebäudeplättchen kaufen und platzieren
- Eigene Alhambra umbauen

> Nachdem der Spielzug beendet ist, werden die ausliegenden Geldkarten und die Gebäudeplättchen wieder auf vier Stück ergänzt.

1. Geld nehmen: Man darf eine beliebige Geldkarte aus der Auslage nehmen oder aber mehrere Geldkarten gleichzeitig, sofern deren Summe maximal fünf beträgt.
2. Gebäudeplättchen kaufen und platzieren: Beim Kaufen nimmt man ein Gebäudeplättchen vom Bauhof und zahlt mindestens den aufgedruckten Preis in der auf dem Bauhof angezeigten Währung. Das bezahlte Geld bildet einen Ablagestapel neben dem Bauhof, es wird kein Wechselgeld ausbezahlt. Wenn man den Preis genau passend bezahlen kann, ist man noch einmal an der Reihe und kann sich erneut zwischen den drei möglichen Spielzügen entscheiden. Entweder wird das neu erworbene Plättchen direkt angebaut oder auf das Reservefeld gelegt. Beim Anbauen von Gebäudeteilen gibt es folgende Regeln zu beachten: alle Gebäudeplättchen müssen mit der Beschriftung nach unten angelegt werden. Es kann nur an bereits ausliegende Gebäudeteile angebaut werden. Es dürfen nur gleichartige Seiten aneinandergrenzen, entweder beide Seiten mit oder ohne Stadtmauer. Das neue Plättchen muss vom Startplättchen aus erreichbar sein, ohne dass die Plättchen verlassen werden müssen oder, dass eine Stadtmauer überquert werden muss. Jedes Plättchen muss mit mindestens einer Seite an die Alhambra angrenzen. Es dürfen keine vollständig umbauten leere Flächen entstehen.
3. Eigene Alhambra umbauen Zum Umbau der Alhambra stehen drei Möglichkeiten unter Beachtung der oben genannten Bauregeln zur Verfügung. Achtung: Das Startplättchen darf niemals abgebaut oder ausgetauscht werden.
   - Ein Gebäudeteil vom Reservefeld an der Alhambra anbauen
   - Ein Gebäudeteil der Alhambra abbauen und auf das Reservefeld legen
   - Ein Gebäudeteil aus dem Reservefeld mit einem der Teil aus der Alhambra austauschen – dabei muss das neue Gebäudeteil exakt die freigewordene Position einnehmen.

## Wertungen
Im Spiel gibt es insgesamt 3 Wertungen. Die ersten beiden Wertungen finden dann statt, wenn die entsprechende Karte vom Stapel mit den Geldkarten gezogen wird. Die dritte und letzte Wertung findet am Spielende statt. Für jeden Gebäudetyp werden einzeln Punkte an die Spieler*innen, die die meisten dieser Gebäude in ihrer Alhambra verbaut haben, vergeben. Gebäude im Reservefeld werden bei allen Wertungen nicht berücksichtigt. Bei der 2. Zwischenwertung werden auch Punkte für die zweitmeisten Gebäude eines Typs vergeben. Die jeweiligen zu vergebenen Punktezahlen sind den Wertungskarten zu entnehmen. Besitzen mehrere Spieler*innen gleich viele Gebäude eines Typs werden die Punkte geteilt und dabei abgerundet. Bei der dritten Wertung zum Abschluss des Spiels werden auch für die drittmeisten Gebäudeanzahlen eines Typs Punkte vergeben. Auch für die Außenmauern gibt es Siegpunkte. Die Spieler*innen erhalten jeweils für ihr längstes zusammenhängendes Stück Außenmauer pro Mauerstück einen Punkt. Doppelt liegende Mauern (also Innenmauern) bringen keine Punkte.

## Spielende
Das Spiel ist beendet, sobald nach dem Zug eines Spielers die Gebäudeplättchen nicht mehr komplett nachgelegt werden können, weil diese aufgebraucht sind. Die restlichen Gebäudeplättchen vom Bauhof werden noch an die Spieler*innen vergeben, die in der jeweiligen Währung das meiste Geld auf der Hand haben (der Preis der Gebäude spielt jetzt keine Rolle mehr). Bei Gleichstand bleibt das Gebäudeteil auf dem Bauhof. Diese so erstandenen Gebäudeteile dürfen noch gemäß den Bauregeln verbaut werden. Anschließend folgt die dritte und letzte Wertung (s.o.). Wer am Ende insgesamt die meisten Punkte hat, hat das Spiel gewonnen.

en benötigten Informationen werden im Doku-Wiki bekannt gegeben. Wenn der Geldstapel verbraucht ist, wird im Originalspiel der Ablagestapel gemischt und dient danach als neuer Geldstapel. Im KI-Spiel wird der Stapel nicht gemischt, vielmehr wird der Stapel direkt wieder verwendet.




## Anforderungen an das Programm
- Das Programm steuert den Spielablauf und sorgt für die Einhaltung der Spielregeln. Das Spiel soll über beliebig viele Partien gespielt werden können. Die Reihenfolge der Spieler*innen soll vor Spielstart frei wählbar sein.

- Das Programm soll bis zu 6 Spieler*innen das Spielen von Alhambra ermöglichen. Es sollen simulierte Mitspieler*innen mit min. drei unterschiedlichen Spielstärken zur Verfügung gestellt werden.

- Das Spiel soll unterbrochen und gespeichert werden können, um es an einem späteren Zeitpunkt fortzusetzen.

- Realisieren Sie eine Highscore-Liste. Überlegen Sie sich selbst geeignete Kriterien zur Bewertung der Leistung der Spieler*innen.

- Spieler*innen können sich einen qualifizierten Tipp geben lassen. Wird ein Tipp in Anspruch genommen, werden die Spieler*innen entsprechend nicht mehr in der Highscore-Liste berücksichtigt.

- Um verschiedene Strategien studieren und ausprobieren zu können, soll das Spiel über eine Undo- und eine Redo-Funktion verfügen. Die Spielzüge sollen bis zum Spielstart zurückgenommen werden können. Spiele, in denen die Undo-Funktion verwendet wurde, werden nicht mehr in die Highscore-Liste aufgenommen.

- Am Ende des Projekts soll ein KI-Turnier stattfinden. Dazu ist es notwendig, dass die Reihenfolge der Mitspieler*innen in beiden beteiligten Spielprogrammen identisch ist. Auch die ausgelegten Karten müssen identisch sein. Außerdem müssen die Stapel, von denen während des Spiels gezogen wird, in gleicher Weise sortiert sein. Das wird erreicht, indem die Karten der Nachziehstapel für Geld und Gebäudeteile aus einer Datei eingelesen werden. Die für das Karteneinlesen benötigten Informationen werden im Doku-Wiki bekannt gegeben. Wenn der Geldstapel verbraucht ist, wird im Originalspiel der Ablagestapel gemischt und dient danach als neuer Geldstapel. Im KI-Spiel wird der Stapel nicht gemischt, vielmehr wird der Stapel direkt wieder verwendet.

## Über Gruppe 1
### Hier einige fiktive Aussagen:

#### Ein Meisterwerk aus dem Indie-Bereich! 

> Louis Radtke, Software- und KI-Entwickler bei Gruppe_1 

#### Joa, könnte cool sein.
 
> Dr. Doris Schmedding

#### Die beste Erfindung, seit  geschnittenem Brot. 
 
 > Anonym

#### Muss man erstmal machen können.
 
 > Ut🐮

### Betreuer*in
- Doris Schmedding  
### Mitglieder
 
- Jin Ke 
- Oskar Kleine 
- David Nagy 
- Niels Gasper 
- Louis Radtke 
- Niklas Untiet 
- Rezeda Mirgalieva 
- Utku Pazarci 
