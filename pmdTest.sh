#!/bin/bash

alias pmd="$HOME/pmd-bin-6.22.0/bin/run.sh pmd"


number=$(cat pdm.txt | wc -l  )
result=${number//[!0-9]/}

echo "PMD start"
echo "$result"
echo "PMD end"

if [ $result -gt 0 ]
    then
        if [ $result -gt 20 ]
        then
            wget "https://img.shields.io/badge/PMD-$result Fehler-red" -O pmd.svg
        else
            wget "https://img.shields.io/badge/PMD-$result Fehler-yellow" -O pmd.svg
        fi

    else
        wget "https://img.shields.io/badge/PMD-Perfect-green" -O pmd.svg
fi

echo "Result"
cat pdm.txt

exit 0;