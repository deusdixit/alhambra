package util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.ModelTypeUtil.BuildingType;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;

public class GameLogicUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		BuildingTile[][] board = new BuildingTile[20][20];
		board[10][10] = new BuildingTile();
		board[11][10] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(true, false, false, false));
		int value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 1);
		board[10][9] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, true));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 2);

		board[12][10] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(true, false, true, true));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 5);

		board[9][10] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(true, true, true, false));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 5);

		board[9][9] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(true, true, true, false));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 5);

		board[9][11] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, true, true, false));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 5);

		board[10][11] = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, true, false));
		value = GameLogicUtil.getLargestOuterWallLength(board);
		assertEquals(value, 6);
	}

}
