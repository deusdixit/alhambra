package controller;

import static org.junit.Assert.*;

import java.util.Stack;

import org.junit.Before;
import org.junit.Test;

import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.CardType;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.board.BuildingTile.Wall;
import java.io.File;

public class IOLogicControllerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testReadCSV() {
		File geld = new File("tests/controller/res/geld.csv");
		File building = new File("tests/controller/res/gebaeude.csv");
		IOLogicController controller = new IOLogicController(null);
		Stack<MoneyCard> moneyStack = controller.readMoneyCSV(geld);
		Stack<BuildingTile> buildingStack = controller.readBuildingCSV(building);
		Stack<MoneyCard> moneyStack2 = new Stack<>();
		Stack<BuildingTile> buildingStack2 = new Stack<>();
		assertTrue(buildingStack.size() == 12);
		assertTrue(moneyStack.size() == 10);
		

		BuildingTile p1 = new BuildingTile(2, BuildingType.PAVILLON, new Wall(true, true, false, true));
		BuildingTile p2 = new BuildingTile(3, BuildingType.PAVILLON, new Wall(false, true, true, false));
		BuildingTile p3 = new BuildingTile(3, BuildingType.SERAIL, new Wall(false, true, true, true));
		BuildingTile p4 = new BuildingTile(4, BuildingType.SERAIL, new Wall(true, false, false, true));
		BuildingTile p5 = new BuildingTile(5, BuildingType.ARKADEN, new Wall(true, true, false, false));
		BuildingTile p6 = new BuildingTile(6, BuildingType.ARKADEN, new Wall(true, false, false, true));
		BuildingTile p7 = new BuildingTile(6, BuildingType.GEMAECHER, new Wall(false, false, true, true));
		BuildingTile p8 = new BuildingTile(7, BuildingType.GEMAECHER, new Wall(true, false, false, true));
		BuildingTile p9 = new BuildingTile(7, BuildingType.GARTEN, new Wall(true, true, true, false));
		BuildingTile p10 = new BuildingTile(8, BuildingType.GARTEN, new Wall(true, false, false, true));
		BuildingTile p11 = new BuildingTile(9, BuildingType.TURM, new Wall(true, false, false, true));
		BuildingTile p12 = new BuildingTile(11, BuildingType.TURM, new Wall(false, false, false, false));
		
		MoneyCard m1 = new MoneyCard(1, CardType.WERTUNG);
		MoneyCard m2 = new MoneyCard(2, CardType.WERTUNG);
		MoneyCard m3 = new MoneyCard(1, CardType.DENAR);
		MoneyCard m4 = new MoneyCard(4, CardType.DENAR);
		MoneyCard m5 = new MoneyCard(1, CardType.DIRHAM);
		MoneyCard m6 = new MoneyCard(2, CardType.DIRHAM);
		MoneyCard m7 = new MoneyCard(1, CardType.DUKATEN);
		MoneyCard m8 = new MoneyCard(2, CardType.DUKATEN);
		MoneyCard m9 = new MoneyCard(2, CardType.GULDEN);
		MoneyCard m10 = new MoneyCard(9, CardType.GULDEN);

		buildingStack2.add(p1);
		buildingStack2.add(p2);
		buildingStack2.add(p3);
		buildingStack2.add(p4);
		buildingStack2.add(p5);
		buildingStack2.add(p6);
		buildingStack2.add(p7);
		buildingStack2.add(p8);
		buildingStack2.add(p9);
		buildingStack2.add(p10);
		buildingStack2.add(p11);
		buildingStack2.add(p12);

		moneyStack2.add(m1);
		moneyStack2.add(m2);
		moneyStack2.add(m3);
		moneyStack2.add(m4);
		moneyStack2.add(m5);
		moneyStack2.add(m6);
		moneyStack2.add(m7);
		moneyStack2.add(m8);
		moneyStack2.add(m9);
		moneyStack2.add(m10);

		for (int i = 0; i < moneyStack2.size(); i++) {
			assertEquals(moneyStack2.get(moneyStack2.size()-1-i), moneyStack.get(i));
		}

		for (int i = 0; i < buildingStack2.size(); i++) {
			assertEquals(buildingStack2.get(buildingStack2.size()-1-i), buildingStack.get(i));
		}

	}

}
