package model.strategy;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.board.MoneyCard;
import model.game.GameTurn;
import model.game.Player;
import org.junit.Test;
import util.exception.NoMoneyException;

import static org.junit.Assert.*;
import static util.GameInstanceUtil.checkSize;

public class RobBankStrategyTest {

    @Test
    public void robBankStrategyTest() throws NoMoneyException {
        GameInstance gameInstance;
        gameInstance = new GameInstance();
        BoardLogicControllerUtil.initBoard(gameInstance);

        new RefillStrategy(null, gameInstance).doTurn();

        Player player = gameInstance.getCurrentPlayer();
        GameTurn robBankStrategy = new RobBankStrategy(gameInstance, player,
                gameInstance.getBank()[0]);


        int playerMoneySize = player.getMoneyCards().size();
        MoneyCard[] oldBank = gameInstance.getBank();

        assertTrue(robBankStrategy.validate());

        robBankStrategy.doTurn();

        assertEquals(playerMoneySize+1, player.getMoneyCards().size());
        assertEquals(3,checkSize(gameInstance.getBank()));

        robBankStrategy.unDoTurn();

        assertEquals(playerMoneySize, player.getMoneyCards().size());
        assertSame(4, checkSize(gameInstance.getBank()));

    }


}
