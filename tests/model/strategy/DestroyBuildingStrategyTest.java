package model.strategy;

import model.BoardPosition;
import org.junit.Test;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;
import model.game.Player;

import static org.junit.Assert.*;
import util.exception.NoMoneyException;

public class DestroyBuildingStrategyTest {

	@Test
	public void destroyBuildingStrategyTest() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player current = new Player("Test", PlayerColor.BLUE);
		BoardLogicControllerUtil.initBoard(gameInstance, current);
		BuildingTile bTile = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, true, false, false));
		current.getReserveBuildings().add(bTile);
		AddFromReserveStrategy turn = new AddFromReserveStrategy(current, bTile, gameInstance, new BoardPosition(10, 11));
		turn.doTurn();
		DestroyBuildingStrategy dTurn = new DestroyBuildingStrategy(current, gameInstance, new BoardPosition(10, 11));
		assertEquals(bTile,current.getGameBoard().getBoard()[10][11]);
		dTurn.doTurn();
		assertEquals(null,current.getGameBoard().getBoard()[10][11]);
		dTurn.unDoTurn();
		assertEquals(bTile,current.getGameBoard().getBoard()[10][11]);
	}

}
