package model.strategy;

import static org.junit.Assert.*;
import static util.GameInstanceUtil.*;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.game.GameTurn;
import org.junit.Test;
import util.exception.NoMoneyException;

public class RefillStrategyTest {

    @Test
    public void refillStrategyTest() throws NoMoneyException {
        GameInstance gameInstance;
        gameInstance = new GameInstance();
        BoardLogicControllerUtil.initBoard(gameInstance);

        GameTurn turn= new RefillStrategy(null,gameInstance);
        MoneyCard[] bank = gameInstance.getBank().clone();
        BuildingTile[] buildingTiles = gameInstance.getBuildingYard().clone();
        int buildingStackSize=gameInstance.getBuildingStack().size();
        int moneyStackSize=gameInstance.getCardStack().size();

        turn.doTurn();

        assertEquals(4,checkSize(gameInstance.getBank()));
        assertEquals(4,checkSize(gameInstance.getBuildingYard()));

        assertEquals(buildingStackSize-4,gameInstance.getBuildingStack().size());
        assertEquals(moneyStackSize-4,gameInstance.getCardStack().size());

        turn.unDoTurn();

        assertArrayEquals(bank,gameInstance.getBank());
        assertArrayEquals(buildingTiles,gameInstance.getBuildingYard());

        assertEquals(buildingStackSize,gameInstance.getBuildingStack().size());
        assertEquals(moneyStackSize,gameInstance.getCardStack().size());



    }



}
