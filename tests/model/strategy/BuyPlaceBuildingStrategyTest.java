package model.strategy;

import model.BoardPosition;
import org.junit.Test;
import util.exception.NoMoneyException;
import static org.junit.Assert.*;
import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.CardType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.MoneyCard;
import model.board.BuildingTile.Wall;
import model.game.Player;

public class BuyPlaceBuildingStrategyTest {

	@Test
	public void buyPlaceBuildingStrategyTest() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player current = new Player("Test", PlayerColor.BLUE);
		BoardLogicControllerUtil.initBoard(gameInstance, current);
		BuildingTile bTile = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, true, false, false));
		MoneyCard m1 = new MoneyCard(5, CardType.DENAR);
		MoneyCard m2 = new MoneyCard(5, CardType.DENAR);
		int cardCount = current.getMoneyCards().size();
		current.getMoneyCards().add(m1);
		current.getMoneyCards().add(m2);
		gameInstance.getBuildingYard()[0] = bTile;
		BuyPlaceBuildingStrategy turn = new BuyPlaceBuildingStrategy( bTile, gameInstance, new BoardPosition(10, 11),m1,m2);
		turn.doTurn();
		assertEquals(bTile, current.getGameBoard().getBoard()[10][11]);
		assertEquals(cardCount, current.getMoneyCards().size());
		assertEquals(null, gameInstance.getBuildingYard()[0]);
		turn.unDoTurn();
		assertEquals(null, current.getGameBoard().getBoard()[10][11]);
		assertEquals(cardCount+2, current.getMoneyCards().size());
		assertEquals(bTile, gameInstance.getBuildingYard()[0]);
	}

}
