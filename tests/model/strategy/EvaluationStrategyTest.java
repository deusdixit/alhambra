package model.strategy;

import org.junit.Test;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;
import model.game.Player;

import static org.junit.Assert.*;
import util.exception.NoMoneyException;

public class EvaluationStrategyTest {

	@Test
	public void evaluationStrategyTest() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player p1 = new Player("Niels", PlayerColor.BLUE);
		Player p2 = new Player("Jörn", PlayerColor.RED);
		Player p3 = new Player("Paul", PlayerColor.YELLOW);
		
		BoardLogicControllerUtil.initBoard(gameInstance, p1,p2,p3);

		BuildingTile bTile1 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile2 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile3 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile4 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile5 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile16 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));

		BuildingTile bTile6 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile7 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile8 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile9 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile10 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		
		BuildingTile bTile11 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile12 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile13 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile14 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile15 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));

		BuildingTile[][] board1 = p1.getGameBoard().getBoard();
		BuildingTile[][] board2 = p2.getGameBoard().getBoard();		
		BuildingTile[][] board3 = p3.getGameBoard().getBoard();

		board1[10][11] = bTile1;
		board1[10][9] = bTile2;
		board1[9][10] = bTile3;

		board2[10][11] = bTile6;
		board2[10][9] = bTile7;
		board2[9][10] = bTile5;


		board3[10][11] = bTile13;
		board3[10][9] = bTile8;
		board3[9][10] = bTile11;

		EvaluationStrategy turn = new EvaluationStrategy(p1, gameInstance, 1);
		turn.doTurn();
		assertEquals(3, p1.getScore());
		assertEquals(5, p2.getScore());
		assertEquals(6, p3.getScore());

		board2[11][10] = bTile4;
		board2[11][11] = bTile16;

		turn.unDoTurn();
		EvaluationStrategy turn1 = new EvaluationStrategy(p1, gameInstance, 1);
		turn1.doTurn();
		assertEquals(1, p1.getScore());
		assertEquals(6, p2.getScore());
		assertEquals(6, p3.getScore());
		turn1.unDoTurn();

		EvaluationStrategy turn3 = new EvaluationStrategy(p1, gameInstance, 2);
		turn3.doTurn();
		assertEquals(6, p1.getScore());
		assertEquals(18, p2.getScore());
		assertEquals(18, p3.getScore());
	}

    	@Test
	public void evaluationStrategyTest2() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player p1 = new Player("Niels", PlayerColor.BLUE);
		Player p2 = new Player("Jörn", PlayerColor.RED);
		
		BoardLogicControllerUtil.initBoard(gameInstance, p1,p2);

		BuildingTile bTile1 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile2 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile3 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile4 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile5 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile16 = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));

		BuildingTile bTile6 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile7 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile8 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile9 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		BuildingTile bTile10 = new BuildingTile(0, BuildingType.GARTEN, new Wall(false, false, false, false));
		
		BuildingTile bTile11 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile12 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile13 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile14 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));
		BuildingTile bTile15 = new BuildingTile(0, BuildingType.TURM, new Wall(false, false, false, false));

		BuildingTile[][] board1 = p1.getGameBoard().getBoard();
		BuildingTile[][] board2 = p2.getGameBoard().getBoard();		

		board1[10][11] = bTile1;
		board1[10][9] = bTile2;
		board1[9][10] = bTile3;

		board2[10][11] = bTile6;
		board2[10][9] = bTile7;
		board2[9][10] = bTile5;


		EvaluationStrategy turn = new EvaluationStrategy(p1, gameInstance, 1);
		turn.doTurn();
		assertEquals(3, p1.getScore());
		assertEquals(5, p2.getScore());

		board2[11][10] = bTile4;
		board2[11][11] = bTile16;

		turn.unDoTurn();
		EvaluationStrategy turn1 = new EvaluationStrategy(p1, gameInstance, 1);
		turn1.doTurn();
		assertEquals(1, p1.getScore());
		assertEquals(6, p2.getScore());
		turn1.unDoTurn();

		EvaluationStrategy turn3 = new EvaluationStrategy(p1, gameInstance, 2);
		turn3.doTurn();
		assertEquals(6, p1.getScore());
		assertEquals(18, p2.getScore());
	}


}
