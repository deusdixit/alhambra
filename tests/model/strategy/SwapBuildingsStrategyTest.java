package model.strategy;

import model.BoardPosition;
import org.junit.Test;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;
import model.game.Player;
import util.exception.NoMoneyException;

import static org.junit.Assert.*;

public class SwapBuildingsStrategyTest {

	@Test
	public void swapBuildingsStrategyTest() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player current = new Player("Test", PlayerColor.BLUE);
		BoardLogicControllerUtil.initBoard(gameInstance, current);
		BuildingTile bTile = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, false, false, false));
		BuildingTile bTile2 = new BuildingTile(10, BuildingType.TURM, new Wall(false, false, false, false));
		current.getGameBoard().getBoard()[10][11] = bTile2;
		current.getReserveBuildings().add(bTile);
		SwapBuildingsStrategy turn = new SwapBuildingsStrategy(current, bTile, gameInstance, new BoardPosition(10, 11));
		turn.doTurn();
		assertEquals(current.getGameBoard().getBoard()[10][11], bTile);
		assertEquals(true, current.getReserveBuildings().contains(bTile2));
		turn.unDoTurn();
		assertEquals(current.getGameBoard().getBoard()[10][11], bTile2);
		assertEquals(true, current.getReserveBuildings().contains(bTile));
		
	}

}
