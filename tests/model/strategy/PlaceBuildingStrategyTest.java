package model.strategy;

import model.BoardPosition;
import org.junit.Test;
import util.exception.NoMoneyException;
import static org.junit.Assert.*;
import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;
import model.game.Player;

public class PlaceBuildingStrategyTest {

	@Test
	public void placeBuildingStrategyTest() throws NoMoneyException {
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player current = new Player("Test", PlayerColor.BLUE);
		BoardLogicControllerUtil.initBoard(gameInstance, current);
		BuildingTile bTile = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, true, false, false));
		PlaceBuildingStrategy turn = new PlaceBuildingStrategy(current, bTile, new BoardPosition(10, 11), gameInstance);
		turn.doTurn();
		assertEquals(bTile, current.getGameBoard().getBoard()[10][11]);
		turn.unDoTurn();
		assertEquals(null, current.getGameBoard().getBoard()[10][11]);
	}

}
