package model.strategy;

import model.BoardPosition;
import org.junit.Test;

import controller.BoardLogicControllerUtil;
import model.GameInstance;
import model.ModelTypeUtil.BuildingType;
import model.ModelTypeUtil.PlayerColor;
import model.board.BuildingTile;
import model.board.BuildingTile.Wall;
import model.game.Player;
import util.exception.NoMoneyException;

import static org.junit.Assert.*;

public class AddFromReserveStrategyTest {

	@Test
	public void addFromReserveStrategyTest() throws NoMoneyException{
		GameInstance gameInstance;
		gameInstance = new GameInstance();
		Player current = new Player("Test", PlayerColor.BLUE);
		BoardLogicControllerUtil.initBoard(gameInstance,current);
		
		BuildingTile bTile = new BuildingTile(0, BuildingType.ARKADEN, new Wall(false, true, false, false));
		current.getReserveBuildings().add(bTile);
		AddFromReserveStrategy turn = new AddFromReserveStrategy(current, bTile, gameInstance, new BoardPosition(10,11));
		turn.doTurn();
		assertEquals(current.getReserveBuildings().size(), 0);
		assertEquals(gameInstance.getCurrentPlayer().getGameBoard().getBoard()[10][11], bTile);
		turn.unDoTurn();
		assertEquals(current.getReserveBuildings().size(), 1);
		assertEquals(gameInstance.getCurrentPlayer().getGameBoard().getBoard()[10][11], null);
	}

}
