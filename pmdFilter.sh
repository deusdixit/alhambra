#!/bin/bash

alias pmd="$HOME/pmd-bin-6.22.0/bin/run.sh pmd"
number=$(cat pdm.txt | wc -l  )
result=${number//[!0-9]/}
echo "PMD start"
echo "$result"
echo "PMD end"
if [ $result -gt 20 ]
    then
        exit 1;
fi

echo "Diese fehler wurden gefunden:"

cat pdm.txt

exit 0;